---
title: 限流
date: 2024-01-18 21:33
---

# 限流

## 1.怎么理解限流？

限流（Rate Limiting）是一种控制系统访问频率的机制。在分布式系统中，为了保护系统免受过多请求的影响，限流机制可以限制每个用户或每个服务对系统的访问速率。
通过设置最大请求数或单位时间内的请求数来限制访问频率，确保系统资源得到合理利用，避免系统过载。

## 2. 限流的原理是什么？

限流的原理是通过设置系统访问频率的最大阈值，控制请求的流量，从而保护系统免受过多请求的影响。以下是限流的一般原理：

1. 计数器：系统会为每个用户或服务维护一个计数器，记录在特定时间窗口内的请求数或请求速率；
2. 阈值设定：根据系统的处理能力和资源情况，设置一个最大请求数或单位时间内的请求数作为阈值；
3. 请求处理：当用户或服务发起请求时，系统会检查当前计数器的值是否已经达到阈值；
4. 判断与处理：如果计数器的值小于阈值，则请求可以继续处理；如果计数器的值超过阈值，则系统可以采取相应的处理策略，如拒绝请求、延迟处理、返回错误信息等；
5. 计数器更新：每次请求处理完成后，系统会更新计数器的值，以反映新的请求数；

## 3. 常用的限流方案有哪些？

1. 固定窗口算法（Fixed Window Algorithm）：在固定的时间窗口内，限制请求的数量或速率。例如，每秒钟只允许处理100个请求；
2. 滑动窗口算法（Sliding Window Algorithm）：将时间划分为多个小的时间窗口，限制每个时间窗口内的请求数量或速率。例如，每秒钟只允许处理100个请求，但可以平滑地分布在整个时间窗口内；
3. 令牌桶算法（Token Bucket Algorithm）：通过令牌的生成和消耗来控制请求的通过率。系统会维护一个令牌桶，每个请求需要消耗一个令牌才能通过，当令牌桶为空时，请求会被限制；
4. 漏桶算法（Leaky Bucket Algorithm）：类似于一个漏水的桶，请求按照固定的速率从桶中流出。如果请求到达时桶已经满了，那么请求会被限制；
5. 计数器算法（Counting Algorithm）：根据单位时间内的请求数量进行限制。例如，每分钟只允许处理1000个请求；

## 4. 常用的限流框架有哪些？

1. Netflix Hystrix：Hystrix是Netflix开源的容错框架，提供了熔断、限流、服务降级等功能。它可以与Spring Cloud等微服务框架集成，为分布式系统提供弹性和稳定性；
2. Alibaba Sentinel：Sentinel是阿里巴巴开源的流量控制组件，支持流量控制、熔断降级、系统负载保护等功能。它可以与Spring Cloud、Dubbo等主流框架集成，为微服务架构提供稳定性保障；
3. Guava RateLimiter：Guava是Google的Java工具库，其中的RateLimiter类提供了基于令牌桶算法的限流功能。它可以方便地集成到各种Java应用中，实现对请求的限速控制；
4. Nginx：Nginx是一款高性能的Web服务器和反向代理服务器，通过配置Nginx的限流模块可以实现对请求的限速控制。Nginx的限流功能适用于对HTTP请求进行全局或特定URL的限制；
5. Envoy：Envoy是一个用于边缘和服务网格代理的云原生高性能代理，支持各种流量控制策略，包括限流。Envoy可以与Kubernetes、Istio等云原生技术进行无缝集成；

## 5. 与限流相关的概念有哪些？怎么理解这些概念？

1. QPS：每秒处理请求的数量，即Queries Per Second的缩写；
2. 熔断：当系统出现故障或异常时，熔断器会拦截请求并降级处理，从而保护系统的稳定性；
3. 降级：在系统出现故障或异常时，将某些服务或功能暂时关闭或降低质量，以保证系统的稳定运行；
4. 超时：设置请求的最大等待时间，超过该时间则认为请求失败；
5. 重试：在请求失败时，重新发送请求以尝试解决问题；

