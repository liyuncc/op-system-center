---
title: 消息推送通知
date: 2024-08-28 10:05
---

# 消息推送通知

在当今的互联网应用中，实时消息推送成为了提升用户体验和增强系统交互性的关键技术。

## 实时性获取数据的几个方案

**Polling(轮询)**

在客户端重复的向服务端发送新请求。如果服务器没有新的数据更动，关闭本次连接。然后客户端在稍等一段时间之后，再次发起新请求，一直重复这样的步骤。

缺点：是有一定的时间间隔，如果间隔变小，会对服务端造成比较大的压力。

**Long-polling(长轮询)**

在长轮询中，客户端发送一个请求到服务端。如果服务端没有新的数据更动，那么本次连接将会被保持，直到等待到更新后的数据，返回给客户端并关闭这个连接。

缺点：服务器维持长连接的资源消耗，且浏览器对于长连接数有限制。

**WebSocket**

真正意义上的双向连接。

**Server-Sent Events**

SSE类似于长轮询的机制，但是它在每一次的连接中，不只等待一次数据的更动。客户端发送一个请求到服务端 ，服务端保持这个请求直到一个新的消息准备好，将消息返回至客户端，此时不关闭连接，仍然保持它，供其它消息使用。SSE的一大特色就是重复利用一个连接来处理每一个消息（又称event）。

缺点：并不是所有浏览器都支持。

## 实现示例

### Server-Sent Events（SSE）

#### SSE 实现原理

SSE 基于 HTTP 协议，客户端向服务器发起一个请求，服务器保持该连接打开。当有新的消息产生时，服务器通过这个连接将消息以特定的格式推送给客户端。SSE 利用了 HTTP 协议的流特性，通过设置特定的响应头，如Content-Type: text/event-stream，来标识这是一个 SSE 连接。服务器推送的消息以特定的格式组织，每行以data:开头表示数据内容。

#### SSE 特点

- 单向通信：SSE 是服务器向客户端的单向推送，客户端无法主动向服务器发送数据。
- 轻量级：相较于 WebSocket 等技术，SSE 的实现相对简单，开销较小。
- 基于HTTP：利用现有的 HTTP 基础设施，易于部署和集成。
- 跨平台支持：大多数现代浏览器都支持 SSE，具有较好的兼容性。

#### SSE 应用场景

- 实时通知：如邮件到达通知、系统告警等。
- 数据实时更新：股票行情、天气预报等数据的实时推送。
- 日志实时监控：将服务器端的日志实时推送到前端展示。

#### SSE 代码示例

```java
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.time.LocalTime;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@RestController
public class SseController {

    @GetMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public SseEmitter streamMessage() {

        // 设置超时时间，0表示不过期。默认30秒，超过时间未完成会抛出异常：AsyncRequestTimeoutException
        SseEmitter emitter = new SseEmitter(0L);

        ExecutorService sseMvcExecutor = Executors.newSingleThreadExecutor();
        sseMvcExecutor.execute(() -> {
            Timer timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    try {

                        SseEmitter.SseEventBuilder event = SseEmitter.event()
                            .id(UUID.randomUUID().toString().replaceAll("-", ""))
                            .name("SSE EVENT - MVC")
                            .data("SSE MVC - " + LocalTime.now());

                        emitter.send(event);
                        
                    } catch (Exception ex) {
                        emitter.completeWithError(ex);
                    }
                }
            }, 0L, 1000L);
        });

        // complete() 示例
        emitter.onCompletion(() -> log.warn("连接正常完成"));

        // onTimeout(Runnable callback) 示例
        emitter.onTimeout(() -> log.warn("连接超时"));

        // onError(ErrorCallback callback) 示例
        emitter.onError((Throwable t) -> log.warn("发生错误: " + t.getMessage()));

        return emitter;
    }
}
```
![message-push-notify-sse-code-example.png](images/message-push-notify-sse-code-example.png)
