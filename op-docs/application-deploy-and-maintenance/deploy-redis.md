# Linux单机版Redis部署文档

## 一、操作步骤

**1.下载链接：**

[Redis Download](https://redis.io/download/)

**2.解压安装包，并编译**

```shell
tar xzf redis-x.x.x.tar.gz
cd redis-x.x.x
make
```

**3.启动Redis**

编译好的二进制文件在 src 目录里。启动 Redis:

```shell
sh redis-server
```

**4.编写脚本管理start、stop、restart、status**

修改redis.conf
> daemonize no 修改为：daemonize yes


*脚本存在部分问题，但不影响使用。（以后在优化。。。）*

```shell
#!/bin/sh
#
# Simple Redis init.d script conceived to work on Linux systems
# as it does use of the /proc filesystem.

### BEGIN INIT INFO
# Provides:     redis_6379
# Default-Start:        2 3 4 5
# Default-Stop:         0 1 6
# Short-Description:    Redis data structure server
# Description:          Redis data structure server. See https://redis.io
### END INIT INFO

REDISPORT=6379
EXEC=/home/vagrant/database/redis-6.2.14/src/redis-server
CLIEXEC=/home/vagrant/database/redis-6.2.14/src/redis-cli

PID=$(ps -ef | grep redis-server | awk '{print$2}')
CONF="/home/vagrant/database/redis-6.2.14/redis.conf"

# 启动服务
start(){
    if [ ! -z $PID ]
    then
        echo "$PID exists, process is already running or crashed"
    else
        echo "Starting Redis server..."
        $EXEC $CONF
    fi
}
# 停止服务
stop(){
    if [ -z $PID ]
    then
        echo "$PID does not exist, process is not running"
    else
        echo $PID
        echo "Stopping ..."
        # $CLIEXEC -p $REDISPORT -a 123456 shutdown 2>/dev/null
        kill /pid $PID
        while [ -x /proc/${PID} ]
        do
            echo "Waiting for Redis to shutdown ..."
            sleep 1
        done
        echo "Redis stopped"
    fi
}
# 启动状态
status(){
    echo "$PID"
    if [ ! -z $PID ]
    then
        echo "redis is running，pid=${PID}"
    else
        echo "redis is not running"
    fi
}

case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    status)
        status
        ;;
    restart)
        stop
        sleep 1s
        start
        ;;
    *)
        echo "Please use start|stop|restart|status as first argument"
        ;;
esac
```

**5.配置访问**

- 防火墙添加过滤拦截端口

> sudo systemctl start firewalld
> sudo firewall-cmd --zone=public --add-port=5432/tcp --permanent
> sudo firewall-cmd --reload

- 修改redis.conf，注释绑定的主机地址

> #bind 127.0.0.1 ，或修改为 bind 0.0.0.0 原理都一样
