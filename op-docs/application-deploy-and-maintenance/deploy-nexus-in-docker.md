---
title: docker搭建Nexus私有仓库
date: 2024-12-04 15:35
---

# docker搭建Nexus私有仓库

**拉取镜像**

```shell
# 下载最新版本镜像
docker pull sonatype/nexus3
```

**创建nexus3配置目录，将容器内目录挂载到此目录上，这样我们可以在宿主机上对文件的修改同步到容器内。**

```shell
# 创建目录
mkdir -p /var/local/nexus3
```

**启动容器，同时挂载目录和映射端口**

```shell
# 映射端口对应的用途：
# 8081：可以通过http访问nexus应用
# 8082：docker(hosted)私有仓库，可以pull和push
# 8083：docker(proxy)代理远程仓库，只能pull
# 8084：docker(group)私有仓库和代理的组，只能pull
# 使用参数 -v 建立宿主机与Docker目录映射关系，/nexus-data：docker里存nexus数据目录，所以将数据目录存放到宿主机/var/local/nexus3
docker run -d --user root -p 8081:8081 -p 8082:8082 -p 8083:8083 -p 8084:8084 -v /var/local/nexus3:/nexus-data --name nexus3 sonatype/nexus3
```

**访问网页端*

```shell
http://192.168.56.10:8081 
```

查看密码：

```shell
# 默认用户名admin
cat /var/local/nexus3/admin.password
```

**创建仓库**

创建 docker(hosted) 类型的仓库

**修改docker配置, 添加如下配置**

```json
{
  "insecure-registries": [
    "192.168.100.163:8082"
  ]
}
```

```shell
[root@localhost ~]# systemctl daemon-reload

[root@localhost ~]# systemctl restart docker

[root@localhost ~]# docker info

Insecure Registries:

192.168.100.163:8082  #生效了

127.0.0.0/8
```

**上传、拉取镜像**

```shell
docker login 192.168.56.10:8082

```

*可能会登录失败*

```shell
Error response from daemon: login attempt to http://192.168.56.10:8082/v2/ failed with status: 401 Unauthorized
```

解决方案：https://blog.csdn.net/michaelwoshi/article/details/106185800

docker tag pcc-restapi 192.168.56.10:8082/pcc-restapi:1.0.0

docker push 192.168.56.10:8082/pcc-restapi:1.0.0

**配置docker仓库、配置maven仓库**