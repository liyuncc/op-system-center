---
title: docker部署jenkins，集成CICD
date: 2024-11-29 10:21
---

# docker部署jenkins，集成CICD

### docker部署jenkins

**1.拉取 Jenkins 镜像。**

```shell
docker pull jenkins/jenkins

```

**2.创建 Jenkins 工作目录，将容器内目录挂载到此目录上，这样我们可以在宿主机上对文件的修改同步到容器内。**

```shell
mkdir -p /usr/local/jenkins
chmod 777 /usr/local/jenkins
```

**3.启动容器，同时映射宿主机和容器内端口。**

```shell
# -d 后台方式启动
# --privileged 在 docker 容器内部运行 docker命令
# -u root 指定运行用户
# -p 映射端口，宿主机端口:容器内端口
# -v 挂载卷，将容器Jenkins工作目录/var/jenkins_home挂载到宿主机目录/usr/local/jenkins
# -v /var/run/docker.sock:/var/run/docker.sock 容器内也可访问宿主机docker
# -v /usr/bin/docker:/usr/bin/docker 容器内也可访问宿主机docker
# -name 给容器起个别名
docker run --privileged -d -u root -p 8099:8080 -p 50099:50000 -v /usr/local/jenkins:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock -v /usr/bin/docker:/usr/bin/docker --restart=always --name jenkins jenkins/jenkins

```

<span style="color:red">非常重要：这三个参数一定要配置才能继续后续操作!!!</span>

<span style="color:red">--privileged 获取宿主机权限</span>

<span style="color:red">-u root 使用root用户执行命令</span>

<span style="color:red">-v /var/run/docker.sock:/var/run/docker.sock 容器内部使用宿主机docker环境</span>

<span style="color:red">-v /usr/bin/docker:/usr/bin/docker 容器内部使用宿主机docker环境</span>

**4.查看容器是否启动成功**

```shell
[root@chenpihost local]# docker ps
CONTAINER ID   IMAGE                 COMMAND                  CREATED          STATUS          PORTS                                                                                      NAMES
538de1b1b1f5   jenkins/jenkins   "/sbin/tini -- /usr/…"   18 seconds ago   Up 17 seconds   0.0.0.0:8099->8080/tcp, :::8099->8080/tcp, 0.0.0.0:50099->50000/tcp, :::50099->50000/tcp   jenkins
```

**4.1.进入容器**

```shell
docker exec -it 00cf9467fc65 /bin/bash

## root用户进入
docker exec -it -u 0 f3062cb90059  /bin/bash
```

**5.查看 Jenkins 容器日志。**

```shell
docker logs jenkins
```

**6.将 Jenkins 端口添加到防火墙。**

```shell
firewall-cmd --zone=public --add-port=8099/tcp --permanent
systemctl restart firewalld
firewall-cmd --zone=public --list-ports
```

### 登录初始化 Jenkins

**7.在浏览器访问http://jenkins所在主机ip:8099**

**8.可以查看宿主机/usr/local/jenkins/secrets/initialAdminPassword文件获取密码。**

**9.创建一个 root 用户，并进行登录。**

**10.下载推荐插件**

**11.Manage Jenkins -》 Tools -》 配置jdk和maven**

**12.Manage Jenkins -》 Plugins -》 下载Maven集成插件**

### 配置build item

**配置仓库地址**

**配置编译成功后的执行脚本**

例如:

```shell
# build image
echo "starting build image..."
docker build -t pcc-restapi .

# stop and remove running container
echo "stop and remove running container..."
docker stop pcc-restapi
docker rm pcc-restapi

# start new instance container
echo "start new instance container..."
docker run -d -p 8888:8080 --name pcc-restapi pcc-restapi:latest
```

**编写dockerfile**

```dockerfile
# Use the official Java 17 base image
#FROM adoptopenjdk:17-jdk-hotspot
FROM openjdk:17-oracle

# Set environment variables
ENV APP_HOME=/app \
    JAVA_OPTS=""

# Set the working directory
WORKDIR $APP_HOME

# Copy the application files to the container
COPY /pcc-restapi/target/pcc-restapi-1.0-SNAPSHOT.jar .

# Set the entrypoint command
ENTRYPOINT ["java", "-jar", "pcc-restapi-1.0-SNAPSHOT.jar"]
```

**开始构建 -》 构建成功 -》 image build成功 -》 image run成功**
