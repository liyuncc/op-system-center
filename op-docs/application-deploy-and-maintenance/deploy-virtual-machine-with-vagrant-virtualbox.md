---
title: Vagrant+VirtualBox部署本地虚拟机
date: 2023-12-27 09:51
---

# Vagrant+VirtualBox部署本地虚拟机

## 一、部署Vagrant虚拟机

### 1.确认vagrant和virtualBox匹配版本

VirtualBox和Vagrant版本要匹配，推荐使用：VirtualBox-6.1.48、vagrant-2.2.19

以下一些常见的Vagrant版本和VirtualBox版本的匹配：

| Vagrant Version | VirtualBox Version       |
|-----------------|--------------------------|
| Vagrant 2.2.x   | VirtualBox 6.0.x 或 5.2.x |
| Vagrant 2.1.x   | VirtualBox 5.2.x         |
| Vagrant 2.0.x   | VirtualBox 5.1.x         |
| Vagrant 1.9.x   | VirtualBox 5.0.x         |

如果你已经安装了一个不兼容的版本，可以尝试升级/降级其中一个软件。如果需要升级/降级VirtualBox，请先移除现有版本，然后再安装所需版本。
如果需要升级/降级Vagrant，请运行以下命令：

```shell
vagrant plugin install vagrant-version --plugin-version <your-wanted-version>
vagrant version switch <your-wanted-version>
```

### 2.下载vagrant、virtualBox

**vagrant**

```shell
https://developer.hashicorp.com/vagrant
```

**virtualBox**

```shell
https://www.virtualbox.org/wiki/Downloads
```

### 3.新建虚拟机

#### 3.1 创建工作空间

```shell
mkdir vagrant-workspace
```

#### 3.2 初始化操作系统

```shell
vagrant init centos/7
```

#### 3.3 启动虚拟机

```shell
vagrant up
```

#### 3.4 登录虚拟机

```shell
vagrant ssh
```

### 4.挂载目录

修改Vagrantfile文件：添加如下配置：

```shell
config.vm.synced_folder "C:\\Packages\\vagrant-workspace\\mount", "/mount"
```

报错解决方案：

```shell
vagrant plugin install --plugin-clean-sources --plugin-source https://gems.ruby-china.com/ vagrant-vbguest --plugin-ve
rsion 0.24
```

### 5.配置IP（配置虚拟网络）

#### 5.1、端口转发

配置好虚拟机后，默认使用的是端口转发。例如虚拟机中有一个mysql服务，需要在virtualbox中设置端口映射物理机器的本地端口（虚拟机3306->
物理机3306），才可以在物理机上访问虚拟机上的mysql服务。

- 缺点：需要配置很多端口映射，可能造成物理机可使用端口紧缺；

- 优点：使用虚拟网卡给虚拟机配置一个IP，使本机和虚拟机可以ping通；

#### 5.2配置虚拟网络

**方法1：登录虚拟机，设置网卡**

**方法2：修改vagrantfile配置文件**

1. 查看物理机ip地址：ipconfig （192.168.56.1）

2. 修改Vagrantfile文件：添加如下配置：

    ```shell
    config.vm.network "private_network", ip: "192.168.56.10"
    ```

   *注意：虚拟机IP需要和宿主机IP在同一个网段。*

3. 重启vagrant

    ```shell
    vagrant reload
    ```

4. 验证：ip addr，互相ping

### 6.vagrant常用命令

- vagrant init：初始化一个新的Vagrant环境。
- vagrant up：启动虚拟机并创建/配置虚拟环境。
- vagrant ssh：通过SSH连接到正在运行的虚拟机。
- vagrant halt：关闭虚拟机。
- vagrant suspend：挂起虚拟机并保存当前状态。
- vagrant resume：恢复先前挂起的虚拟机。
- vagrant reload：重启虚拟机并应用新的Vagrantfile配置更改。
- vagrant destroy：销毁虚拟机及其所有相关资源。
- vagrant status：查看当前虚拟机的状态。
- vagrant global-status：显示所有Vagrant环境的状态。
- vagrant ssh-config：显示SSH配置信息。
- vagrant plugin install <name>：安装Vagrant插件。
- vagrant plugin uninstall <name>：卸载Vagrant插件。

### 7.配置SSH登录

vagrant在启动后，可以通过`vagrant ssh`连接虚拟机，但是此命令不满足我们日常使用需要，因此需要配置ssh，以便远程登录；

#### 7.1 登录到虚拟机，切换到root用户：

```shell
vagrant ssh
su root
```

#### 7.2 修改配置文件

修改配置文件`/etc/ssh/sshd_config`，修改 passwordAuthentication no 改为 yes

> PasswordAuthentication yes

#### 7.3 重启ssh服务，vagrant用户默认密码是vagrant

```shell
systemctl restart sshd.service
```

### 8.设置虚拟机运行内存CUP数

**修改vagrantfile配置文件**

添加如下配置：

   ```shell
    config.vm.provider "virtualbox" do |v|
      v.memory = 4096
      v.cpus = 2
   end
   ```

## 二、Vagrant部署应用实例

### 1.安装docker

#### 1.1 检查是否有yum

```shell
yum install -y redhat-lsb
```

#### 1.2 查看是否已安装docker

```shell
yum list installed | grep docker
```

#### 1.3 卸载旧版本docker

```shell
sudo yum remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-engine
```

#### 1.4 设置镜像地址

```shell
sudo yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
```

#### 1.5 安装docker

```shell
sudo yum install -y docker-ce docker-ce-cli containerd.io
```

#### 1.6 启动docker

```shell
systemctl start docker
```

#### 1.7 开机自启docker

```shell
sudo systemctl enable docke
```

### 2.centos部署jar报运行

编写bash

```shell
#！/bin/bash
echo "starting..."
nohup java -jar jarName-0.0.1-SNAPSHOT.jar >msg.log 2>&1 &;
```

脚本添加执行权限

```shell
chmod u+x your-shell-file 
```