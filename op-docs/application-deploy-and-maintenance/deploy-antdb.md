# AntDB单机版centos7部署文档

## 一、操作步骤

官方文档：[国产数据库_AntDB数据库安装过程 - 亚信安慧科技](http://www.antdb.net/72_install)

**0.切换到root用户**

```shell
su root
```

**1.在home目录创建用户**

创建一个用户antdb，目录为/home/antdb

```shell
useradd -d /home/antdb -m antdb
```

**2.修改用户antdb密码**

```shell
passwd antdb
```

**3.下载安装包（不能访问外网可用sftp上传）**

```shell
cd /home/antdb

curl -O http://gz.antdb.net/zip/antdb-ce-7.2.0.centos7.x86_64.tar.gz
```

**4.将安装包权限更改为antdb**

```shell
chown -R antdb:antdb antdb-ce-7.2.0.centos7.x86_64.tar.gz
```

**5.登录新建用户antDB进行安装**

```shell
su - antdb
```

**6.解压安装包**

```shell
tar -xzvf antdb-ce-7.2.0.centos7.x86_64.tar.gz
```

**7.进行安装**

进入 antdb-ce-7.2.0.centos7.x86_64目录，执行sh antdb_install.sh 进行安装。

```shell
cd /home/antdb/antdb-ce-7.2.0.centos7.x86_64

sh antdb_install.sh
```

**8.安装成功后，更新环境变量.**

```shell
source /home/antdb/.bashrc
```

**9.开启远程访问功能**

- 修改**postgresql.conf**文件，当中添加或者编辑下列内容：`listen_addresses = '*'`

- 修改**adb_hba.conf**文件，添加如下内容，以便允许用户通过某种method（例如执行ip范围）
  来访问数据库：`host all all 0.0.0.0/0 trust`

**10.数据库启动、重启、停止命令**

```shell
pg_ctl start -D /home/antdb/app/antdb/data/

pg_ctl stop -D /home/antdb/app/antdb/data/

pg_ctl restart -D /home/antdb/app/antdb/data/
```

## 二、补充说明

_部署到bigcloud8时，antdb_install.sh会报错找不到响应系统版本，可通过修改部署脚本解决，以下为实际解决方案，仅供参考：_

**1.操作系统信息**
![deploy-antdb-modify-script-system-version.png](images/deploy-antdb-modify-script-system-version.png)

**2.需修改脚本及修改内容**

位置：
![deploy-antdb-modify-script-location.png](images/deploy-antdb-modify-script-location.png)

修改内容：

1、解压安装包时使用绝对路径：
![deploy-antdb-modify-script-tar-package-use-absolute-path.png](images/deploy-antdb-modify-script-tar-package-use-absolute-path.png)

2、设置操作系统名称为CentOS
![deploy-antdb-modify-script-change-os-name.png](images/deploy-antdb-modify-script-change-os-name.png)

3、不要检查sha256文件是否为特殊文件
![deploy-antdb-modify-script-dose-not-check-sha256-file-special.png](images/deploy-antdb-modify-script-dose-not-check-sha256-file-special.png)

4、sha256文件使用绝对路径
![deploy-antdb-modify-script-sha256-file-use-absolute-path.png](images/deploy-antdb-modify-script-sha256-file-use-absolute-path.png)