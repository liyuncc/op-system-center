# 单机版Nacos部署文档

[Nacos 文档](https://nacos.io/zh-cn/docs/quick-start.html)

**2.3.0版本nacos启动配置**

- 启用登录验证：

``` 
nacos.core.auth.enabled=true
```

- 配置身份验证：nacos.core.auth.enabled

```
nacos.core.auth.server.identity.key=your-key
nacos.core.auth.server.identity.value=your-value
```

- 配置Token密钥：

``` 
nacos.core.auth.plugin.nacos.token.secret.key=your-secret
```

- 修改配置为单机启动：

shutdown.cmd 启动脚本

```
set MODE="standalone"
```

shutdown.sh 启动脚本

```
export MODE="standalone"
```
