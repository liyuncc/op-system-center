---
title: docker部署redis
date: 2024-11-30 17:11
---

# docker部署redis

### docker部署redis

**拉取镜像**

```shell
# 下载最新版本镜像
docker pull redis
```

**创建redis配置目录，将容器内目录挂载到此目录上，这样我们可以在宿主机上对文件的修改同步到容器内。**

```shell
# 创建目录
mkdir -p /var/local/redis/conf
mkdir -p /var/local/redis/data

# 创建文件
touch /var/local/redis/conf/redis.conf
```

注意：

1. 启动前需要先创建Redis外部挂载的配置文件 （ /home/redis/conf/redis.conf ）
2. 之所以要先创建 , 是因为Redis本身容器只存在 /etc/redis 目录 , 本身就不创建 redis.conf 文件
3. 当服务器和容器都不存在 redis.conf 文件时, 执行启动命令的时候 docker 会将 redis.conf 作为目录创建 , 这并不是我们想要的结果 

**启动容器，同时挂载目录和映射端口**

```shell
# -d 后台方式启动
# --restart=always 自动重启
# --log-opt max-size=100m 容器日志的设置，最大大小为 100MB
# --log-opt max-file=2 容器日志文件的设置，最多可以有2个日志文件
# -p 16379:6379 端口映射的设置，将宿主机的16379端口映射到容器的6379端口
# --name redis 给容器起个别名
# -v /var/local/redis/conf:/etc/redis/redis.conf 
# -v /var/local/redis/data:/data
# redis redis-server /etc/redis/redis.conf --appendonly yes --requirepass redis@2024
docker run --restart=always --log-opt max-size=100m --log-opt max-file=2 -p 16379:6379 --name redis -v /var/local/redis/conf/redis.conf:/etc/redis/redis.conf -v /var/local/redis/data:/data -d redis redis-server /etc/redis/redis.conf --appendonly yes --requirepass redis@2024 
```

**Redis 配置文件修改**

```shell
# 修改 /var/local/redis/conf/redis.conf
# protected-mode no 关闭protected-mode模式，此时外部网络可以直接访问 
# bind 0.0.0.0 设置所有IP都可以访问
protected-mode no
bind 0.0.0.0
```

**重启redis**













