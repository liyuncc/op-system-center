1、安装文档：
https://www.postgresql.org/download/linux/redhat/

2、DBeaver连接报错：

1）、Connection to 192.168.XX.XX:5432 refused. Check that the hostname and port are correct and that the postmaster is accepting TCP/IP connections.

解决方案：修改： /var/lib/pgsql/xxx/data/postgresql.conf 新增：新增listen_addresses = '*'

2）、no pg_hba.conf entry for host "192.168.X.X", user "postgres", database "postgres"

解决方案：修改：/var/lib/pgsql/xxx/data/pg_hba.conf 添加：host all all 0.0.0.0/0 md5

3）、The server requested SCRAM-based authentication, but no password was provided.

解决方案：修改密码

3、启用、启动、停止、重启命令
> sudo systemctl enable postgresql-14
> sudo systemctl start postgresql-14
> sudo systemctl stop postgresql-14
> sudo systemctl restart postgresql-14

4、防火墙添加过滤拦截端口
> sudo systemctl start firewalld
> sudo firewall-cmd --zone=public --add-port=5432/tcp --permanent
> sudo firewall-cmd --reload