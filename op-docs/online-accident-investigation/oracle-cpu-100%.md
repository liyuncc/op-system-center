---
title: 记一次数据库cpu90+%调查
date: 2024-07-01 16:18
---

# 记一次数据库cpu90+%调查

1、上午九点左右，客户反馈系统卡顿、白屏

2、查看服务中心，发现消费者服务由9个节点掉到6个节点，挂了三个节点

![img_1.png](images/oracle-cpu-100%-nacos-service-node.png)

3、ssh登录服务器，查看日志，发现连接数据库超时

4、登录数据库服务器，数据库挂了，重启数据库服务

5、重启队列消息消费服务，

6、客户还是反馈系统卡顿

7、iostat 2 命令查看数据库使用情况，发现cpu占用率90+；

![img.png](images/oracle-cpu-100%-avg-cpu.png)

8、怀疑是因为大量数据积压，频繁读写数据库，导致cpu居高不下，

mq队列消息图如下：
![img_2.png](images/oracle-cpu-100%-rabbitMq.png)

9、目前服务集群有两个，分别是后台服务和消费者服务
通过命令查询top20的占用cpu的sql：

```shell
ps aux|head -1;ps aux|grep -v PID|sort -rn -k +3|head -20
```

```shell
## spid的那个值用ps aux命令第二列的pid查询
SELECT sql_text
FROM V$sqltext a
WHERE (a.hash_value, a.address)
IN (SELECT DECODE(sql_hash_value, 0 , prev_hash_value, sql_hash_value),
DECODE(sql_hash_value, 0 ,prev_sql_addr, sql_address)
FROM v$session b
WHERE b.paddr = (SELECT addr FROM v$process c WHERE c.spid = '348550'))
ORDER BY piece ASC;
```

尝试kill高占用cpu的session：

```shell
select  b.sid,
       b.serial# 
FROM v$session b
WHERE b.paddr = (SELECT addr FROM v$process c WHERE c.spid = '185143')
 
alter system kill session '457,6291';
```

10、分别停止两个服务做对比分析，确认是哪中服务导致问题发生，

11、最后对比发现，即使两个服务都停止，断开与数据库的连接，在极低的读写频率下，cpu还是居顶不下，

查询session占用cpu

```shell
SELECT
ss.sid,
se.command,
ss.value CPU ,
se.username,
se.program
FROM
v$sesstat ss,
v$session se
WHERE
ss.statistiC# IN
(
SELECT
statistic#
FROM
v$statname
WHERE
name = 'CPU used by this session')
AND se.sid = ss.sid
AND ss.sid>6
ORDER BY
ss.sid;
```

比较哪个session的CPU使用时间最多，然后查看该Session的具体情况：

```shell
SELECT
s.sid,
s.event,
w.wait_time,
w.seq#,
q.sql_text
FROM
v$session_wait w,
v$session s,
v$process p,
v$sqlarea q
WHERE
s.paddr = p.addr
AND
--　　s.sid=&p and
s.sql_address = q.address;
```

发现有大量的`latch: cache buffers chains`事件

![img_3.png](images/oracle-cpu-100%-database-events.png)

12、跟数据组同事调查发现，是因为同步数据到分布式库时，有一张表数据同步失败，连续数据回滚导致；

```shell
cd $ORACLE_HOME/rdbms/admin
@?/rdbms/admin/awrrpt.sql
```

awr报告也显示大量`latch: cache buffers chains`事件

![img_5.png](images/oracle-cpu-100%-awr-events.png)

![img_6.png](images/oracle-cpu-100%-import-error-logs.png)

13、结束同步操作，停止回滚事务；

14、重启服务，持续监控