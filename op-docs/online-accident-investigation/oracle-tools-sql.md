---
title: ORACLE 数据库常用SQL工具集
date: 2024-08-29 11:06
---

**查询单表数据占用存储空间大小**

```shell
-- 查询单表数据占用存储空间大小
select sum(bytes)/(1024*1024) as "size(M)" 
from user_segments
where segment_name=upper('QW_TBL_WX_MESSAGE');
```

**生成uuid**

```shell
-- 生成uuid
SELECT rawtohex(SYS_GUID()) FROM dual;
```

**自增序列**

```shell
-- 自增序列
select your_tablename_seq.NEXTVAL from dual
```

**复制一张表的数据到另一张表方法**

```shell
-- 复制一张表的数据到另一张表方法
create table YOUR_TABLENAME_BACKUP as select * from YOUR_TABLENAME;
```

**统计表占用空间情况**

```shell
-- 统计表占用空间情况
Select Segment_Name,Sum(bytes)/1024/1024 From User_Extents Group By Segment_Name
```

**查看所有序列**

```shell
-- 查看所有序列
select * from user_sequences
```

**创建序列**

```shell
-- 创建序列
CREATE SEQUENCE your_tablename_seq
 START WITH 80
 INCREMENT BY 1
 NOMINVALUE
 MAXVALUE 9999999999999999999999999999
 NOCYCLE
 NOORDER
 CACHE 100;
```

**删除指定名称的序列**

```shell
-- 删除指定名称的序列
DROP sequence your_tablename_seq;
```