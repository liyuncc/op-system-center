---
title: 使用Logstash对mysql数据做增量同步
date: 2025-01-06 10:20
---

# Mysql数据增量同步，使用Logstash进行同步

## 需求背景

目前有两个内网Mysql数据库，因不同的业务要求，需要两个库同事作为活跃库，需要做到短时间完成增量数据的同步。

**数据量**

源数据库，数据增长大概每天30万上下，数据采频率为5分中采集一次，粗略计算每五分钟写入数据为1042条。

**网络通信**

两台数据库都是部署在内网，访问只能通过外网跳板机进行访问，因此网络通信也需要进行考虑。

## 方案选择

增量数据同步，首先想到的是基于mysql的binlog进行监听实现数据同步，其次是使用一些中间件进行数据同步。

**网络通信**

网络通信方面，没有其他更好的选择，采用ssh端口映射进行内网数据库访问。在外网服务器端口开发限制的条件下，尽可能地减少端口的开放。

**同步方案**

首先需要同步的数据库，主要数据表只有一个；

其次，由于网络限制，需要进行ssh端口映射；且沟通后发现，由于网络策略限制，只能是目标库的跳板机进行端口开发，就意味着只能在源数据库的跳板机上做端口
映射。

由于以上限制，基于binlog的直接配置主从同步的方案就开展不了。

组内沟通得到以下两种方案。

| 名称       | 原理简介                                                                                              | 实现描述                                                         | 备注                    |
|----------|---------------------------------------------------------------------------------------------------|--------------------------------------------------------------|-----------------------|
| canal    | 基于mysql的binlog实现，启动一个连接服务模拟slave数据库，向主库进行binlog事件load，解析事件                                        | 需要启动两个服务，一个是模拟slave的连接服务，另一个是client服务，将canal解析到的数据操作事件同步到目标库 | 需要进行手动编码，实现写入目标库的同步逻辑 |
| logstash | 三个主要组件构成：Input、Filter和Output。Input组件负责从各种数据源中读取数据，Filter组件对读取到的数据进行过滤和处理，Output组件则将处理后的数据发送到指定的目标 | 需要部署logstash服务，进行相关配置，通过条件查询的方式实现数据增量同步                      | 无需手动编码，但需要进行相关的配置文件配置 |

基于上述方案，以及希望简易、快速实现同步效果的期望，最终选择logstash方案进行同步。

## 方案设计

架构图：

![logstash-sync-mysql.png](./images/logstash-sync-mysql.png)

## 方案实现

### ssh端口映射

在182.xxx.xxx.xxx机器上做端口映射，可通过访问本地3306端口访问192.xxx.xxx.xxx:3306。

```shell
ssh -L 3306:192.xxx.xxx.xxx:3306 root@202.xxx.xxx.xxx -N -f
```

### 部署logstash

**安装logstash**

https://elkguide.elasticsearch.cn/logstash/get-start/install.html

**添加mysql连接jar**

### 编写配置文件

> cd config && touch logstash-sync-xxx.config

logstash-sync-xxx.config

```text
input {
  jdbc {
    jdbc_driver_library => "/opt/logstash/logstash-8.17.0/logstash-core/lib/jars/mysql-connector-j-8.0.33.jar"
    jdbc_driver_class => "com.mysql.jdbc.Driver"
    jdbc_connection_string => "jdbc:mysql://172.xxx.xxx.xxx:3306/xxx?characterEncoding=utf8&useSSL=false&serverTimezone=Asia/Shanghai"
    jdbc_user => "xxx"
    jdbc_password => "xxx"
    jdbc_paging_enabled => "true"
    jdbc_page_size => "50000"
    jdbc_default_timezone => "Asia/Shanghai"
    lowercase_column_names => "false"
    parameters => { "sql_last_value" => "create_at" }
    statement_filepath => "/opt/logstash/logstash-8.17.0/config/mysql/query-xxx.sql"
    # statement => "SHOW BINLOG EVENTS"
    schedule => "*/5 * * * *"
    use_column_value => true
    tracking_column => "TM"
    tracking_column_type => "timestamp"
    last_run_metadata_path => "/opt/logstash/logstash-8.17.0/config/mysql/last_run_metadata"
  }
}

filter {
  json {
    source => "message"
  }
}

output {
  jdbc {
    driver_jar_path => "/opt/logstash/logstash-8.17.0/logstash-core/lib/jars/mysql-connector-j-8.0.33.jar"
    driver_class => "com.mysql.jdbc.Driver"
    connection_string => "jdbc:mysql://localhost:3306/xxx"
    username => "xxx"
    password => "xxx"
    statement => ["INSERT INTO you_table (xxx1, xxx2, create_at) VALUES(?, ?, ?)", "xxx1", "xxx2", "create_at"]
  }
}
```

> cd config && mkdir mysql && touch query-xxx.sql && touch last_run_metadata

query-xxx.sql

```text
SELECT yt.xxx1, yt.xxx2, yt.create_at FROM you_table yt WHERE yt.create_at > :sql_last_value order by you_condition
```

last_run_metadata

```text
--- !ruby/object:DateTime '2025-01-06 03:45:00.000000000 Z'
```

### 启动服务

后台运行同步任务

```shell
cd /logstash
nohup bin/logstash -f config/logstash-sync-xxx.config &
```

---

参考资料：
> [Elastic实战：通过logstash-input-jdbc实现mysql8.0全量/增量同步至ES7.x](https://developer.aliyun.com/article/1082068)
>
> [手把手告诉你如何监听 MySQL binlog 实现数据变化后的实时通知！](https://developer.aliyun.com/article/868669)
> 