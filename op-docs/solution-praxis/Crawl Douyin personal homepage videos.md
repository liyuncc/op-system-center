---
title: 爬取抖音个人主页视频
date: 2024-09-03 14:39
---

# 爬取抖音个人主页视频

## 进入个人主业，打开控制台，找到获取所有主页视频的接口：

![img.png](images/crawl-douyin-videos-request-example.png)

**请求原数据**
```html
https://www.douyin.com/aweme/v1/web/aweme/post/?device_platform=webapp&aid=6383&channel=channel_pc_web
&sec_user_id=MS4wLjABAAAAUKRkWkZwdtsDSJ_M5S9w2tzdiT2m2cEx_i61hjNISijuYsL19rD0QLMqddJTek5A&max_cursor=0
&locate_item_id=7410014440319421732&locate_query=false&show_live_replay_strategy=1&need_time_list=1&time_list_query=0
&whale_cut_token=&cut_version=1&count=18&publish_video_strategy_type=2&update_version_code=170400&pc_client_type=1
&version_code=290100&version_name=29.1.0&cookie_enabled=true&screen_width=1920&screen_height=1080&browser_language=en-US
&browser_platform=MacIntel&browser_name=Edge&browser_version=127.0.0.0&browser_online=true&engine_name=Blink
&engine_version=127.0.0.0&os_name=Mac+OS&os_version=10.15.7&cpu_core_num=8&device_memory=8&platform=PC&downlink=10
&effective_type=4g&round_trip_time=100&webid=7409923903698486834&verifyFp=verify_m0kloyua_QkTAfsr2_y7mh_4a6t_9zrw_GUEprAl3VOf0
&fp=verify_m0kloyua_QkTAfsr2_y7mh_4a6t_9zrw_GUEprAl3VOf0
&msToken=zIK89Iexcgx2Pxzt_7QHHwr-lFHmGSAe90HruwlvGzmOZhrXWbu9ErbdMmlUtYss8gGTPDi_Guj3hQOA7MfeOlN4gf8sbR_nkfeCteliL5uSVz9LwdEl
&a_bogus=EJm0%2F50gmD6Pkf6k53QLfY3q6UB3Yh1z0trEMD2fvx3cIg39HMOx9exLmWXvLd8jN4%2FkIeDjy4hbOpn2rQCy01wf7Wsx%2F2CZs640cMlM520j5HgeejumE0hO-ib3SFaM-hNAxc40y75azYT0Ao%2F75vI6Of9ja3Lk96Et7pZTRpS%2FxKY5
```

**响应（删除部分数据）**
```json
{
  "status_code": 0,
  "min_cursor": 0,
  "max_cursor": 1722164063000,
  "has_more": 1,
  "aweme_list": [
    {
      "aweme_id": "7406182537128594703",
      "desc": "先来一波花絮《消失的自行车》#骑行",
      "create_time": 1724386261,
      "author": {},
      "music": {},
      "publish_plus_alienation": {
        "alienation_type": 0
      },
      "video": {
        "play_addr": {
          "uri": "v0300fg10000cr40mofog65lp7kc14s0",
          "url_list": [
            "http://v3-web.douyinvod.com/b79085af89747f33ffcd92fdcdbec5bd/66d6da24/video/tos/cn/tos-cn-ve-15/okkk1DtI4NFAAGhfAgrIeDmDDn9AyAW5EEjBQR/?a=6383&ch=10010&cr=3&dr=0&lr=all&cd=0%7C0%7C0%7C3&cv=1&br=2641&bt=2641&cs=0&ds=3&ft=AJkeU_TERR0sO1C5-Dv2Nc0iPMgzbL7BU~sU_4ow5vwSNv7TGW&mime_type=video_mp4&qs=0&rc=aTNmPDU0Ojk2MzM6OTdnZkBpMzdmOm85cnIzdTMzNGkzM0A2Li42MmEtXmMxYjMvYS0wYSNwLmhtMmRjajFgLS1kLTBzcw%3D%3D&btag=80000e00018000&cquery=100o_101r_100B_100x_100z&dy_q=1725345738&feature_id=2e1813f3872a2105acee44623dff2864&l=20240903144218360F878F0C0334029317",
            "http://v26-web.douyinvod.com/537d99cc35953006294d0e0d92100d69/66d6da24/video/tos/cn/tos-cn-ve-15/okkk1DtI4NFAAGhfAgrIeDmDDn9AyAW5EEjBQR/?a=6383&ch=10010&cr=3&dr=0&lr=all&cd=0%7C0%7C0%7C3&cv=1&br=2641&bt=2641&cs=0&ds=3&ft=AJkeU_TERR0sO1C5-Dv2Nc0iPMgzbL7BU~sU_4ow5vwSNv7TGW&mime_type=video_mp4&qs=0&rc=aTNmPDU0Ojk2MzM6OTdnZkBpMzdmOm85cnIzdTMzNGkzM0A2Li42MmEtXmMxYjMvYS0wYSNwLmhtMmRjajFgLS1kLTBzcw%3D%3D&btag=80000e00018000&cquery=100x_100z_100o_101r_100B&dy_q=1725345738&feature_id=2e1813f3872a2105acee44623dff2864&l=20240903144218360F878F0C0334029317",
            "https://www.douyin.com/aweme/v1/play/?video_id=v0300fg10000cr40mofog65lp7kc14s0&line=0&file_id=3c0dd42667e040f3a607d152991c109f&sign=79c6da902fe9ba5dca2104e4adcf440c&is_play_url=1&source=PackSourceEnum_PUBLISH"
          ]
        }
      }
    }
  ],
  "time_list": [
    "2024·09",
    "2024·08",
    "2024·07",
    "2024·06",
    "2024·05"
  ],
  "log_pb": {
    "impr_id": "20240903144218360F878F0C0334029317"
  },
  "has_locate_item": true,
  "locate_item_available": true,
  "locate_item_cursor": 1725278440000,
  "request_item_cursor": 0,
  "post_serial": 2,
  "replace_series_cover": 1
}
```

仔细观察、测试及查阅资料后其中有几个关键请求参数：
- sec_user_id：视频主页用户ID；
- count：分页大小；
- max_cursor：当前页；
- msToken：msgToken可以理解为Message Token，相当于每次消息请求的令牌，主要用于请求统计；
- a_bogus：是x-bogus的新版本，x-bogus是一种防数据包伪造的参数，又称x伪造，主要用于反爬虫，可以查询到x-bogus生成算法；

分析响应数据：
- has_more：列表是否还有为查询数据；
- aweme_list：视频列表；
- aweme_list.aweme_id：视频ID；
- aweme_list.desc：视频描述；
- aweme_list.create_time：视频发表时间；
- aweme_list.video：视频资源数据；
- aweme_list.video.play_addr：视频资源播放地址；
- aweme_list.video.play_addr.url_list：视频资源播放地址url，可直接访问；

尝试访问`aweme_list.video.play_addr.url_list`，可以访问，并且下载：

```shell
http://v3-web.douyinvod.com/b79085af89747f33ffcd92fdcdbec5bd/66d6da24/video/tos/cn/tos-cn-ve-15/okkk1DtI4NFAAGhfAgrIeDmDDn9AyAW5EEjBQR/?a=6383&ch=10010&cr=3&dr=0&lr=all&cd=0%7C0%7C0%7C3&cv=1&br=2641&bt=2641&cs=0&ds=3&ft=AJkeU_TERR0sO1C5-Dv2Nc0iPMgzbL7BU~sU_4ow5vwSNv7TGW&mime_type=video_mp4&qs=0&rc=aTNmPDU0Ojk2MzM6OTdnZkBpMzdmOm85cnIzdTMzNGkzM0A2Li42MmEtXmMxYjMvYS0wYSNwLmhtMmRjajFgLS1kLTBzcw%3D%3D&btag=80000e00018000&cquery=100o_101r_100B_100x_100z&dy_q=1725345738&feature_id=2e1813f3872a2105acee44623dff2864&l=20240903144218360F878F0C0334029317
```

![img_1.png](images/crawl-douyin-videos-show-video-url.png)

流程走通了，关键问题就是构建请求获取视频列表

## 编写代码构建获取视频列表请求：

github找了一圈，找到响应的开源工具，clone下来，run了下，能正常工作；

仓库地址：[Spider_DouYin_Videos](https://github.com/huifeng-kooboo/Spider_DouYin_Videos)

