---
title: 并发编程设计
date: 2024-05-07 20:52
---

# 并发编程设计

## 1.线程池

> 参考链接：[Java项目亮点系列第一篇，助力2024，让你不再迷茫。](https://www.bilibili.com/video/BV1pC4y1v7Aw/)

**线程池参数配置**

**滑动时间窗口**

**重试纪元**

**线程池自定义拒绝策略**

**线程池状态监控**

示例代码实现：
```java
package com.only4play;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class SolutionThread {
    // 为滑动时间窗口准备一个队列存放元素的集合，用queue实现滑动时间窗口
    Queue<Integer> windowQueue = new LinkedList<>();

    // 创建任务类
    static class MyTask implements Runnable {
        // 任务数据
        private Object data;
        // 重试纪元(重试次数)
        private int retryEpoch;

        @Override
        public void run() {
            // 处理任务
        }

        public Object getData() {
            return data;
        }

        public void setData(Object data) {
            this.data = data;
        }

        public int getRetryEpoch() {
            return retryEpoch;
        }

        public void setRetryEpoch(int retryEpoch) {
            this.retryEpoch = retryEpoch;
        }
    }

    // 创建线程池
    ThreadPoolExecutor executor = new ThreadPoolExecutor(10, 20, 6000, TimeUnit.SECONDS, new LinkedBlockingDeque<>(),
            // 自定义线程池拒绝策略
            new RejectedExecutionHandler() {
                @Override
                public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
                    MyTask task = (MyTask) r;

                    // 任务拒绝时的处理逻辑
                    System.out.println("存入DB或者发送到MQ等待重试，此时retry_epoch为0！");
                    task.setRetryEpoch(0);


                    /**
                     * 为什么滑动时间窗口写在此处，有什么作用，具体是为了什么：
                     * 1. 如果有100万条数据，被拒绝了100条，完全可以直接走重试机制，因为拒绝的条数占比非常小；
                     * 2. 但是100万条数据，被拒绝了10万条甚至更多，那么说明线程池参数定义有问题，或者是其他性能的问题，此时不建议走重试机制，
                     * 而是直接走报警进行人工干预；
                     * 3. 滑动时间窗口就是为此而诞生的：一段时间内，如果有n条以内的数据被拒绝，那么我们可以直接进行重试，超过n条数据就发送告警，
                     * 进行人工干预，用这种方式来避免雪上加霜的重试以及没有必要的告警；
                     */

                    // 滑动时间窗口判断是否需要进行告警，以便进行人工干预
                    // 设置时间窗口
                    int windowSize = 3;
                    // 获取当前时间戳
                    long currentTimestamp = System.currentTimeMillis();

                    // 如果队列不为空且最早入队的元素已经超过了指定的时间窗口大小（单位为秒），则发送警告，人工干预，并移除最老的元素
                    if (!windowQueue.isEmpty() && currentTimestamp - windowQueue.peek() > windowSize * 1000) {
                        // 如果单位时间内，拒绝条数超过设定的阈值，则发送告警，进行人工干预
                        if (windowQueue.size() > 10000) {
                            // 发送告警，进行人工干预
                        }

                        // 删除队列头
                        windowQueue.poll();
                    }

                    // 将新元素加入到队列尾部
                    windowQueue.offer((int) (System.currentTimeMillis()));
                }
            }) {

        @Override
        protected void beforeExecute(Thread t, Runnable r) {
            // 执行任务前
            this.getTaskCount();
            this.getActiveCount();
            this.getCompletedTaskCount();
            // 获取线程池的各项指标，将这些指标信息发送到mq中，消费端通过指标生成监控图，以此来了解线程池的执行情况以及它的负载问题
            // 还可以进行一些其他操作，比如一些日志记录，一些类的转换

        }

        @Override
        protected void afterExecute(Runnable r, Throwable t) {
            // 任务执行后
            // 进行重试的逻辑，会去看滑动窗口的queue是否有数据，如果有数据至少能证明有被拒绝的数据，查看当前线程池的queue里有多少没有处理的任务，
            // 用此来判断我们是否可以开始重试任务，并且在每次重试任务的时候，为重试纪元加一，超过重试的次数就进行报警，进行人工的干预
            if (!windowQueue.isEmpty() && this.getQueue().size() < 1000) {
                // 重试被拒绝的任务
                System.out.println("从DB或者Mq读取被拒绝的任务");
                System.out.println("判断任务重试次数，如果超过某个阈值，则触发告警！");
                System.out.println("将任务重新添加到线程池 executor.execute(MyTask)，并将retry_epoch + 1");

                // retryTasks模拟为从DB或MQ里读取的拒绝的任务列表
                LinkedList<MyTask> retryTasks = new LinkedList<>();
                retryTasks.forEach(myTask -> {
                    int retryEpoch = myTask.getRetryEpoch();
                    // 比如说当前任务被拒绝3次
                    if (retryEpoch > 3) {
                        // 发送告警
                        return;
                    }
                    myTask.setRetryEpoch(retryEpoch + 1);
                    this.execute(myTask);
                });
            }
        }
    };
    
    // 注：
    // 问题1：做线程池的监控与告警，为什么不使用hippo4j？而且hippo4j还能动态调整线程池参数？
    // hippo4j需要配置中心支持；hippo4j是重量级框架，目前项目需求仅能够使用到它的监控模块，后续涉及到更复杂的场景，需要动态线程池参数调配等，
    // 会考虑接入hippo4j；
    // 问题2：频繁的发送线程池消息，对性能有影吗？
    // 监控消息会异步的发送到另一组MQ里，监控部分会接收这部分数据进行处理，对性能上影响不太大；也考虑过频繁发送对性能带来的影响，打算通过一些
    // 判断条件来进行监控告警的发送，比如线程池的queue容量堆积达到50%，才会发送告警，以此来减少发送的频率；
    // 问题3：afterExecute发放里，每处理完一个任务就进行判断，是不是太过频繁？
    // 问题4：滑动时间窗口的原理。
}

```

## 2.用户请求合并

> 参考链接：[Java项目亮点第二期，因为项目亮点头疼的伙伴，拿走不谢](https://www.bilibili.com/video/BV1mg4y1S7P2)

**请求合并设计**

适用场景：批量插入或者信息单字段查询；

**减少DB的访问频次**

**通过配置中心进行服务的升降级**

**合并请求前后使用模版方法模式**

![img.png](images/concurrent-design-user-request-merge.png)

合并请求示例代码实现：
```java
package com.only4play;

import jakarta.annotation.PostConstruct;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

public abstract class AbstractMergeRequestService {

    // 配置中心合并请求开关
    private boolean enableMerge;

    // 封装的请求
    public static class WrapRequest {
        String code;
        CompletableFuture<Map<String, Object>> future;

        public WrapRequest() {
        }

        public WrapRequest(String code, CompletableFuture<Map<String, Object>> future) {
            this.code = code;
            this.future = future;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public CompletableFuture<Map<String, Object>> getFuture() {
            return future;
        }

        public void setFuture(CompletableFuture<Map<String, Object>> future) {
            this.future = future;
        }
    }

    // 创建用来存放请求的区域，将请求封装成一个Request对象
    // 可以用redis或其他组件
    private LinkedBlockingQueue<WrapRequest> wrapRequestMergeRegion = new LinkedBlockingQueue<>();

    // query方法，通过判断是否需要合并请求
    public Object query(WrapRequest wrapRequest) throws ExecutionException, InterruptedException {
        if (enableMerge) {
            // 将request添加到 存放请求的位置
            wrapRequestMergeRegion.add(wrapRequest);
            // 阻塞，直到结果返回
            return wrapRequest.getFuture().get();
        } else {
            // 单独处理逻辑
            return singleExecute(wrapRequest);
        }
    }

    // 使用定时任务批量执行合并请求
    @PostConstruct
    public void init() {
        // 在init方法中初始化一个定时任务，去定时执行我们的批量任务
        // 合并请求
        // 拆分结果
        ScheduledExecutorService poolExecutor = new ScheduledThreadPoolExecutor(1);
        poolExecutor.scheduleAtFixedRate(() -> {
            int size = wrapRequestMergeRegion.size();
            // 如果没有请求直接返回
            if (size == 0) {
                return;
            }

            // 获取当前queue中的所有请求
            List<WrapRequest> list = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                WrapRequest wrapRequest = wrapRequestMergeRegion.poll();
                list.add(wrapRequest);
            }

            // 模版方法1: 合并请求并批量执行
            Object response = mergeRequestAndExecute(list);

            // 模版方法2: 分发结果给不同的请求的future
            splitResponse(response, list);
        }, 0, 200, TimeUnit.MILLISECONDS);
    }

    protected abstract Object singleExecute(WrapRequest wrapRequest);

    // 由不同的处理逻辑来实现
    protected abstract Object mergeRequestAndExecute(List<WrapRequest> list);

    // 由不同的处理逻辑来实现
    protected abstract void splitResponse(Object response, List<WrapRequest> list);
}

```

**Redis和请求合并：**

方案1: 引入redis缓存

优点：实现简单

缺点：（1）无法确定热定数据；（2）数据预热方案较难确定；（3）过期时间确定困难；（4）需要保证数据库和redis的一致性；

方案2: 请求合并

优点：无需考虑缓存一致性的问题、数据IO减少

缺点：（1）实现复杂；（2）定时任务周期和批次数量需要调整到合适的参数；（3）netty的worker线程池会被阻塞；（4）更新的接口需要手动保证事务；（5）请求可能出现延迟；

// https://developer.aliyun.com/article/1203551