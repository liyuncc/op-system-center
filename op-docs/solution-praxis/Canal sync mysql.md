---
title: 基于Canal实现MySQL数据库数据增量同步
date: 2025-01-08 16:00
---

# 基于Canal实现MySQL数据库数据增量同步

## 零、mysql增量数据同步工具

在MySQL增量数据同步的场景中，通常有以下几种工具可以选择，依据不同的需求来进行数据同步。增量数据同步是指只同步发生变化的数据，而不是每次都全量同步，这样可以提高同步效率并减少系统负担。

以下是常见的MySQL增量数据同步工具：

### 1、MySQL的binlog（二进制日志）

**简介:** MySQL的二进制日志（binlog）是记录所有数据库修改操作的日志文件。通过解析binlog日志，您可以提取出增量数据并同步到其他系统或数据库。

**使用方式:** 可以使用mysqlBinlog工具来提取binlog文件中的增量数据，也可以通过开发定制的程序来解析binlog并同步数据。

**工具:**

- Maxwell：Maxwell是一个开源的工具，它可以解析MySQL的binlog并将数据同步到Kafka、Kinesis、Elasticsearch等。
- Debezium：Debezium是一个基于Apache Kafka的分布式平台，专门用于捕获数据库变更并将增量数据推送到下游系统。
- Canal：阿里巴巴开源的一个MySQL binlog增量同步工具，可以通过解析binlog日志实现MySQL的增量数据同步。

**优缺点:**

- 优点：高效，能够实时捕获数据变更。
- 缺点：需要一定的配置和对MySQL的高级功能（如binlog）的理解。

### 2、SymmetricDS

**简介：** SymmetricDS是一个开源的数据库同步工具，它支持多种数据库，包括MySQL。SymmetricDS能够同步增量数据，支持双向同步和多源同步。

**原理：**

通过触发器模式同步时，是将数据库的变化记录到某个系统表中，然后在客户端建立缓冲，并定期将变化push到接收端，接收端和发送端建立通道，并定期pull新的数据变化，并在目标数据库回放。--这个过程会有一定的延迟，和性能影响，但是只要数据库支持触发器，都可以使用SymmetricDS来做数据库同步。
另外一种模式记录SQL，并回放SQL，好像用很少。
还在开发的是解析日志，通过回放日志来实现同步，这个最难，基本相当于流复制的速度了，但是各种数据源的日志格式不一样，造成了解析日志的工作量相当的大，有点难实现。

**优缺点：**

- 优点：支持多种数据库间的同步，包括跨数据中心的同步，支持增量数据同步和复杂的同步拓扑结构。
- 缺点：配置相对复杂，适用于对数据同步有较高要求的场景。

### 3、DataX（阿里巴巴开源数据同步工具）

**简介：** DataX是阿里巴巴开源的一个大数据同步工具，它支持从多种数据源进行增量同步，是阿里云DataWorks数据集成
的开源版本，在阿里巴巴集团内被广泛使用的离线数据同步工具/平台。

**优缺点：**

- 优点：支持多种数据源，适用于批量同步，能够对增量数据进行定时同步。
- 缺点：配置略微复杂，适合数据量较大或涉及多个数据源的同步场景。

### 4、Tungsten Replicator

**简介：** Tungsten Replicator是一个高效的MySQL数据库同步工具，支持增量数据同步和异构数据库之间的同步。

**优缺点：**

- 优点：支持实时数据复制和高可用性。
- 缺点：配置稍微复杂，适合大规模同步场景。

### 5、MyDumper 和 MyLoader

**简介：** MyDumper是一个开源的MySQL数据库备份工具，能够以增量方式进行备份，并支持将数据恢复到另一台服务器。

**优缺点：**

- 优点：高效的增量备份和恢复，能够快速同步数据。
- 缺点：通常用于备份恢复场景，不是典型的同步工具。

### 总结

- 如果你需要实时的增量同步，Canal、Maxwell、**Debezium**等基于binlog的工具非常适合。
- 如果你需要一个全功能的同步工具，支持跨数据库、多源同步，可以考虑**SymmetricDS或DataX**。

## 一、基于MySQL的binlog（二进制日志）的CDC工具选择

### CDC技术

CDC全称叫：change data capture，是一种基于数据库数据变更的事件型软件设计模式。

### 基于Binlog的CDC

Binlog是MySQL 3.23.14引进的，它包含所有的描述数据库修改的事件——DML(增删改)、DDL(表结构定义与修改)操作。

与InnoDB中的redo-log、undo-log不同，binlog和slow_query_log一样是server层的日志，所以InnoDB和MyISAM等各种存储引擎的数据修改都会记录到这个日志中。

### Canal vs. Debezium vs. MaxWell

#### Maxwell

**概述**

Maxwell 是另一个提供 CDC 功能的开源工具。它读取 MySQL binlog 并将行更新以 JSON 格式发送到 Kafka、Kinesis 或其他流媒体平台。Maxwell
以其简单性和易于设置而闻名。它以简约的设计和轻量级架构脱颖而出，使其成为小型应用程序或资源有限的团队的绝佳选择。

![canal-sync-mysql-maxwell-summary.png](images/canal-sync-mysql-maxwell-summary.png)

**优点**

- 实时性：Maxwell能够实时地捕获数据库的增量数据变更，确保数据同步的及时性。
- 支持多种数据类型：Maxwell 支持多种数据类型，包括 JSON、AVRO、CSV 等，可以根据需要自由选择。
- API和命令行工具支持：Maxwell提供了友好的API和命令行工具，用户可以通过简单的命令就能方便地完成binlog解析和数据输出。
- 可靠性：通过解析数据库的日志，Maxwell确保数据同步的准确性和一致性。
- 高性能：Maxwell使用高效的日志解析算法，可以处理大量的数据库操作，并保持较低的延迟。

**缺点**

- 依赖 binlog：Maxwell 需要依赖 MySQL 的 binlog 进行数据解析，如果 MySQL 的 binlog 出现问题，Maxwell 也会受到影响。
- MySQL 版本要求：Maxwell 只支持 MySQL 5.5 及以上版本，对于低版本的 MySQL 不支持。
- 学习成本较高：使用 Maxwell 需要一定的数据库和日志解析知识，对于初次接触的用户来说，可能需要一定的学习和理解。

#### Debezium

**概述**

Debezium 是一个用于捕获变更数据的开源分布式平台。它捕获数据库表的行级更改，并近乎实时地将其传递给下游应用程序。Debezium
旨在实现高度可扩展和强大的功能，为不同的数据库系统（如 MySQL、PostgreSQL、MongoDB 等）提供各种连接器。它能够与 Kafka
无缝集成，因此成为许多寻求高效数据复制和微服务架构的组织的首选。

![canal-sync-mysql-debezium-summary.png](images/canal-sync-mysql-debezium-summary.png)

**优点**

实时性：Debezium能够实时捕获数据库的变更事件，并将其转化为实时的数据流，确保数据同步的及时性。
可靠性：通过连接到数据库的事务日志，Debezium能够确保数据变更的准确性和一致性。
灵活性：Debezium 支持多种数据库，包括 MySQL、PostgreSQL、MongoDB 等，可以适应不同的数据库环境和需求。
可扩展性：Debezium的架构设计支持水平扩展，可以处理大规模的数据变更。

**缺点**

配置复杂性：Debezium 的配置相对复杂，需要了解数据库的事务日志和相关配置参数。
依赖性：Debezium 依赖于数据库的事务日志，需要确保数据库日志的可靠性和稳定性。
性能影响：由于Debezium 需要实时监控数据库的变更日志，可能会对数据库的性能产生一定的影响。

#### Canal

Canal 是一个开源的数据库数据同步工具，主要用于实时获取数据库的增量数据变更，并将这些变更传递给其他应用或系统。它的原理是通过解析数据库的
binlog 日志，捕获数据库的增删改操作，并将这些操作转化为可读的数据格式，比如 json，以便其他应用程序进行消费和处理。

![canal-sync-mysql-canal-summary.png](images/canal-sync-mysql-canal-summary.png)

**优点**

- 实时性：Canal 可以实时地捕获数据库的增量数据变更，保证了数据同步的及时性。
- 灵活性：Canal 支持配置多个数据库和表进行同步，可以根据需求进行灵活的配置和管理。
- 可靠性：Canal 通过解析数据库的日志，确保了数据同步的准确性和一致性。
- 高性能：Canal 使用高效的日志解析算法，可以处理大量的数据库操作，并保持较低的延迟。

**缺点**

- 学习成本较高：使用 Canal 需要一定的数据库和日志解析的知识，对于初次接触的用户来说，可能需要一定的学习和理解。
- 配置复杂性：在处理复杂的数据库同步场景时，配置和管理 Canal 可能会变得复杂，需要仔细设计和调试。
- 依赖性：Canal 需要依赖于数据库的日志服务，如 MySQL 的 binlog，这可能增加了部署和维护的复杂性。
- 局限性：想要使用 canal，必须要对相关账号开启日志复制权限，这对于对数据控制比较严格的企业来说是比较难以实现的。

#### 总结

由于文档、时间、资源、可扩展等因素，综合考虑选用canal。

## 二、canal简介

### 工作原理

#### MySQL主备复制原理

- MySQL master 将数据变更写入二进制日志( binary log, 其中记录叫做二进制日志事件binary log events，可以通过 show binlog
  events 进行查看)
- MySQL slave 将 master 的 binary log events 拷贝到它的中继日志(relay log)
- MySQL slave 重放 relay log 中事件，将数据变更反映它自己的数据

#### canal 工作原理

- canal 模拟 MySQL slave 的交互协议，伪装自己为 MySQL slave ，向 MySQL master 发送dump 协议
- MySQL master 收到 dump 请求，开始推送 binary log 给 slave (即 canal )
- canal 解析 binary log 对象(原始为 byte 流)
- 发送到存储目的地，比如MySQL，Kafka，Elastic Search等等

![canal-sync-mysql-canal-sync-theory.png](images/canal-sync-mysql-canal-sync-theory.png)

![canal-sync-mysql-canal-sync-theory-with-distributed.png](images/canal-sync-mysql-canal-sync-theory-with-distributed.png)

## 三、实际案例

### 需求背景

目前有两个内网Mysql数据库，因不同的业务要求，需要两个库同时作为活跃库，需要做到短时间完成增量数据的同步。

两个业务库：data_online、ynsw_instrument；

源数据库：172.xxx.xxx.xxx:3306，两个源数据库（data_online、ynsw_instrument），需要通过跳板机182.xxx.xxx.xxx进行访问；

目标数据库1：10.xxx.xxx.189:3306，目标库（data_online）,需要通过跳板机202.xxx.xxx.xxx进行访问；

目标数据库2：10.xxx.xxx.225:3306，目标库（ynsw_instrument）,需要通过跳板机202.xxx.xxx.xxx进行访问；

**数据量**

源数据库，数据增长大概每天30万上下，数据采频率为5分中采集一次，粗略计算每五分钟写入数据为1100条左右。

**网络通信**

两台数据库都是部署在内网，访问只能通过外网跳板机进行访问，因此网络通信也需要进行考虑。

### 方案选择

全量数据同步，对于数据量不大，写入频率不高的表使用sql文件或者csv文件进行数据迁移，对于巨量数据表可以使用中间件进行同步。

增量数据同步，首先想到的是基于mysql的binlog进行监听实现数据同步，其次是使用一些中间件进行数据同步。

**网络通信**

网络通信方面，没有其他更好的选择，采用ssh端口映射进行内网数据库访问。在外网服务器端口开发限制的条件下，尽可能地减少端口的开放。

**同步方案**

首先需要同步的数据库，主要数据表只有一个；

其次，由于网络限制，需要进行ssh端口映射；且沟通后发现，由于网络策略限制，只能是目标库的跳板机进行端口开发，就意味着只能在源数据库的跳板机上做端口映射。

由于以上限制，基于binlog的直接配置主从同步的方案就开展不了。

| 名称       | 原理简介                                                                                              | 实现描述                                                         | 备注                    |
|----------|---------------------------------------------------------------------------------------------------|--------------------------------------------------------------|-----------------------|
| canal    | 基于mysql的binlog实现，启动一个连接服务模拟slave数据库，向主库进行binlog事件load，解析事件                                        | 需要启动两个服务，一个是模拟slave的连接服务，另一个是client服务，将canal解析到的数据操作事件同步到目标库 | 需要进行手动编码，实现写入目标库的同步逻辑 |
| logstash | 三个主要组件构成：Input、Filter和Output。Input组件负责从各种数据源中读取数据，Filter组件对读取到的数据进行过滤和处理，Output组件则将处理后的数据发送到指定的目标 | 需要部署logstash服务，进行相关配置，通过条件查询的方式实现数据增量同步                      | 无需手动编码，但需要进行相关的配置文件配置 |

敲定方案为：

全量同步：文件导入/导出少量数据表 + logstash同步巨量数据表

增量同步：canal实现增量同步

### 方案设计

![canal-sync-mysql-solution-design.png](images/canal-sync-mysql-solution-design.png)

### 方案实现

#### ssh端口映射

**在182.xxx.xxx.xxx机器上做端口映射，可通过访问本地3306端口访问10.xxx.xxx.189:3306。**

```shell
ssh -L 3306:10.xxx.xxx.189:3306 root@202.xxx.xxx.xxx -N -f
```

**在182.xxx.xxx.xxx机器上做端口映射，可通过访问本地3307端口访问10.xxx.xxx.225:3306。**

```shell
ssh -L 3307:10.xxx.xxx.225:3306 root@202.xxx.xxx.xxx -N -f
```

#### 全量同步

##### 使用文本文件同步数据量较小的表数据

**mysqldump**

> [mysqldump使用手册](https://mysql.net.cn/doc/refman/8.0/en/mysqldump.html)

mysqldump客户端实用程序执行 逻辑备份，生成一组 SQL 语句，可以执行这些语句以重现原始数据库对象定义和表数据。它转储一个或多个MySQL数据库以进行
备份或传输到另一台SQL服务器。mysqldump命令还可以生成CSV、其他分隔文本或XML格式的输出 。

```shell
mysqldump --skip-lock-tables --no-create-info --disable-keys --extended-insert --ignore-table=ynsw_instrument.sys_oper_log --replace -u root --host=172.xxx.xxx.xxx --port=3306 ynsw_instrument
```

| keyword            | describe                                              |
|--------------------|-------------------------------------------------------|
| --skip-lock-tables | 跳过表锁定                                                 |
| --no-create-info   | 不要编写重新创建每个转储表的 CREATE TABLE 语句                        |
| --disable-keys     | 使得加载转储文件更快，因为索引是在插入所有行之后创建的。此选项仅对表的非唯一索引有效。           |
| --extended-insert  | 使用包含多个 VALUES列表的多行语法 编写语句。这会导致转储文件更小，并在重新加载文件时加快插入速度。 |
| --ignore-table     | 不要转储给定的表，该表必须使用数据库名和表名指定。要忽略多个表，请多次使用此选项。此选项也可用于忽略视图。 |
| --replace          | 写REPLACE陈述而不是INSERT 陈述。                               |

**将转储文件加载回服务器：**

> mysql db_name < backup-file.sql

```shell
mysql -u username -p --host=172.xxx.xxx.xxx target_database < source_database_dump.sql

```

##### 使用logstash同步大表数据

**部署logstash**

https://elkguide.elasticsearch.cn/logstash/get-start/install.html

**添加mysql连接jar**

连接mysql需要添加mysql驱动，需下载对应的jar包，在配置是使用该jar进行数据库连接

**编写配置文件**

> cd config && touch logstash-sync-nsy_rtrun.config

**logstash-sync-nsy_rtrun.config**

```text
input {
  jdbc {
    jdbc_driver_library => "/opt/logstash/logstash-8.17.0/logstash-core/lib/jars/mysql-connector-j-8.0.33.jar"
    jdbc_driver_class => "com.mysql.jdbc.Driver"
    jdbc_connection_string => "jdbc:mysql://172.xxx.xxx.xxx:3306/data_online?characterEncoding=utf8&useSSL=false&serverTimezone=Asia/Shanghai"
    jdbc_user => "xxx"
    jdbc_password => "xxx"
    jdbc_paging_enabled => "true"
    jdbc_page_size => "50000"
    jdbc_default_timezone => "Asia/Shanghai"
    lowercase_column_names => "false"
    parameters => { "sql_last_value" => "TM" }
    statement_filepath => "/opt/logstash/logstash-8.17.0/config/mysql/query-nsy_rtrun.sql"
    # statement => "SHOW BINLOG EVENTS"
    schedule => "*/5 * * * *"
    use_column_value => true
    tracking_column => "TM"
    tracking_column_type => "timestamp"
    last_run_metadata_path => "/opt/logstash/logstash-8.17.0/config/mysql/last_run_metadata"
  }
}

filter {
  json {
    source => "message"
  }
}

output {
  jdbc {
    driver_jar_path => "/opt/logstash/logstash-8.17.0/logstash-core/lib/jars/mysql-connector-j-8.0.33.jar"
    driver_class => "com.mysql.jdbc.Driver"
    connection_string => "jdbc:mysql://localhost:3306/data_online"
    username => "xxx"
    password => "xxx"
    statement => ["INSERT INTO data_online.nsy_rtrun (STCD, DATATYPE, TM, DA, DP, DI, DQ, INSERTTM) VALUES(?, ?, ?, ?, ?, ?, ?, ?)", "STCD", "DATATYPE", "TM", "DA", "DP", "DI", "DQ", "INSERTTM"]
  }
}
```

> cd config && mkdir mysql && touch query-nsy_rtrun.sql && touch last_run_metadata

**query-nsy_rtrun.sql**

```text
SELECT
        nr.STCD,
        nr.DATATYPE,
        nr.TM,
        nr.DA,
        nr.DP,
        nr.DI,
        nr.DQ,
        nr.INSERTTM
FROM
        data_online.nsy_rtrun nr 
WHERE  
        nr.TM >= :sql_last_value
ORDER BY 
        nr.STCD,
        nr.DATATYPE,
        nr.TM
```

**last_run_metadata**

```text
--- !ruby/object:DateTime '2025-01-06 03:45:00.000000000 Z'
```

**后台运行同步任务**

```shell
cd /logstash
nohup bin/logstash -f config/logstash-sync-xxx.config &
```

#### 增量同步（基于canal）

##### MySQL源数据库配置

使用binlog需要预先对mysql服务进行配置。

**1、创建单独的用户，并授予需要的权限**

> MySQL提供的权限：https://dev.mysql.com/doc/refman/8.0/en/privileges-provided.html

```shell
mysql> CREATE USER 'replica_user'@'%' IDENTIFIED BY 'password';
mysql> GRANT SELECT, RELOAD, SHOW DATABASES, REPLICATION SLAVE, REPLICATION CLIENT ON *.* TO 'replica_user' IDENTIFIED BY 'password';
mysql> FLUSH PRIVILEGES;
```

![canal-sync-mysql-mysql-config-privileges.png](images/canal-sync-mysql-mysql-config-privileges.png)

需要几个关键权限的作用：

| keyword            | describe                                                                                  |
|--------------------|-------------------------------------------------------------------------------------------|
| SELECT             | SELECT查询权限。只被用在初始化阶段。                                                                     |
| REPLICATION SLAVE  | 常用于建立复制时所需要用到的用户权限，也就是slave server必须被master server授权具有 该权限的用户，才能通过该用户复制。读取MySQL binlog。   |
| REPLICATION CLIENT | 不可用于建立复制，有该权限时，只是多了可以使用如"SHOW SLAVE STATUS"、"SHOW MASTER STATUS"等命令。配合REPLICATION SLAVE使用 |

**2、开启MySQL服务的binlog功能**

```text
[mysqld]
server-id = 1
log-bin = /var/lib/mysql/mysql-bin
binlog_format = ROW
binlog_row_image = FULL
expire_logs_days = 10

binlog-do-db = data_online
binlog-do-db = ynsw_instrument
```

![canal-sync-mysql-mysql-config-master.png](images/canal-sync-mysql-mysql-config-master.png)

各项配置的作用：

| keyword          | describe                                         |
|------------------|--------------------------------------------------|
| server-id        | 在MySQL集群中每个server和replication的 server-id 必须是唯一的。 |
| log_bin          | binlog文件的前缀                                      |
| binlog_format    | binlog-format 必须设置成ROW模式。                        |
| binlog_row_image | binlog_row_image 必须设置成FULL。ROW模式下binlog需要记录所有的列。 |
| expire_logs_days | binlog的过期时间。默认位 0, 意味着不会自动删除。这个值可根据自己的环境需求进行设置。  |

> mysql的binlog有三种模式：
>
> STATEMENT模式只记录SQL语句，从节点通过执行同步过来的sql在从库中再执行一遍。STATEMENT模式的问题是有些语句(比如update t
> set num=num+1 limit 1)可能会产生不一致性，而且STATEMENT模式下sql发给异构系统将会无法使用。
>
>ROW模式会直接复制修改的数据行，但是有可能会导致日志量过大，比如执行一条update t set
> num=num+1，修改了一万行就会有一万行日志，肯定没有STATEMENT模式来的快。
>
>MIXED模式，则将两者结合，默认情况下使用statement，某些情况会切换为基于行的复制。

**3、改了配置文件之后，重启MySQL**

查看是否打开binlog模式

```shell
show variables like 'log_bin';
```

![canal-sync-mysql-mysql-config-show-binlog.png](images/canal-sync-mysql-mysql-config-show-binlog.png)

查看binlog日志文件列表

```shell
show binary logs;
```

![canal-sync-mysql-mysql-config-show-binlog-list.png](images/canal-sync-mysql-mysql-config-show-binlog-list.png)

查看二进制日志文件名和位置

```shell
show master status;
```

![canal-sync-mysql-mysql-config-show-master-status.png](images/canal-sync-mysql-mysql-config-show-master-status.png)

至此， MySQL源数据库配置基本已完成。

##### 部署配置canal-deploy

> 官方下载地址：https://github.com/alibaba/canal/releases

![canal-sync-mysql-canal-deploy-download.png](images/canal-sync-mysql-canal-deploy-download.png)

解压

```shell
tar -zxvf canal.deployer-1.1.8.tar.gz -C /opt/canal-deployer
```

同步为MySQL数据库间的数据同步，所以此处不需要修改 canal.properties 配置文件，只需修改 instance.properties 即可

```shell
vi conf/example/instance.properties
```

```text
#################################################
## mysql serverId , v1.0.26+ will autoGen
# canal.instance.mysql.slaveId=0

# enable gtid use true/false
canal.instance.gtidon=false

# rds oss binlog
canal.instance.rds.accesskey=
canal.instance.rds.secretkey=
canal.instance.rds.instanceId=

# position info
# 数据库地址
canal.instance.master.address=172.xxx.xxx.xxx:3306
# binlog日志名称
canal.instance.master.journal.name=mysql-bin.000002
# mysql主库链接时起始的binlog偏移量
canal.instance.master.position=35760
canal.instance.master.timestamp=
canal.instance.master.gtid=

# ssl
#canal.instance.master.sslMode=DISABLED
#canal.instance.master.tlsVersions=
#canal.instance.master.trustCertificateKeyStoreType=
#canal.instance.master.trustCertificateKeyStoreUrl=
#canal.instance.master.trustCertificateKeyStorePassword=
#canal.instance.master.clientCertificateKeyStoreType=
#canal.instance.master.clientCertificateKeyStoreUrl=
#canal.instance.master.clientCertificateKeyStorePassword=

# table meta tsdb info
canal.instance.tsdb.enable=true
#canal.instance.tsdb.url=jdbc:mysql://127.0.0.1:3306/canal_tsdb
#canal.instance.tsdb.dbUsername=canal
#canal.instance.tsdb.dbPassword=canal

#canal.instance.standby.address =
#canal.instance.standby.journal.name =
#canal.instance.standby.position =
#canal.instance.standby.timestamp =
#canal.instance.standby.gtid=

# username/password
# 在MySQL服务器授权的账号密码
canal.instance.dbUsername=replica_user
canal.instance.dbPassword=xxx
canal.instance.connectionCharset = UTF-8
# enable druid Decrypt database password
canal.instance.enableDruid=false
#canal.instance.pwdPublicKey=MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBALK4BUxdDltRRE5/zXpVEVPUgunvscYFtEip3pmLlhrWpacX7y7GCMo2/JM6LeHmiiNdH1FWgGCpUfircSwlWKUCAwEAAQ==

# table regex
# .*\\..*表示监听所有表 也可以写具体的表名，用，隔开
canal.instance.filter.regex=.*\\..*
# table black regex
canal.instance.filter.black.regex=mysql\\.slave_.*
# table field filter(format: schema1.tableName1:field1/field2,schema2.tableName2:field1/field2)
#canal.instance.filter.field=test1.t_product:id/subject/keywords,test2.t_company:id/name/contact/ch
# table field black filter(format: schema1.tableName1:field1/field2,schema2.tableName2:field1/field2)
#canal.instance.filter.black.field=test1.t_product:subject/product_image,test2.t_company:id/name/contact/ch

# mq config
canal.mq.topic=example
# dynamic topic route by schema or table regex
#canal.mq.dynamicTopic=mytest1.user,topic2:mytest2\\..*,.*\\..*
canal.mq.partition=0
# hash partition config
#canal.mq.enableDynamicQueuePartition=false
#canal.mq.partitionsNum=3
#canal.mq.dynamicTopicPartitionNum=test.*:4,mycanal:6
#canal.mq.partitionHash=test.table:id^name,.*\\..*
#
# multi stream for polardbx
canal.instance.multi.stream.on=false
#################################################
```

其余暂未用到消息队列，注释掉配置

启动deploy，canal-deployer/bin

```shell
sh startup.sh
```

查看日志，启动成功
![canal-sync-mysql-canal-deploy-show-running-logs.png](images/canal-sync-mysql-canal-deploy-show-running-logs.png)

##### 部署配置canal-adapter

**安装部署**

> 官方下载地址：https://github.com/alibaba/canal/releases

![canal-sync-mysql-canal-adapter-download.png](images/canal-sync-mysql-canal-deploy-download.png)

解压

```shell
tar -zxvf canal.adapter-1.1.8.tar.gz -C /opt/canal-adapter
```

**修改application.yml文件，canal-adapter/conf**

```text
server:
  port: 8081
spring:
  jackson:
    date-format: yyyy-MM-dd HH:mm:ss
    time-zone: GMT+8
    default-property-inclusion: non_null
canal.conf:
  mode: tcp
  flatMessage: true
  zookeeperHosts:
  syncBatchSize: 1000
  retries: 0
  timeout:
  accessKey:
  secretKey:
  consumerProperties:
    # canal tcp consumer
    # 修改位置：Canal deployer所在主机IP（目前deploy，adapter都部署在一台机器上）
    canal.tcp.server.host: localhost:11111
    canal.tcp.zookeeper.hosts:
    canal.tcp.batch.size: 500
    canal.tcp.username:
    canal.tcp.password:
    # kafka consumer
    # kafka.bootstrap.servers: 127.0.0.1:9092
    # kafka.enable.auto.commit: false
    # kafka.auto.commit.interval.ms: 1000
    # kafka.auto.offset.reset: latest
    # kafka.request.timeout.ms: 40000
    # kafka.session.timeout.ms: 30000
    # kafka.isolation.level: read_committed
    # kafka.max.poll.records: 1000
    # rocketMQ consumer
    # rocketmq.namespace:
    # rocketmq.namesrv.addr: 127.0.0.1:9876
    # rocketmq.batch.size: 1000
    # rocketmq.enable.message.trace: false
    # rocketmq.customized.trace.topic:
    # rocketmq.access.channel:
    # rocketmq.subscribe.filter:
    # rabbitMQ consumer
    # rabbitmq.host:
    # rabbitmq.virtual.host:
    # rabbitmq.username:
    # rabbitmq.password:
    # rabbitmq.resource.ownerId:
  # 修改位置：添加源库配置信息，此处为同步同库下所有表信息
  srcDataSources:
    defaultDS::
      url: jdbc:mysql://172.xxx.xxx.xxx:3306/data_online?useUnicode=true&characterEncoding=utf8&autoReconnect=true&useSSL=false
      username: replica_user
      password: xxx
    ynswInstrumentDS:
      url: jdbc:mysql://172.xxx.xxx.xxx:3307/ynsw_instrument?useUnicode=true&characterEncoding=utf8&autoReconnect=true&useSSL=false
      username: replica_user
      password: xxx
  canalAdapters:
  - instance: example # canal instance Name or mq topic name
    groups:
    - groupId: g1
      outerAdapters:
      - name: rdb
        key: mysql-189-data_online
        properties:
          jdbc.driverClassName: com.mysql.jdbc.Driver
          jdbc.url: jdbc:mysql://192.168.91.135:3306/data_online?useUnicode=true&characterEncoding=utf8&autoReconnect=true&useSSL=false
          jdbc.username: root
          jdbc.password: xxx
      - name: rdb
        key: mysql-225-ynsw_instrument
        properties:
          jdbc.driverClassName: com.mysql.jdbc.Driver
          jdbc.url: jdbc:mysql://192.168.91.135:3307/ynsw_instrument?useUnicode=true&characterEncoding=utf8&autoReconnect=true&useSSL=false
          jdbc.username: root
          jdbc.password: xxx
#      - name: rdb
#        key: oracle1
#        properties:
#          jdbc.driverClassName: oracle.jdbc.OracleDriver
#          jdbc.url: jdbc:oracle:thin:@localhost:49161:XE
#          jdbc.username: mytest
#          jdbc.password: m121212
#      - name: rdb
#        key: postgres1
#        properties:
#          jdbc.driverClassName: org.postgresql.Driver
#          jdbc.url: jdbc:postgresql://localhost:5432/postgres
#          jdbc.username: postgres
#          jdbc.password: 121212
#          threads: 1
#          commitSize: 3000
#      - name: hbase
#        properties:
#          hbase.zookeeper.quorum: 127.0.0.1
#          hbase.zookeeper.property.clientPort: 2181
#          zookeeper.znode.parent: /hbase
#      - name: es
#        hosts: 127.0.0.1:9300 # 127.0.0.1:9200 for rest mode
#        properties:
#          mode: transport # or rest
#          # security.auth: test:123456 #  only used for rest mode
#          cluster.name: elasticsearch
#        - name: kudu
#          key: kudu
#          properties:
#            kudu.master.address: 127.0.0.1 # ',' split multi address
```

说明:

- 其中 outAdapter 的配置: name统一为rdb, key为对应的数据源的唯一标识需和下面的表映射文件中的outerAdapterKey对应,
  properties为目标库jdbc的相关参数

- adapter将会自动加载 conf/rdb 下的所有.yml结尾的表映射配置文件

**修改canal-adapter/conf/rdb文件夹下的yml文件**

说明：canal-adapter/conf/rdb文件夹下的yml文件可以同步数据库下的一张表，也可以同步数据库下的所有表(
此处的前提是：同步数据的数据库与主库的schema一致)

**同步数据库下的某张表，例如同步database1数据库下的user表，操作如下：**

创建配置文件：database1-user.yml

```yaml
dataSourceKey: database1        # 源数据源的key, 对应上面配置的srcDataSources中的值
destination: example            # cannal的instance或者MQ的topic
groupId: g1                     # 对应MQ模式下的groupId, 只会同步对应groupId的数据
outerAdapterKey: mysql1         # adapter key, 对应上面配置outAdapters中的key
concurrent: true                # 是否按主键hash并行同步, 并行同步的表必须保证主键不会更改及主键不能为其他同步表的外键!
dbMapping:
  database: database1                # 源数据源的database/schema
  table: user                   # 源数据源表名
  targetTable: database1.user        # 目标数据源的库名.表名
  targetPk: # 主键映射
    id: id                      # 如果是复合主键可以换行映射多个
  mapAll: true                  # 是否整表映射, 要求源表和目标表字段名一模一样 (如果targetColumns也配置了映射, 则以targetColumns配置为准)
  #targetColumns:               # 字段映射, 格式: 目标表字段: 源表字段, 如果字段名一样源表字段名可不填
  #  id:
  #  name:
  #  role_id:
  #  c_time:
  #  test1:
```

**同步数据库下所有表数据，例如：同步database2数据库下所有表数据，操作如下：**

创建配置文件：database2-user.yml

```yaml
## Mirror schema synchronize config
dataSourceKey: database2
destination: example
groupId: g1
outerAdapterKey: mysql2
concurrent: true
dbMapping:
  mirrorDb: true
  database: database2
```

data_online、ynsw_instrument配置

189-data_online.yml

```yaml
#dataSourceKey: defaultDS
#destination: example
#groupId: g1
#outerAdapterKey: mysql1
#concurrent: true
#dbMapping:
#  database: mytest
#  table: user
#  targetTable: mytest2.user
#  targetPk:
#    id: id
#  mapAll: true
#  targetColumns:
#    id:
#    name:
#    role_id:
#    c_time:
#    test1:
#  etlCondition: "where c_time>={}"
#  commitBatch: 3000 # 批量提交的大小


## Mirror schema synchronize config
dataSourceKey: defaultDS
destination: example
groupId: g1
outerAdapterKey: mysql-189-data_online
concurrent: true
dbMapping:
  mirrorDb: true
  database: data_online
```

225-ynsw_instrument.yml

```yaml
#dataSourceKey: defaultDS
#destination: example
#groupId: g1
#outerAdapterKey: mysql1
#concurrent: true
#dbMapping:
#  database: mytest
#  table: user
#  targetTable: mytest2.user
#  targetPk:
#    id: id
#  mapAll: true
#  targetColumns:
#    id:
#    name:
#    role_id:
#    c_time:
#    test1:
#  etlCondition: "where c_time>={}"
#  commitBatch: 3000 # 批量提交的大小


## Mirror schema synchronize config
dataSourceKey: ynswInsturmentDS
destination: example
groupId: g1
outerAdapterKey: mysql-225-ynsw_instrument
concurrent: true
dbMapping:
  mirrorDb: true
  database: ynsw_instrument
```

**启动deploy：canal-adapter/bin; 查看日志：canal-adapter/logs/adapter**

至此，同步数据正常工作。


--- 

参考连接：

https://github.com/alibaba/canal

https://github.com/zendesk/maxwell

https://github.com/debezium/debezium

[基于Canal实现MySQL 8.0 数据库数据同步](https://www.cnblogs.com/cndarren/p/16318728.html)

[基于Binlog的实时同步功能——debezium、canel、databus技术选型](https://blog.hufeifei.cn/2021/03/DB/mysql-binlog-parser/index.html)

[SSH 隧道（SSH Tunnel）指南](https://www.cnblogs.com/spacelit/p/17936899)

[一文彻底搞懂ssh的端口转发](https://developer.aliyun.com/article/1277485)

[Debezium vs Maxwell: Detailed Comparison of Open-source CDC Tools](https://www.upsolver.com/blog/debezium-vs-maxwell)

[Maxwell's Daemon](https://maxwells-daemon.io/)

[Debezium入门介绍](https://www.baeldung-cn.com/debezium-intro)

[常见的10种 CDC 组件和方案](https://cloud.tencent.com/developer/article/2410127)

[Elastic实战：通过logstash-input-jdbc实现mysql8.0全量/增量同步至ES7.x](https://developer.aliyun.com/article/1082068)

[基于MaxWell 实时同步 MySQL 日志 binlog 到 Kafka](https://www.cnblogs.com/wuning/p/12623512.html)

[Maxwell - 增量数据同步工具（1）](https://developer.aliyun.com/article/1532368)