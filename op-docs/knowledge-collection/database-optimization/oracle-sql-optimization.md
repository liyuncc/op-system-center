---
title: ORACLE常用SQL优化操作
date: 2024-03-11 11:13:00
---

# ORACLE常用SQL优化操作

参考链接：[手记 : Oracle 慢查询排查步骤](https://juejin.cn/post/7002490281228517413)

## 1、查询慢SQL

```shell
select *
 from (select sa.SQL_TEXT,
        sa.SQL_FULLTEXT,
        sa.EXECUTIONS "执行次数",
        round(sa.ELAPSED_TIME / 1000000, 2) "总执行时间",
        round(sa.ELAPSED_TIME / 1000000 / sa.EXECUTIONS, 2) "平均执行时间",
        sa.COMMAND_TYPE,
        sa.PARSING_USER_ID "用户ID",
        u.username "用户名",
        sa.HASH_VALUE
     from v$sqlarea sa
     left join all_users u
      on sa.PARSING_USER_ID = u.user_id
     where sa.EXECUTIONS > 0
     order by (sa.ELAPSED_TIME / sa.EXECUTIONS) desc)
 where rownum <= 50;
```

## 2、查询执行次数最多SQL

```shell
select *
 from (select s.SQL_TEXT,
        s.EXECUTIONS "执行次数",
        s.PARSING_USER_ID "用户ID",
        rank() over(order by EXECUTIONS desc) EXEC_RANK
     from v$sql s
     left join all_users u
      on u.USER_ID = s.PARSING_USER_ID) t
 where exec_rank <= 100;
```

## 3、读硬盘多或占用内存可能多的SQL

```shell
SELECT
	*
FROM ( SELECT
		sql_text,
		disk_reads,
		buffer_gets,
		parsing_schema_name,
		executions
	FROM
		v$sqlarea
	ORDER BY
		disk_reads DESC
)
WHERE
	rownum <= 50;
```

### 4、查看排序次数最多的SQL

```shell
SELECT
	*
FROM
	(
	SELECT
		sql_text,
		sorts,
		parsing_schema_name
	FROM
		v$sqlarea
	ORDER BY
		sorts DESC
)
WHERE
	rownum <= 50;
```