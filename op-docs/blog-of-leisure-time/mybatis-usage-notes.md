---
title: Mybatis 使用笔记
date: 2022-05-21
remark: 从印象笔记重新整理同步
---

# Mybatis使用笔记

## Mybatis中$与#的区别

### 一、理解mybatis中 $与#

在mybatis中的$与#都是在sql中动态的传入参数。

### 二、使用$与#

#{}: 解析为一个 JDBC 预编译语句（prepared statement）的参数标记符，一个 #{} 被解析为一个参数占位符 。

${}: 仅仅为一个纯碎的 string 替换，在动态 SQL 解析阶段将会进行变量替换。

### 三、总结

1. #是将传入的值当做字符串的形式，eg:select * from user where id =#{id},当前端把id值1，传入到后台的时候，就相当于 select *
   from user where id ='1'.
2. $是将传入的数据直接显示生成sql语句，eg:select * from user where id =${id},当前端把id值1，传入到后台的时候，就相当于
   select * from user where id = 1.
3. 使用#可以很大程度上防止sql注入。(语句的拼接)
4. 但是如果使用在order by 中就需要使用 $.
5. 在大多数情况下还是经常使用#，但在不同情况下必须使用$.
6. #与$的区别最大在于：#{} 传入值时，sql解析时，参数是带引号的，而${}传入值，sql解析时，参数是不带引号的。
