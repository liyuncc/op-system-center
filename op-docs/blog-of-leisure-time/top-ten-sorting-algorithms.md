---
title: 十大排序算法
date: 2020-08-03
remark: 从印象笔记重新整理同步
---

# 十大排序算法

说一下常见的排序算法和分别的复杂度:

排序算法经过长时间演变，大体可以分为两类：内排序和外排序。在排序过程中，全部记录存放在内存，则成为内排序；如果排序过程中需要使用外存，则称为外排序。

内排序可以分为以下几类。

1. 插入排序：直接插入排序、二分排序、希尔排序
2. 选择排序：直接选择排序、堆排序
3. 交换排序：冒泡排序、快速排序
4. 归并排序：
5. 基数排序：
6. 计数排序：
7. 桶排序：

## 直接插入排序

基本思想：

> 每一步将一个待排序的记录按其顺序码大小插入到前面已经排序好的子序列的合适位置（从后往前找合适位置），直到全部插入排序完为止。

Java代码实现：

```java
public class StraightInsertionSort {

    public int[] sort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            // 代排元素小于有序序列的最后一个元素时，向前插入
            if (array[i - 1] > array[i]) {
                // 记录要插入的元素
                int temp = array[i];
                // 循环将当前位置和要插入位置之间的元素，将其往后挪一位
                for (int j = i; j >= 0; j--) {
                    if (j > 0 && array[j - 1] > temp) {
                        array[j] = array[j - 1];
                    } else {
                        // 在插入完之后，结束当前循环
                        array[j] = temp;
                        break;
                    }
                }
            }
        }
        return array;
    }
}
```

复杂度分析：

- 插入排序的最好情况是数组已经有序，此时只需要进行n-1次比较，时间复杂度为O(n)
- 最坏情况是数组逆序排序，此时需要进行n(n-1)/2次比较以及n-1次赋值操作（插入）
- 平均来说插入排序算法的复杂度为O(n²)
- 空间复杂度上，直接插入法是就地排序，空间复杂度为(O(1))

## 二分插入排序

基本思想：

> 二分插入排序的思想和直接插入排序一样，但有所不同的是，查找插入位置的方式不一样，这里是按照二分查找找到适合位置的，这样可以减少比较的次数。

Java代码实现：

```java
public class BinaryInsertSort {
    public int[] sort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            //还是一如既往的判断当前元素是否小于之前有序序列的最后一个元素
            if (array[i] < array[i - 1]) {
                int start = 0;
                int end = i;
                int middle = 0;
                int temp = array[i];
                //使用二分查找找到插入元素需要插入的位置，此为他小于数组中与他相减绝对值最小的元素的位置
                //只能是start
                while (start <= end) {
                    middle = (start + end) / 2;
                    if (temp > array[middle]) {
                        //注意，元素相同时，也加在start的后面
                        start = middle + 1;
                    } else {
                        end = middle - 1;
                    }
                }
                //循环遍历，将插入位置和原位置之间的元素下标加一
                for (int j = i; j > start; j--) {
                    array[j] = array[j - 1];
                }
                array[start] = temp;
            }
        }
        return array;
    }
}
```

复杂度分析：

- 最坏情况：每次都在有序序列的起始位置插入，则整个有序序列的元素需要后移，时间复杂度为O(n²)
- 最好情况：待排序数组本身就是正序的，每个元素所在位置即为它的插入位置，此时时间复杂度仅为比较时的时间复杂度，为O(log2n)
- 平均情况：O(n²)
- 空间复杂度上，二分插入也是就地排序，空间复杂度为(O(1)).

## 希尔排序

基本思想：

> 希尔排序又叫做“缩小增量排序”，先取一个小于n的整数d1作为第一个增量，把文件的全部记录分组。所有距离为d1的倍数的记录放在同一个组中。
> 先在各组内进行直接插入排序；然后，取第二个增量d2 ，d3...

Java代码实现：

```java
public class ShellsSort {
    public int[] sort(int[] array) {
        //第一个增量最大，为数组的一半长度
        int gap = array.length / 2;
        //当增量大于0是，一直在比较
        while (gap > 0) {
            for (int j = gap; j < array.length; j++) {
                //定义i，让其只有在i为gap的倍数是才进入while循环
                int i = j;
                //进入循环条件为：
                //1.当前下表为子序列下表顺序，
                //2.待插入元素小于其所属子序列的前一个元素，将其两两交换位置，让大元素尽可能往后移，小元素尽可能往前移动
                while (i >= gap && array[i - gap] > array[i]) {
                    int temp = array[i];
                    array[i] = array[i - gap];
                    array[i - gap] = temp;
                    //每循环一次对i进行减gap操作，保证每次进入循环的都为对应子序列的下一个值
                    i -= gap;
                }
            }
            gap = gap / 2;
        }
        return array;
    }
}
```

复杂度分析：

- 增量排序的时间复杂度依赖于所取增量序列的函数，但是到目前为止还没有一个最好的增量序列.有人在大量的实验后得出结论;当n在某个特定的范围后希尔排序的比较和移动次数减少至n^1.3
  不管增量序列如何取值，都应该满足最后一个增量值为1。
- 有文献指出，当增量序列为d[k]=2^(t-k+1)时，希尔排序的时间复杂度为O(n^1.5), 其中t为排序趟数。
- 空间复杂度上，二分插入也是就地排序，空间复杂度为(O(1))。

## 直接选择排序

基本思想：

> 在要排序的一组数中，选出最小的一个数与第一个位置的数交换；然后在剩下的数当中再找最小的与第二个位置的数交换，如此循环到倒数第二个数和最后一个数比较为止。

Java代码实现：

```java
public class StraightSelectSort {
    public int[] sort(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            //假设当前元素为最小值，记录下标，
            int minIndex = i;
            //通过循环来判断假设的元素是否确实为最小，是的话，不处理，不是的话找出最下元素的下标
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[minIndex]) {
                    minIndex = j;
                }
            }
            //将最下元素与当前元素交换位置
            int temp = array[i];
            array[i] = array[minIndex];
            array[minIndex] = temp;
        }
        return array;
    }
}
```

复杂度分析：

- 选择排序第一轮内循环比较n-1次，然后是n-2次、n-3次........最后一轮内循环比较1次，共(n-1)+(n-2)+....+3+2+1=(n-1+1)
  n/2=n^2/2，
  其时间复杂度为O(n²)
- 空间复杂度就是在交换元素时那个临时变量所占的内存空间，空间复杂度为O(1)。

## 堆排序

堆排序是一种树形选择排序，是对直接选择排序的有效改进。

堆的定义下：

> 具有n个元素的序列 （h1,h2,…,hn),当且仅当满足（hi>=h2i,hi>=2i+1）或（hi<=h2i,hi<=2i+1） (i=1,2,…,n/2)时称之为堆。
> 在这里只讨论满足前者条件的堆。由堆的定义可以看出，堆顶元素（即第一个元素）必为最大项（大顶堆）。
> 完全二叉树可以很直观地表示堆的结构。堆顶为根，其它为左子树、右子树。（可以延伸到前序遍历、中序遍历、后序遍历）

基本思想：

> 初始时把要排序的数的序列看作是一棵顺序存储的二叉树，调整它们的存储序，使之成为一个堆，这时堆的根节点的数最大。然后将根节点与堆的最后一个节点交换。
> 然后对前面(n-1)个数重新调整使之成为堆。依此类推，直到只有两个节点的堆，并对它们作交换，最后得到有n个节点的有序序列。
> 从算法描述来看，堆排序需要两个过程，一是建立堆，二是堆顶与堆的最后一个元素交换位置。所以堆排序有两个函数组成。一是建堆的渗透函数，二是反复调用渗透函数实现排序的函数。

Java代码实现：

```java
public class HeapSort {
    public int[] sort(int[] array) {
        // 1.构建大顶堆
        for (int i = array.length / 2 - 1; i >= 0; i--) {
            // 从第一个非叶子结点从下至上，从右至左调整结构
            adjustHeap(array, i, array.length);
        }
        // 2.调整堆结构+交换堆顶元素与末尾元素
        for (int j = array.length - 1; j > 0; j--) {
            swap(array, 0, j);// 将堆顶元素与末尾元素进行交换
            adjustHeap(array, 0, j);// 重新对堆进行调整
        }
        return array;
    }

    /**
     * 调整大顶点堆
     *
     * @param array
     * @param i
     * @param length
     */
    public void adjustHeap(int[] array, int i, int length) {
        // 先取出当前元素
        int temp = array[i];
        // 从i节点的左子结点开始
        for (int j = 2 * i + 1; j < length; j = j * 2 + 1) {
            // 如果左子结点小于右子结点，k指向右子结点
            if (j + 1 < length && array[j] < array[j + 1]) {
                j++;
            }
            if (array[j] > temp) {// 如果子节点大于父节点，将子节点值赋给父节点（不用进行交换）
                array[i] = array[j];
                i = j;
            } else {
                break;
            }
        }
        array[i] = temp;// 将temp值放到最终的位置
    }

    /**
     * 交换元素
     *
     * @param arr
     * @param a
     * @param b
     */
    public static void swap(int[] arr, int a, int b) {
        int temp = arr[a];
        arr[a] = arr[b];
        arr[b] = temp;
    }
}
```

复杂度分析：

- 堆排序的时间复杂度主要由两部分组成：初始化建堆和每次弹出堆顶元素后重新建堆的过程
- 初始化建堆过程的时间复杂度O(n)：假设堆的高度为k，则从倒数第二层右边的节点开始，这一层的节点都要进行子节点比较然后选择是否交换，倒数第三层类似，
  一直到第一层(即层数从k-1到1)；那么总的时间为(2^(i-1))*(k-i)，其中i表示第i层(范围是k-1到1)，2^(i-1)表示该层上有多少元素，
  (k-i)表示子树上要比较的次数，即S = 2^(k-2)*1 + 2^(k-3)*2 + 2^(k-4)*3 + ... + 2^1*(k-2) + 2^0*(k-1)，使用错位相减法(
  用常数2来辅助转换，两边都乘以2再减去原等式)得到S = 2^(K-1) + 2^(K-2) + 2^(K-3) + ... + 2 - (K-1)
  ，忽略最后一项常数项就是等比数列，即S=2^k-2-(k-1)=2^k-k-1，
  又因为k为完全二叉树的深度，所以有 2^k <= n < 2^k-1，可以认为k = logn，综上所述S = n - logn -1，所以时间复杂度为O(n)
- 弹出堆顶元素后重建堆过程的时间复杂度O(nlogn)：循环n-1次，每次都从跟节点往下循环查找所以每一次时间都是logn，总时间为(n-1)*
  logn = nlogn - logn
- 故堆排序的时间复杂度为O(n) + O(nlogn) = O(nlogn)
- 堆排序是接地排序，所以空间复杂度为常数O(1)

## 冒泡排序

基本思想：

> 在要排序的一组数中，对当前还未排好序的范围内的全部数，自左向右对相邻的两个数依次进行比较和调整，让较大的数往下沉，较小的往上冒。
> 即：每当两相邻的数比较后发现它们的排序与排序要求相反时，就将它们互换。依次比较相邻的两个数，将小数放在前面，大数放在后面。
> 即在第一轮比较中：首先比较第1个和第2个数，将小数放前，大数放后；然后比较第2个数和第3个数，将小数放前，大数放后，如此继续，直至比较最后两个数，
> 将小数放前，大数放后。重复第一轮的步骤，直至全部排序完成。

Java代码实现：

```java
public class BubbleSort {
    public int[] bubbleSort(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            //这里减去i是为了不用再去考虑已经排好的元素，再减去1是为了防止下标越界
            for (int j = 0; j < array.length - i - 1; j++) {
                //经过两两比较，将最大的元素往后移
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        return array;
    }
}
```

复杂度分析：

- 最坏情况：冒泡排序要进行n-1轮排序循环，每轮排序循环中序列都是非正序的，则每轮排序循环中要进行n-i次比较(1<=i<=n-1)
  ，即其外循环执行n-1次，
  内循环最多执行n次，最少执行1次，由于内循环执行次数是线性的，故内循环平均执行(n+1)/2次，时间复杂度计算为((n-1)(n+1))/2=(-1)
  /2 ，时间复杂度为O(n²)
- 最好情况：待排序数组本身就是正序的，一轮扫描即可完成排序，此时时间复杂度仅为比较时的时间复杂度，为O(n)
- 平均情况：O(n²)
- 空间复杂度就是在交换元素时那个临时变量所占的内存空间，最优的空间复杂度就是开始元素顺序已经排好了，则空间复杂度为0，最差的空间复杂度就是开始元素逆序排序了，
  则空间复杂度为O(n)，平均的空间复杂度为O(1)

## 快速排序

基本思想：

> 选择一个基准元素,通常选择第一个元素或者最后一个元素,通过一轮扫描，将待排序列分成两部分,一部分比基准元素小,一部分大于等于基准元素,
> 此时基准元素在其排好序后的正确位置,然后再用同样的方法递归地排序划分的两部分，直到各区间只有一个数。

Java代码实现：

```java
public class QuickSort {
    public int[] sort(int[] array, int start, int end) {
        int pivot = array[start];
        int i = start;
        int j = end;
        while (i < j) {
            while ((i < j) && (array[j] > pivot)) {
                j--;
            }
            while ((i < j) && (array[i] < pivot)) {
                i++;
            }
            if ((array[i] == array[j]) && (i < j)) {
                i++;
            } else {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        }
        if (i - 1 > start)
            array = quickSort(array, start, i - 1);
        if (j + 1 < end)
            array = quickSort(array, j + 1, end);
        return (array);
    }
}
```

复杂度分析：

- 最好情况：是每轮划分都将待排序列正好分为两部分，那么每部分需要的时间为上一轮的1/2。如果排序n个元素的序列，其递归树深度为[logn]+1
  即仅需递归logn次，需要总时间为T(n)的话，第一次需要扫描整个序列，做n次比较，然后将序列一分为二，这两部分各自还需要T(n/2)
  的时间，依次划分下去：T(n) = 2*T(n/2)+n T(n) = 2*(2*(T(n/4)+n/2)+n = 4*T(n/4)+2n 等等，且T(1) = 0，所以T(n) = n*T(1) + n*
  logn = O(nlogn)
- 最坏情况：快速排序的平均时间复杂度也是O(nlog2^n)。因此，该排序方法被认为是目前最好的一种内部排序方法。
- 最坏情况：当待排序列为有序序列(正序或倒序)
  ，每次划分后得到的情况是一侧有1个元素，另一侧是其余元素，则最终要进行n-1轮循环，且第i次循环要进行n-i次比较，总比较次数为n-1 +
  n-2 + ... + 1 = n(n-1)/2，即时间复杂度为O(n²)
- 从空间性能上看，尽管快速排序只需要一个元素的辅助空间，但快速排序需要一个栈空间来实现递归。最好的情况下，
  即快速排序的每一趟排序都将元素序列均匀地分割成长度相近的两个子表，所需栈的最大深度为log2(n+1)；但最坏的情况下，栈的最大深度为n。
  这样，快速排序的空间复杂度为O(log2^n))。

## 归并排序

基本思想：

> 归并（Merge）排序法是将两个（或两个以上）有序表合并成一个新的有序表，即把待排序序列分为若干个子序列，每个子序列是有序的。然后再把有序子序列合并为整体有序序列。
> 归并排序中第二步，对两个有序数组排序法则非常简单，同时对两个数组的第一个位置比较大小，将小的放入一个空数组，然后被放入空数组的那个位置的指针往后移一个，
> 然后继续和另一个数组的上一个位置进行比较，以此类推。直到最后任何一个数组先出栈完，就将另外一个数组里的所有元素追加到新数组后面。

Java代码实现：
```java
public class MergeSort {
    public int[] sort(int[] array, int start, int end) {
        // 将原数组原封不动的复制一遍
         int[] arr = Arrays.copyOf(array,  array.length);
         // 判断，若数组长度小于2，返回数组
         if (arr.length < 2) {
              return arr;
         }
         // 将数组对半分，分别保存在两个数组中
         // int middle = arr.length/2;
         int middle = (int) Math.floor(arr.length /  2);// floor,向下取整
         
         int[] left = Arrays.copyOfRange(arr, 0,  middle);
         int[] right = Arrays.copyOfRange(arr, middle,  arr.length);
         
         return merge(mergeSort(left),  mergeSort(right));
    }
     
    public int[] merge(int[] left, int[] right) {
        int[] result = new int[left.length +  right.length];
        int i = 0;
        while (left.length > 0 && right.length > 0) {
             if (left[0] <= right[0]) {
                 result[i] = left[0];
                 left = Arrays.copyOfRange(left, 1,  left.length);
             } else {
                 result[i] = right[0];
                 right = Arrays.copyOfRange(right, 1,  right.length);
             }
             i++;
        }
        while (left.length > 0) {
             result[i] = left[0];
             left = Arrays.copyOfRange(left, 1,  left.length);
             i++;
        }
        while (right.length > 0) {
             result[i] = right[0];
             right = Arrays.copyOfRange(right, 1,  right.length);
             i++;
        }
        return result;
    }
}
```

复杂度分析

- 时间复杂度：归并排序主要分为拆分和对有序数组进行排序，拆分操作的时间复杂度为logn，排序的复杂度为n，所以归并排序的时间复杂度为O(nlogn)
- 归并排序的空间复杂度就是那个临时数组和递归时压如栈的数据占用的空间：n + logn，所以空间复杂度为O(n)

## 基数排序

基本思想：

> 将所有待比较数值（正整数）统一为同样的数位长度，数位较短的数前面补零。然后，从最低位开始，依次进行一次排序。
> 这样从最低位排序一直到最高位排序完成以后,数列就变成一个有序序列。

Java代码实现：

```java
public class RadixSort {
    // d表示最大的数有多少位
    public int[] sort(int[] array, int d) {
        int k = 0;
        int n = 1;
        int m = 1; // 控制键值排序依据在哪一位
        int[][] temp = new int[10][array.length]; //  数组的第一维表示可能的余数0-9
        int[] order = new int[10]; // 数组orderp[i]用来表示该位是i的数的个数
        while (m <= d) {
             for (int i = 0; i < array.length; i++) {
                 int lsd = ((array[i] / n) % 10);
                 temp[lsd][order[lsd]] = array[i];
                 order[lsd]++;
             }
             for (int i = 0; i < 10; i++) {
                 if (order[i] != 0)
                      for (int j = 0; j < order[i];  j++) {
                           array[k] = temp[i][j];
                           k++;
                      }
                 order[i] = 0;
             }
             n *= 10;
             k = 0;
             m++;
        }
        return array;
    }
}
```

复杂度分析：

- 时间复杂度：给定n个d位数(即d个关键码，关键码的取值范围为r)，基数排序需要比较元素的每一位，则复杂度为O(d(n+r))，其中一轮循环分配时间复杂度为O(n)， 
  一轮循环收集时间复杂度为O(r)，共需要d次循环来进行分配收集，即时间复杂度为O(d(n+r))

## 桶排序

基本思想：

> 

Java代码实现：
```java
public class BucketSort {
    public int[] sort(int[] array) {
        // 计算最大值与最小值
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < array.length; i++) {
             max = Math.max(max, array[i]);
             min = Math.min(min, array[i]);
        }
        // 计算桶的数量
        int bucketNum = (max - min) / array.length +  1;
        ArrayList<ArrayList<Integer>> bucketArr = new  ArrayList<>(bucketNum);
        for (int i = 0; i < bucketNum; i++) {
             bucketArr.add(new ArrayList<Integer>());
        }
        // 将每个元素放入桶
        for (int i = 0; i < array.length; i++) {
             int num = (array[i] - min) /  (array.length);
             bucketArr.get(num).add(array[i]);
        }
        // 对每个桶进行排序
        for (int i = 0; i < bucketArr.size(); i++) {
             //对每个桶进行排序时可以用其他排序算法，建议快排
             //这里我们使用的时Collections下的sort函数
             Collections.sort(bucketArr.get(i));
        }
        // 将桶中的元素赋值到原序列
        int index = 0;
        for (int i = 0; i < bucketArr.size(); i++) {
             for (int j = 0; j <  bucketArr.get(i).size(); j++) {
                 array[index++] =  bucketArr.get(i).get(j);
             }
        }
        return array;
    }
}
```

复杂度分析：

- 时间复杂度：O(n+k)，k为 n*logn-n*logm，m是桶的数量。
- 空间复杂度：由算法过程可知，桶排序的时间复杂度为 ，其中  表示桶的个数。由于需要申请额外的空间来保存元素，并申请额外的数组来存储每个桶，所以空间复杂度为 O(n+m), m是桶的数量。

## 计数排序

基本思想：

> 

Java代码实现：

```java
public class CountingSort {
    public int[] sort(int[] array) {
        int array1[] = new int[array.length];
        int max = array[0], min = array[0];
        for (int i : array) {
             if (i > max) {
                 max = i;
             }
             if (i < min) {
                 min = i;
             }
        }
        // 这里k的大小是要排序的数组中，元素大小的极值差+1
        int k = max - min + 1;
        int array2[] = new int[k];
        for (int i = 0; i < array.length; ++i) {
             // 优化过的地方，减小了数组array2的大小
             array2[array[i] - min] += 1;
        }
        for (int i = 1; i < array2.length; ++i) {
             array2[i] = array2[i] + array2[i - 1];
        }
        for (int i = array.length - 1; i >= 0; --i) {
             // 按存取的方式取出array2的元素
             array1[--array2[array[i] - min]] =  array[i];
        }
        return array1;
    }
}
```

复杂度分析：

- 时间复杂度：计数排序的时间复杂度为 O(n+k)，k为申请的额外空间大小。
- 空间复杂度：因为算法过程中需要申请一个额外空间和一个与待排序集合大小相同的已排序空间，所以空间复杂度为O(n+k)。
