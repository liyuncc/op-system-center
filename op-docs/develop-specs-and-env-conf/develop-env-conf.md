# 开发环境配置

## 一、Java开发环境

### 1、JDK

*注意: oracle open-jdk download，建议下载zip/tar.gz包，然后解压后使用*

[open-jdk-17](https://jdk.java.net/java-se-ri/17)

[open-jdk-8](https://jdk.java.net/java-se-ri/8-MR5)

### 2、IntelliJ IDEA

#### 2.0、IDEA download

*注意: 考虑到版权问题，推荐使用社区版*

[IntelliJ IDEA Community Edition](https://www.jetbrains.com/zh-cn/idea/download/other.html)

#### 2.1、编码设置

需要设置编码为utf8:
![img.png](images/idea-setting-encoding.png)

#### 2.2、代码格式化设置

##### 2.2.1、自动导包去除星号

打开设置 -> Editor -> Code Style -> Java -> Scheme 下拉框内选择Default -> Tab栏目中选择 Imports

1. 将```Class count to use import with ""```改为99（导入同一个包的类超过这个数值自动变为 * ）
2. 将```Names count to use static import with ""```改为99（同上）
3. 将```Package to Use import with "*"```删掉默认的这两个包（不管使用多少个类，只要在这个列表里都会变为 * ）

![img.png](images/idea-setting-import-packages-count.png)

![img_1.png](images/idea-setting-import-packages-static.png)

#### 2.3、代码格式CheckStyle检查设置

##### 2.3.1、配置CheckStyle

先在"设置 -> Plugins"中搜索"CheckStyle-IDEA"进行安装，重启IDEA
![img.png](images/idea-setting-check-style-plugin.png)
"设置 -> Editor -> Code Style -> Java"；然后点击齿轮， 选择"
Import Scheme -> CheckStyle Configuration"，
选择导入"check-code-style.xml"
![img.png](images/idea-setting-check-style-import.png)
成功导入CheckStyle配置以后，切换到JavaDoc Tab页， 取消勾选```Generate "<p>" on empty lines```，
![img.png](images/idea-setting-empty-lines.png)
经过以上配置以后，确保每次提交代码以前都执行过代码格式化，需要在"Actions on Save"当中做如下设置：
![img.png](images/idea-setting-reformat-code.png)

##### 2.3.2、CheckStyle使用

![img.png](images/idea-setting-check-style-run.png)

#### 2.4、SpotBugs设置

在```设置->插件```当中搜索与安装SpotBugs插件
![img.png](images/idea-setting-spot-bugs-plugin.png)
然后就可以对代码进行检查：
![img.png](images/idea-setting-spot-bugs-run-all.png)
![img.png](images/idea-setting-spot-bugs-run-moule.png)

#### 2.5、文件模板及方法注释设置

##### 2.5.1、文件模板

复制以下内容，然后粘贴到```设置->Editor -> File and Code Tempaltes -> Includes```当中的“File Header”。

```shell
/**
 * @file ${filename}
 * @brief ${briefDescription}
 * @details ${detailDescription}
 * @version 1.0
 * @author ${USER}
 * @date ${YEAR}-${MONTH}-${DAY}
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * ${YEAR}-${MONTH}-${DAY}               ${USER}          Created
 *
 */
```

![img.png](images/idea-setting-file-header.png)

### 3、数据库及管理工具

#### 3.0、Oracle Database download

*注意: 推荐使用 11g Release 2*

[Oracle Database 11g Release 2 (11.2.0.1.0)](https://www.oracle.com/partners/campaign/112010-win64soft-094461.html)

#### 3.1、安装及配置

[Oracle 11g安装教程](https://www.w3cschool.cn/oraclejc/oraclejc-41xa2qqv.html)

#### 3.2、数据库管理工具

*注意：考虑到版权问题，推荐使用开源工具*

图形化建议使用[dbeaver](https://dbeaver.io)，可以支持多种数据库通信协议。

### 4、版本管理

#### 4.0、GIT

*注意：推荐使用GIT进行版本管理。*

[GIT](https://git-scm.com/download)

#### 4.1、全局配置

```shell
git config --global user.name "your-username"
git config --global user.email "your-email-address"
```

代码合并策略，推荐使用rebase：

```shell
git config --global pull.rebase true
```

#### 4.2 跨操作系统

参考资料：

https://kamaslau.wordpress.com/2023/03/30/handle-git-console-hint-crlf-will-be-replaced-by-lf-the-next-time-git-touches-it/
https://stackoverflow.com/a/1552782/6087010

在Windows下使用Git进行修改提交时，控制台显示了如下输出内容：
`warning: in the working copy of 'xxx.file', LF will be replaced by CRLF the next time Git touches it`

这是因为目前的Git仓库由于跨操作系统操作而引发了部分文件的换行符转换问题。

具体来说，Linux、macOS、Windows操作系统对于文本文件的换行符有不同的标准，因此一个文件如果与上次操作的系统环境不同，Git自然会在文件对比时识别到标识符被修改，从而引发提示。

LF和CR字符在不同的操作系统中被用作操作符，其中LF（0x0A, \n）的初始定义是将光标从当前位置下移一行，不涉及移动到该行行首位置的动作，而CR（0x0D,
\r）的原始含义则是将光标前移到当前行的行首，不涉及下移的动作。Linux系操作系统（含macOS，虽然它在OSX时期曾经使用过CR）使用LF直接表示光标换行+移到行首；Windows组合使用了CRLF（0x0D
0x0A, \r\n），无疑是符合标准语义的做法。尽管这不是一个Bug或错误，但还是可以通过如下方式对Git进行配置，以避免在每次提交代码时显示：

``` shell
# Linux/macOS系统下在提交代码时自动将CRLF转换为LF
git config --global core.autocrlf input

# Windows系统下在提交代码时自动将LF转换为CRLF
git config --global core.autocrlf true
```

#### 4.3 提交时错误提交大文件（提交限制100MB）

**错误提示：**
> remote: error: File: 90b39f4470e405ed852e517a73473b527ac60eaa 362.16 MB, exceeds 100.00 MB.
> remote: Use command below to see the filename:
> remote: git rev-list --objects --all | grep 90b39f4470e405ed852e517a73473b527ac60eaa

**解决步骤:**

1. 根据提示查看超大文件
   ```shell
   git rev-list --objects --all | grep 90b39f4470e405ed852e517a73473b527ac60eaa
   ```
2. 执行命令忽略掉超大的文件
   ```shell
   git rm --cached file_name
   #如果是文件夹
   git rm -r --cached directory_name
   ```
3. 在commit的提交历史里面去除这个超大文件
   ```shell
   git filter-branch --tree-filter 'rm -rf 文件名' HEAD
   ```

#### 4.4 .gitignore忽略文件不工作

1. 清除本地当前的Git缓存
   ```shell
   git rm -r --cached .
   ```
2. 应用.gitignore等本地配置文件重新建立Git索引
   ```shell
   git add .
   ```
