# 开发规范（Java版本）

## 一、源代码

### 1、命名规范

#### 1.1、编码设计

1. 禁止使用拼音（如特殊要求、复杂对象可使用拼音首字母命名的，必须标明注释）；
2. 实体建议以Entity结尾，如UserEntity；
3. 枚举建议以Enum结尾，如ValidatedStatusEnum；
4. 变量命名尽量清晰规范，尽量准确表达含义
5. 禁止使用下划线，使用驼峰命名规范
6. service接口建议：I{对象}Service；serviceImpl类建议：{对象}ServiceImpl
7. 通用接口、工具类，强制编写单元测试
8. 禁止写死常量（不论是sql还是业务逻辑）
9. 数据查询，建议不要使用```*```代替常量，对于一些不需要查询的数据禁止使用```*```连同查询出来
10. 禁止大量代码重复

#### 1.2、接口设计

1. 理论上推荐使用restfull进行规范设计，但是考虑到业务的复杂性及接口的命名难度，建议在restful上进行扩展修改，统一风格，建议{动作}{副词}{名词}方式
   进行接口动作设计，例如根据用户名查询用户数据：queryByUsername?username=xxx
    - 推荐使用动作、query、create、remove、update
    - 推荐GET请求使用动作查询后在url拼接参数
2. 一般来说，数据库中的表都是同种记录的集合（collection），所以api资源也应该使用复数，例如产品接口：/api/v1/products
3. url命名建议统一使用小写，不用大写
4. 用中杠“-”，不用下划线“_”，建议使用" - "而不是" _ "字符可能会在某些浏览器或屏幕中被部分遮挡或完全隐藏；
5. 易读，通过请求url大概能猜到是什么操作；
6. URL结尾不应该包含斜杠" / "，正斜杠（/）不会增加语义值，且可能导致混淆；
7. url层级从资源层开始，不能太深，，建议不超过三层。例如：资源层为/api/v1/products，查询分类信息：/api/v1/products/categories/class?productId=xxx

### 2、编码组织结构

#### 2.1、系统架构组织

##### 2.1.1、模块设计原则

*通用的、抽象的、高复用、模块化的代码抽出来，尽可能减少耦合、冗余（细节依赖抽象，依赖倒置原则）。例如一些模板方法、基础父类、工具类，这些代码做好适配、重载，提供给业务模块去使用，尽量不要在业务模块再去做重复的编码，每个业务module只做自己职责内的事情（单一原则）。*

1. 模块之间通信必须经过它们的接口。
2. 通过服务接口暴露数据和功能。
3. 不允许有其他形式的进程通信，包括：直接链接、直接读取数据存储、进行内存共享。没有任何后门，唯一允许的通信是通过网络上的服务接口调用。
4. 架构模块设计：尽量遵从单一职责原则，每个模块只做一件事，例如：op-bom模块，依赖管理、版本管理；op-common-core模块，通用模块，定义通用数据结构、
   通用工具、通用方法等；op-mybatis-spring-boot-starter模块，封装mybatis、mybatis-plus相关，基础父类、自动填充、拦截器、动态配置等；
5. 依赖管理，尽量减少重复导入，导入时应检查父类是否导入，子模块禁止出现版本数据，所有依赖及版本应统一由op-bom模块进行处理；

#### 2.2、业务模块：可参照该样例进行组织

```
package
    |____config
    |____constant
        |____enums
    |____conmmon
        |____annotation
        |____aop
        |____domain
    |____controller
    |____model
        |____entity
    |____dto
    |____vo
    |____service
        |____impl
    |____mapper/repository
    |____uitls
    |____converter
```

### 3、接口请求

#### 3.1、通用请求

1. 如无特殊限制，建议按照restfull规范进行设计
2. 如有限制，建议限制使用GET、POST，方法统一使用注解@PostMapping、@GetMapping，减少使用@RequestMapping；

#### 3.2、分页请求

1. 对于除分页参数外（pageIndex、pageSize）的其他帅选条件，参数个数小于等于三个建议使用get（时间条件除外）；筛选参数较多或参数值含有特殊字符串或格式的，推荐使用POST，条件参数使用requestBody封装；
2. 分页数据响应必须包含分页数据信息（包括当前页码、每页大小、总页数、总条数、排序规则等）；

### 4、传输对象封装

*推荐使用vo、dto、model，避免暴露不需要展示的数据，同时可对一些数据进行转换展示（枚举类型字段）*

```
请求对象封装：
    推荐格式：{对象}{操作}Dto
    例如：UserCreateDto
响应对象封装：
    推荐格式：{试图对象}Vo
    例如：UserVo
内部传输对象封装：
    推荐格式：{对象关系}Model
    例如：UserLoginModel
```

### 5、埋点日志规范

*建议格式为：{时间}|{来源}|{对象}|{操作类型}|{对象属性(以&分割)}*

```
例如：2016-07-27 23:37:23|file-center|master|file-upload|fileName=xxx&filePath=xxx
```

### 6、代码注释

#### 6.1、文件注释

#### 6.2、方法注释

#### 6.3、关键代码块注释

### 7、文档管理

建议手动编写设计文档及接口文档。设计文档应大致描述业务操作流程、涉及的接口、数据结构；接口文档应规范method、url、params、header、requestBody、responseBody、接口描述；

#### 7.1、设计文档

#### 7，2、接口文档

### 8、代码提交规范

1. 确保代码测试通过、编译正确、运行正确；
2. 代码是否格式化，是否符合代码规范，无用的包引入、变量是否清除等等。
3. 统一提交格式，提交时检查注释是否准确简洁的表达出了本次提交的内容。
4. 不要提交与项目无关的内容文件。

### 9、数据库脚本

建议保存各个版本的sql脚本；严格保存各个变动版本脚本；

## 二、质量要求

### 1、单元测试

为了保障产品质量，要求基础组件必须使用单元测试，保障测试覆盖范围>=70%，确保逻辑分支尽可能被覆盖到。其它复合场景且难以模拟的测试，要求把测试的方法以文
档+测试资源文件的形式放置到src/test目录下的integration目录。

### 2、代码样式与低级别BUG

项目已经集成有checkstyle与findbugs，原则上在PUSH代码以前，须执行checkstyle与findbugs的检查并修改ERROR级别的问题。若不能把握是否应该忽略，
请向高级开发/架构师咨询。

### 3、code review（建议推行，代码质量保证）

feature开发、bugfix修复、config增改，应该需要进行code review，避免代码低质量代码；

## 三、数据库

### 1、命名

表名、字段名采用lowercaseUnderscore的方式命名。即数据库名、表名、字段名全部使用小写，且使用下划线分隔单词。主键统一使用id命名，禁止使用数据库的
外键功能。在当前表需要存储其它表的id值时使用{表名}_id的方式对列进行命名，如person_id。

### 2、索引

推荐的命名方式为

```
{tablename}_{columnname(s)}_{suffix}
```

#### suffix类型

```pkey```: 主键

```key```: 唯一约束

```excl```: 排除约束

```idx```：一般索引

```fkey```：外键，一般不使用外键

```check```: 检查约束

```seq```： 序列

#### 实例：

```user_id_pkey```
```user_email_key```

对于经常在WHERE，以及JOIN时用到且区分率比较高的字段，尽可能加上索引。

### 3、强制相关要求

1. 潜在可能部署多实例且连接到一个数据库实例的后端程序，禁止直接在数据库变更脚本中 删除或者重命名字段。

2. 经由系统架构、业务程序访问的表，必须为表与字段添加注释。

3. 表的命名规则为 {前缀}_表用途，如user_person，表示用户模块的个人信息表

**重要：** 编写SQL时，SQL关键字大写。数据库字段命名避免与数据库关键字重复，如order by。

### 4、数据操作

除对象关系表，原则上不允许物理删除数据，对于无效的数据仅作逻辑删除。

### 5、数据库查询

1. 只SELECT需要的字段；
2. 尽可能少地JOIN表，原则上不允许JOIN超过3张表。

## 三、Git版本管理

### 1、建议推行git flow

![img.png](images/git-flow-summary.png)
[什么是 git-flow？](https://www.git-tower.com/learn/git/ebook/cn/command-line/advanced-topics/git-flow/)

#### 1.1、master

#### 1.2、release

#### 1.3、feature

#### 1.4、hotfix

### 2、建议使用git rebase

git rebase会将后修改的提交追加到需要合并的分支上，这个样可以清晰地展示出每一次提交的修改历史。

### 3、git常用命令推荐

1. 远程分支于本地分支映射关系：

   ```git branch -vv```

2. 合并其他分支代码到当前分支：

   ```git pull --rebase origin remote-branch-name```

3. 创建远程分支，并推送当前分支到新创建的远程分支：

   ```git push --set-upstream origin HEAD:new-remote-branch-name```

4. 修改当前分支于远程分支的映射：

   ```git branch --set-upstream-to origin/remote-branch-name```

5. 修改提交信息

   ```git commit --amend```
