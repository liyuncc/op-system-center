---
title: 个人开发环境、习惯配置
date: 2024-01-9 09:34:45
---

# 个人开发环境、习惯配置：

## 1、代码管理、多人协作：

1. [Git](https://git-scm.com/downloads)

2. [Apache Maven](https://maven.apache.org/)

## 2、命令行终端：

- [Windows Terminal](https://github.com/microsoft/terminal)
- [Git Bash](https://git-scm.com/)
- [Tabby Terminal](https://tabby.sh/)

## 3、开发集成工具

1. [IntelliJ IDEA Community Edition 社区版](https://www.jetbrains.com/zh-cn/idea/download/other.html)
2. [PyCharm Community Edition 社区版](https://www.jetbrains.com/zh-cn/pycharm/download/other.html)
3. idea 专业版、goland 专业版（破解 or 无限适用）
   - https://jetbra.in/5d84466e31722979266057664941a71893322460
   - https://zhuanlan.zhihu.com/p/350771702
4. [Visual Studio Code](https://code.visualstudio.com/)

## 4、工具

1. 文档编辑器
    - [MarkText](https://www.marktext.cc/)
    - [Sublime Text](https://www.sublimetext.com/)
    - WPS
    - Word、Excel、PowerPoint

2. 浏览器
    - Google Chrome
    - Microsoft Edge
    - Thorium

3. 科学上网
    - 内容可能含有违规信息

4. 解压缩工具
    - [7zip](https://www.7-zip.org/)

5. 网络工具
    - [Wireshark](https://www.wireshark.org/download.html)

6. 产品原型
    - [Axure RP](https://www.axure.com/)
    - [墨刀](https://modao.cc/)

7. 杀毒软件
    - 火绒安全软件

8. 缺陷分析工具
    - [jvisualvm](https://docs.oracle.com/javase/8/docs/technotes/tools/unix/jvisualvm.html)
    - [JProfiler](https://www.ej-technologies.com/jprofiler)

9. 集成工具
    - Jenkins
    - GitLab

10. 设计工具
    - [Visio](https://www.microsoft.com/en-us/microsoft-365/visio/flowchart-software)
    - [ProcessON](https://www.processon.com/)
    - [Power Designer](https://www.powerdesigner.biz/)

11. 集成测试化工具
    - [ApiPost](https://www.apipost.cn/)
    - Apache JMeter
    - 

12. 其他开发工具
    - [jd-gui-windows-1.6.6（jar包反编译源码查看工具）](https://github.com/java-decompiler/jd-gui/releases)
    - [jce_policy-8（jar包反编译源码查看工具）](https://www.oracle.com/java/technologies/javase-jce-all-downloads.html)

## 5、接口调试工具：

- [Insomnia](https://insomnia.rest/download)：需要选择合适版本，最新版本个人感觉不好用
- [Postman](https://www.postman.com/)
- [Apifox](https://apifox.com/)

## 6、数据库连接可视化工具

- [Another Redis Desktop Manager](https://goanother.com/)
- [MongoDBCompass](https://www.mongodb.com/products/tools/compass)
- [DBeaver](https://dbeaver.io/)

## 7、环境

1. Java
    - [OpenJDK 17](https://jdk.java.net/java-se-ri/17)
    - [OpenJDK 8](https://jdk.java.net/java-se-ri/8-MR6)
2. Python
3. Node
4. Golang
5. WSL
6. Vagrant
7. NodeJs