---
title: Commit message 和 Change log 编写指南
date: 2024-06-13 10:22
---

# Commit message 和 Change log 编写指南

> 参考链接：[Commit message 和 Change log 编写指南](https://www.ruanyifeng.com/blog/2016/01/commit_message_change_log.html)

一般来说，`commit message`应该清晰明了，说明本次提交的目的。

目前，社区有多种`Commit message`
的写法规范。本文推荐使用[Angular规范](https://docs.google.com/document/d/1QrDFcIiPjSLDn3EL15IJygNPiHORgU1_OOAqWjiDU5Y/edit#heading=h.uyo6cb12dt6w)，
这是目前使用最为广泛的写法，比较合理和系统化，并且有配套工具。

## 一、Commit message 的作用

格式化的`Commit message`，有几个好处。

### 1. 提供更多的历史信息，方便快速浏览。

**1.1 指定commitId，查看某次提交之前的变动，只看首行，就知道某次提交的`commit`的目的。**

```shell
git log <commit-id> --pretty=format:%s
```

**1.2 显示上次发布后的变动**

```shell
git log <last-tag> HEAD --pretty=format:%s
```

### 2. 可以过滤某些`commit`，便于快速查找信息

```shell
git log --grep=<grep-keyword> --pretty=format:%s
```

### 3. 可以直接从`commit`生成`Change log`

Change Log 是发布新版本时，用来说明与上一个版本差异的文档。

## 二、Commit message 的格式

每次提交，`Commit message`都包括三个部分：`Header`，`Body`，`Footer`。

```shell
<type>(<scope>): <subject>
// 空行
<body>
// 空行
<footer>
```

其中，`Header`是必须的，`Body`和`Footer`可以省略。

不管是哪一个部分，任何一行都不得超过72个字符（或100个字符）。这是为了避免自动换行影响美观。

### 1. Header

`Header`部分只有一行，包括三个字段：`type`（必需）、`scope`（可选）和`subject`（必需）。

#### 1.1 type

`type`用于说明`commit`的类别，只允许使用下面的7个标识。

- feat：新功能（feature）
- fix：修补bug
- docs：文档（documentation）
- style：格式（不影响代码运行的变动）
- refactor：重构
- test：增加测试
- chore：构建过程或辅助工具的变动

如果`type`为`feat`和`fix`，则该 `commit` 将肯定出现在 `Change log` 之中。其他情况`（docs、chore、style、refactor、test）`
由你决定，要不要放入 `Change log`，建议是不要。

#### 1.2 scope

`scope`用于说明 `commit` 影响的范围，比如数据层、控制层、视图层等等，视项目不同而不同。

#### 1.3 subject

`subject`是 `commit` 目的的简短描述，不超过50个字符。

### 2. Body

`Body` 部分是对本次 `commit` 的详细描述，可以分成多行。

**建议：**

1. 应该说明代码变动的动机，以及与以前行为的对比。

### 3. Footer

Footer 部分只用于两种情况。

#### 3.1 不兼容变动

如果当前代码与上一个版本不兼容，则 Footer 部分以`BREAKING CHANGE`开头，后面是对变动的描述、以及变动理由和迁移方法。

#### 3.2 关闭Issue

如果当前 commit 针对某个issue，那么可以在 Footer 部分关闭这个 issue 。

### 4. Revert

还有一种特殊情况，如果当前 `commit` 用于撤销以前的 `commit`，则必须以`revert:`开头，后面跟着被撤销 `Commit` 的 `Header`。

```shell
revert: feat(pencil): add 'graphiteWidth' option
This reverts commit 667ecc1654a317a13331b17617d973392f415f02.
```

`Body`部分的格式是固定的，必须写成`This reverts commit <hash>.`，其中的`hash`是被撤销 `commit` 的 `SHA` 标识符。

如果当前 `commit` 与被撤销的 `commit`，在同一个发布`（release）`里面，那么它们都不会出现在 `Change log` 里面。
如果两者在不同的发布，那么当前 `commit`，会出现在 `Change log` 的`Reverts`小标题下面。

## 三、生成 Change log

生成的文档包括以下三个部分。

- New features
- Bug fixes
- Breaking changes.

##  四、配套工具

Commitizen是一个撰写合格 Commit message 的工具。

validate-commit-msg 用于检查 Node 项目的 Commit message 是否符合格式。

