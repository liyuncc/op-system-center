---
title: 后端WEB接口设计建议
date: 2024-08-23 11:13
---

# 后端WEB接口设计建议

## 1.接口参数校验

接口的入参和返回值都需要进行校验处理。

- 入参校验：格式、类型、长度、范围等；
- 响应校验：枚举字段、空值字段等；

## 2.接口扩展性

接口扩展性思考，对于某些接口，是单独为某个特定业务场景服务，还是公共抽象，为相似业务场景服务。

## 3.接口幂等性

**`幂等`是指多次调用接口不会改变业务状态，可以保证重复调用的结果和单次调用的结果一致。**

考虑接口幂等性设计，可保证同一个请求，多次触发请求，最终处理结果与预期一致。

## 4.日志打印

关键业务日志检测，最常见就是日志打印，控制台输出。在入参、官家逻辑、异常处理、返回值等响应代码块位置进行日志打印。

- 方便排查和定位线上问题；
- 如接入日志监控系统，可根据打印日志进行系统关键业务异常监测；

## 5.核心接口进行线程池隔离

常见业务处理有同步处理和异步处理，这些接口都可能使用到线程池，如果不做线程池隔离，就会出现核心业务被阻塞、超时等异常，会导致主业务收到影响。

## 6.三方接口异常重试

在业务开发过程中难免会出现调用三方接口，或者是微服务接口调用，就会涉及到请求响应等相关问题：

- 异常处理：在调用三方接口时发生异常，如组装参数异常、三方接口参数校验异常；
- 请求超时：三方接口调用超时，是否继续等待或者返回空值或者抛出异常；
- 重试机制：三方接口返回失败或者限制调用或者超时，是否需要重试，如何设置重试策略；

## 7.异步处理

对于一些链路较长、较为耗时、且无需同步返回的接口，可采用异步处理。

## 8.接口处理优化，串行优化为并行

- 减少读取频率

    - 遍历循环读写改为批量读写，避免循环读、循环写；
    - 能一次用sql处理的数据，尽量一次数据库IO处理，避免多次IO查询数据；

- 接口并行调用

    - 对于需要读取多个数据，且相互无关的逻辑，采取并行调用的方式，同时查询，而不是阻塞；

## 9.高频接口注意限流

限制并发调用频率；包括单用户调用频率、单位时间内调用频率；

## 10.保障接口安全

配置黑白名单，用Bloom过滤器实现黑白名单的配置；布隆过滤器的具体使用；

## 11.接口控制锁粒度

在高并发场景下，为了保障数据一致性，们会对共享资源进行加锁的操作来保证线程安全的问题，但是如果加锁的粒度过大，是会影响到接口性能的。

无论是使用synchronized加锁、自定义Lock、还是redis分布式锁，只需要在共享临界资源加锁即可，不涉及共享资源的，就不必要加锁。

## 12.避免长事务问题

长事务期间可能伴随cpu、内存升高、严重时会导致服务端整体响应缓慢，导致在线应用无法使用；产生长事务的原因除了sql本身可能存在问题外，和应用层的事务控制逻辑也有很大的关系。

如何尽可能地避免长事务问题：

- RPC远程调用不要放到事务里面；
- 一些查询相关的操作如果可用，尽量放到事务外面；
- 并发场景下，尽量避免使用@Transactional注解来操作事务，使用TransactionTemplate的编排式事务来灵活控制事务的范围；

原先使用@Transactional来管理事务
```java
@Transactional
public void doSomethings(SomeParams params){
    // 该方法为数据库操作
    doCurd(params);

    // 该方法为远程RPC接口
    sendEmailRpc(params);
}
```
使用TransactionTemplate进行编排式事务
```java
@Autowired
private TransactionTemplate transactionTemplate;

public void doSomethings(SomeParams params){
    // 该方法为数据库操作
    transactionTemplate.execute(transactionStatus -> {
        try {
            // 该方法为数据库操作
            doCurd(params);
        } catch (Exception exception) {
            // 异常手动设置回滚
            transactionStatus.setRollbackOnly();
        }
        return true;
    });
    doCurd(params);

    // 该方法为远程RPC接口
    sendEmailRpc(params);
}
```
