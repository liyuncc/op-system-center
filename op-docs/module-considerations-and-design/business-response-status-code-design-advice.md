---
title: 业务响应状态码设计建议
date: 2024-08-28 11:14
---

# 业务响应状态码设计建议

目前较为通用的前后端数据交互，序列化格式一般使用`JSON体`，大致格式如下：

```json
{
  "code": 0,
  "message": "操作成功",
  "timestamp": "2024-05-21T11:42:26.4548099",
  "payload": {}
}
```

**关于状态码CODE**

通常情况下，服务端只枚举`200`、`500`，囊括成功响应和处理失败；然后使用不同的`message`响应不同的异常情况；这样虽然能够照常满足业务，但状态码太凌乱了。

我们应该可以参考HTTP请求返回的状态码（下面是常见的HTTP状态码）：
> 200 - 请求成功
>
> 301 - 资源（网页等）被永久转移到其它URL
>
> 404 - 请求的资源（网页等）不存在
>
> 500 - 内部服务器错误

| 分类  | 区间      | 分类描述                    |
|-----|---------|-------------------------|
| 1** | 100~199 | 信息，服务器收到请求，需要请求者继续执行操作  |
| 2** | 200~299 | 成功，请求被成功接收并处理           |
| 3** | 300~399 | 重定向，需要进一步的操作已完成请求       |
| 4** | 400~499 | 客户端错误，请求包含语法错误或无法完成请求   |
| 5** | 500~599 | 服务器错误，服务器在处理请求的过程中发生了错误 |

我们可以参考这样的设计，这样的好处就把错误类型归类到某个区间内，如果区间不够，可以设计成4（5、6、或更多）位数。

> 100*～199* 区间表示参数错误
>
> 300*～399* 区间表示用户错误
>
> 500*～599* 区间表示接口异常
>

`同时，我们还可以根据模块、微服务进行分类，对模块、微服务进行编号分组，将编号和错误码拼接，能够更具体、更直观展示错误信息。`

**关于错误信息MESSAGE**

这个字段相对理解比较简单，就是发生错误时，如何友好地进行提示。一般的设计是和code状态码一起设计。状态码和信息就会一一对应，比较好维护。

```java
public enum ErrorCodeEnum implements BaseEnum<ErrorCodeEnum> {

    SUCCESS(0, "操作成功"),
    FAILURE(1, "操作失败"),
    NOT_FOUND_ERROR(10001, "未查询到信息"),
    VALIDATE_ERROR(10004, "数据检验失败"),
    VALIDATE_CAPTCHA_ERROR(100041, "验证码错误"),
    VALIDATE_CAPTCHA_TIMEOUT(100042, "验证码过期"),
    INVALID_USERNAME_OR_PASSWORD(30001, "用户名或密码无效"),
    UNAUTHORIZED(300011, "未登录"),
    SERVER_ERROR(500, "服务内部报错");

    private final Integer code;
    private final String message;

    ErrorCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }


    @Override
    public Integer getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    public static Optional<ErrorCodeEnum> of(Integer code) {
        return Optional.ofNullable(BaseEnum.parseByCode(ErrorCodeEnum.class, code));
    }

}

```