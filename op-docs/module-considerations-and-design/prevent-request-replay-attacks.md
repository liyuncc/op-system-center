---
title: 防止请求重放攻击
date: 2024-1-16 09:56
---

# 使用时间戳和随机数：

**例如：**

http://api.3dept.com/?msg_signature=ASDFQWEXZCVAQFASDFASDFSS&timestamp=13500001234&nonce=123412323&echostr=ENCRYPT_STR

| 参数        | 类型      | 说明                             |
|-----------|---------|--------------------------------|
| timestamp | Integer | 时间戳。与nonce结合使用，用于防止请求重放攻击。     |
| nonce     | String  | 随机数。与timestamp结合使用，用于防止请求重放攻击。 |