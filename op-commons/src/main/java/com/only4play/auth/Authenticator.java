package com.only4play.auth;

/**
 * @author liyuncong
 * @version 1.0
 * @file Authenticator.java
 * @brief 验证者
 * @details 验证者，返回已经认证的用户
 * @date 2022-03-31
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                        NAME               DESCRIPTION
 * 2022-03-31                  liyuncong          Created
 */
public interface Authenticator<T> {
    AuthenticatedUser<T> authenticate(String username, String password);
}
