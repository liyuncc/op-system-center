package com.only4play.auth;

/**
 * @author liyuncong
 * @version 1.0
 * @file AuthenticationDataLoader.java
 * @brief 认证信息加载
 * @details 加载需要认证用户的权限信息
 * @date 2022-03-31
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                        NAME               DESCRIPTION
 * 2022-03-31                  liyuncong          Created
 */
public interface AuthenticationDataLoader {

    Iterable<String> getRoles(AuthenticatedUser<?> user);
}
