package com.only4play.auth;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * @author liyuncong
 * @version 1.0
 * @file AuthenticatedAccount.java
 * @brief 已经认证过的账号
 * @details 已经认证过的账号
 * @date 2022-03-15
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2022-03-15               liyuncong        Created
 */
@Getter
@Setter
public class AuthenticatedAccount<T> extends AbstractAuthenticationToken {
    private final AuthenticatedUser<T> user;

    public AuthenticatedAccount(
        final Collection<? extends GrantedAuthority> authorities,
        final AuthenticatedUser<T> realUser) {
        super(authorities);
        this.user = realUser;
    }

    @Override
    public Object getCredentials() {
        return "";
    }

    @Override
    public Object getPrincipal() {
        return this.user.getUserId();
    }
}
