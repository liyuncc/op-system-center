package com.only4play.common.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.only4play.common.constants.ErrorCodeEnum;
import com.only4play.common.exception.ApplicationException;
import com.only4play.common.exception.ValidationException;
import com.only4play.common.model.GeneralResponse;
import com.only4play.common.model.ValidateResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

/**
 * ApplicationExceptionHandler
 *
 * @author liyuncong
 * @date 2023/11/20 16:04
 **/
@Slf4j
@RestControllerAdvice
public class ApplicationExceptionHandler {

    @ExceptionHandler(value = {Exception.class, Throwable.class})
    public GeneralResponse<?> handlerException(Exception exception) {
        Integer code = ErrorCodeEnum.SERVER_ERROR.getCode();
        String message = ErrorCodeEnum.SERVER_ERROR.getMessage();
        Object payload = null;

        if (exception instanceof ApplicationException) {
            code = ((ApplicationException) exception).getErrorCodeEnum().getCode();
            message = exception.getMessage();
            log.error("[全局异常拦截] ApplicationException: errorCode=[{}], errorMessage=[{}]", code, message);
        } else if (exception instanceof ValidationException) {
            code = ErrorCodeEnum.PARAM_ILLEGAL.getCode();
            message = ErrorCodeEnum.PARAM_ILLEGAL.getMessage();
            List<ValidateResult> result = ((ValidationException) exception).getResult();

            String validateResultJson = "";
            if (!CollectionUtils.isEmpty(result)) {
                StringBuilder jsonBuild = new StringBuilder(result.size());
                for (ValidateResult validateResult : result) {
                    jsonBuild.append("{").append(validateResult.getName()).append(" : ").append(validateResult.getMessage()).append("} ");
                }
                validateResultJson = jsonBuild.toString();
            }

            log.error("[全局异常拦截] ValidationException: errorCode=[{}], errorMessage=[{}], validateResult=[{}]", code, message, validateResultJson);
            payload = result;
        } else {
            log.error("[全局异常拦截] Exception: errorMessage=[{}]", exception.getMessage());
        }

        // printStackTrace exception
        exception.printStackTrace();

        // return json message
        return GeneralResponse.of(code, message, payload);
    }
}
