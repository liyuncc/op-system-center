package com.only4play.common.transaction;

/**
 * 事务处理Hook
 *
 * @author liyuncong
 * @date 2023/11/06 14:45
 **/
public interface TransactionHook {

    void afterTransaction(Runnable action);
}
