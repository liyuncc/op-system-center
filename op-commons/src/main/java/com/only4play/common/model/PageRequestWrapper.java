package com.only4play.common.model;

import lombok.*;

import java.util.Map;

/**
 * PageRequestWrapper
 *
 * @author liyuncong
 * @date 2023/11/06 13:58
 **/
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class PageRequestWrapper<T> {
    private Integer pageIndex = 1;
    private Integer pageSize = 10;
    private T bean;
    private Map<String, String> sorts;
}
