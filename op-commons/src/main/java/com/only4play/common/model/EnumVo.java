package com.only4play.common.model;

import lombok.Data;

/**
 * 枚举展示Vo
 *
 * @author liyuncong
 * @date 2023/11/06 14:12
 **/
@Data
public class EnumVo {
    private Integer code;
    private String name;
    private String text;
}
