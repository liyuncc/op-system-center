package com.only4play.common.model;

import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.util.Collection;

/**
 * 分页结果
 *
 * @author liyuncong
 * @date 2023/11/06 14:02
 **/

@Data
public class PageableResponse<T> extends AbstractResponse {
    private Integer pageIndex;
    private Integer pageSize;
    private Integer totalPages;
    private Long total;
    private Iterable<T> payload;

    /**
     * 构造函数.
     */
    public PageableResponse() {
    }

    public PageableResponse(Iterable<T> payload, Long total, Integer pageSize, Integer pageIndex) {
        this.payload = payload;
        this.total = total;
        this.pageSize = pageSize;
        this.pageIndex = pageIndex;
    }

    public static <T> PageableResponse<T> of(Iterable<T> list, Long total, Integer pageSize, Integer pageNumber) {
        return new PageableResponse<T>(list, total, pageSize, pageNumber);
    }

    /**
     * 快速构造方法.
     *
     * @param payload      消息体
     * @param pageSize     每页大小
     * @param pageIndex    页码
     * @param totalRecords 当前页数量
     * @param totalPages   总记录数量
     * @param <T>          消息体类型
     * @return PageableResponse
     */
    public static <T> PageableResponse<T> of(
        Collection<T> payload,
        int pageSize, int pageIndex, Long totalRecords, int totalPages
    ) {
        if (CollectionUtils.isEmpty(payload)) {
            return PageableResponse.empty();
        }
        return new PageableResponse<>(
            payload,
            pageSize,
            pageIndex,
            payload.size(),
            totalRecords,
            totalPages
        );
    }

    /**
     * 构造函数.
     *
     * @param code          错误码
     * @param message       错误消息
     * @param payload       消息体
     * @param pageSize      每页大小
     * @param pageIndex     页码
     * @param currentNumber 当前页数量
     * @param totalRecords  总记录数量
     * @param totalPages    总页数
     */
    public PageableResponse(
        int code, String message, Iterable<T> payload,
        int pageSize, int pageIndex, int currentNumber, Long totalRecords, int totalPages
    ) {
        super(code, message);
        this.pageSize = pageSize;
        this.pageIndex = pageIndex;
        this.total = totalRecords;
        this.totalPages = totalPages;
        this.payload = payload;
    }

    /**
     * 构造函数.
     *
     * @param payload       消息体
     * @param pageSize      每页大小
     * @param pageIndex     页码
     * @param currentNumber 当前页数量
     * @param totalRecords  总记录数量
     * @param totalPages    总页数
     */
    public PageableResponse(
        Collection<T> payload,
        int pageSize, int pageIndex, int currentNumber, Long totalRecords, int totalPages
    ) {
        this(0, null, payload,
            pageSize, pageIndex, currentNumber, totalRecords, totalPages);
    }

    /**
     * 快速构造方法.
     *
     * @param <T> 消息体类型
     * @return PageableResponse
     */
    public static <T> PageableResponse<T> empty() {
        return new PageableResponse<>();
    }

}
