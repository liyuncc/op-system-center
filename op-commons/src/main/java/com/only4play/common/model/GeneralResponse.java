package com.only4play.common.model;

import com.only4play.common.constants.BaseEnum;
import com.only4play.common.constants.ErrorCodeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 通用响应
 *
 * @author liyuncong
 * @date 2023/11/06 14:17
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class GeneralResponse<T> extends AbstractResponse {
    private T payload;

    public GeneralResponse(T payload) {
        this.payload = payload;
    }

    public GeneralResponse(int code, String message, T payload) {
        super(code, message);
        this.payload = payload;
    }

    public static <T> GeneralResponse<T> success() {
        return of(ErrorCodeEnum.SUCCESS, null);
    }

    public static <T> GeneralResponse<T> success(T payload) {
        return of(ErrorCodeEnum.SUCCESS, payload);
    }

    public static <T> GeneralResponse<T> success(String message, T payload) {
        return of(ErrorCodeEnum.SUCCESS.getCode(), message, payload);
    }

    public static <T> GeneralResponse<T> failure() {
        return of(ErrorCodeEnum.FAILURE, null);
    }

    public static <T> GeneralResponse<T> failure(String message) {
        return of(ErrorCodeEnum.FAILURE.getCode(), message, null);
    }

    public static <T> GeneralResponse<T> failure(T payload) {
        return of(ErrorCodeEnum.FAILURE, payload);
    }

    public static <T> GeneralResponse<T> failure(String message, T payload) {
        return of(ErrorCodeEnum.FAILURE.getCode(), message, payload);
    }

    public static <T> GeneralResponse<T> of(T payload) {
        return of(ErrorCodeEnum.SUCCESS, payload);
    }

    public static <T> GeneralResponse<T> of(BaseEnum<ErrorCodeEnum> codeEnum, T payload) {
        return of(codeEnum.getCode(), codeEnum.getMessage(), payload);
    }

    public static <T> GeneralResponse<T> of(int code, String message, T payload) {
        return new GeneralResponse<T>(code, message, payload);
    }
}
