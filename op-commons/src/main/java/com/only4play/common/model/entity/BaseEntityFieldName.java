package com.only4play.common.model.entity;

/**
 * BaseEntityFieldName
 *
 * @author liyuncong
 * @date 2023/11/28 14:28
 **/
public class BaseEntityFieldName {
    public static final String UPDATED_AT = "updatedAt";
    public static final String UPDATED_BY = "updatedBy";
    public static final String CREATED_AT = "createdAt";
    public static final String CREATED_BY = "createdBy";
    public static final String ROW_STATUS = "rowStatus";
}
