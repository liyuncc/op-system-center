package com.only4play.common.model.entity;

import java.io.Serializable;

/**
 * 能被识别的对象
 *
 * @author liyuncong
 * @date 2023/11/10 10:28
 **/
public interface IdentifiedObject extends Serializable {

    String getId();
}
