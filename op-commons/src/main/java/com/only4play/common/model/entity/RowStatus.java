package com.only4play.common.model.entity;

/**
 * 行数据状态
 *
 * @author liyuncong
 * @date 2023/11/06 14:26
 **/

public final class RowStatus {
    /**
     * 数据正常
     */
    public static final int OK = 1;
    /**
     * 数据已删除
     */
    public static final int DELETE = 0;
}
