package com.only4play.common.model;

/**
 * Jpa结果响应
 *
 * @author liyuncong
 * @date 2023/11/06 14:09
 **/
public abstract class AbstractJpaResponse implements Response {

    private Long id;
    private Long createdAt;
    private Long updatedAt;
    private Integer version;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
