package com.only4play.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PageableParam {
    private Integer pageIndex;
    private Integer pageSize;
}
