package com.only4play.common.model;

import java.io.Serializable;

/**
 * 访问请求
 *
 * @author liyuncong
 * @date 2023/11/06 14:05
 **/
public interface Request extends Serializable {
}
