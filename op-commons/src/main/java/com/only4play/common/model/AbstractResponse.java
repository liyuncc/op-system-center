package com.only4play.common.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * 响应结果抽象父类
 *
 * @author liyuncong
 * @date 2023/11/06 14:18
 **/
@Data
@NoArgsConstructor
public abstract class AbstractResponse {
    protected Integer code;
    protected String message;
    protected LocalDateTime timestamp = LocalDateTime.now();

    public AbstractResponse(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
