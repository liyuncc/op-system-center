package com.only4play.common.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class PageableRequest extends PageableParam {
    private String order;
}
