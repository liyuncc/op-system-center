package com.only4play.common.model;

import lombok.Value;

@Value
public class ValidateResult {
    String name;
    String message;

    public ValidateResult(String name, String message) {
        this.name = name;
        this.message = message;
    }

    public static ValidateResult of(String name, String message) {
        return new ValidateResult(name, message);
    }
}
