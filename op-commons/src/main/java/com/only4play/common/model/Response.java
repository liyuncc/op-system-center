package com.only4play.common.model;

import java.io.Serializable;

/**
 * 结果响应
 *
 * @author liyuncong
 * @date 2023/11/06 14:07
 **/
public interface Response extends Serializable {
}
