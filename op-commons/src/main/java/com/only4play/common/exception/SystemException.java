package com.only4play.common.exception;

/**
 * SystemException
 *
 * @author liyuncong
 * @date 2023/11/06 13:49
 **/
public class SystemException extends RuntimeException {

    private String msg;

    public SystemException(String msg) {
        super(msg);
        this.msg = msg;
    }
}
