package com.only4play.common.exception;

import com.only4play.common.constants.ErrorCodeEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * 强制业务异常必须提供code码，便于统一维护
 *
 * @author liyuncong
 * @date 2023/11/06 13:50
 **/
@Getter
public class ApplicationException extends RuntimeException {

    private final ErrorCodeEnum errorCodeEnum;
    @Setter
    private Object data;

    public ApplicationException(ErrorCodeEnum ece) {
        super(ece.getMessage());
        this.errorCodeEnum = ece;
    }

    public ApplicationException(ErrorCodeEnum ece, Object data) {
        super(ece.getMessage());
        this.errorCodeEnum = ece;
        this.data = data;
    }

    public ApplicationException(ErrorCodeEnum ece, Throwable cause) {
        super(ece.getMessage(), cause);
        this.errorCodeEnum = ece;
    }

}
