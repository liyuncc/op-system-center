package com.only4play.common.exception;

import com.only4play.common.model.ValidateResult;
import lombok.Getter;

import java.util.List;

@Getter
public class ValidationException extends RuntimeException {

    private final List<ValidateResult> result;

    public ValidationException(List<ValidateResult> list) {
        super();
        this.result = list;
    }
}
