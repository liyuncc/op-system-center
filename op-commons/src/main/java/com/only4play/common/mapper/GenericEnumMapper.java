package com.only4play.common.mapper;

import com.only4play.common.constants.ValidStatus;

/**
 * GenericEnumMapper
 *
 * @author liyuncong
 * @date 2023/11/06 10:27
 **/
public class GenericEnumMapper {
    public Integer asInteger(ValidStatus status) {
        return status.getCode();
    }

    public ValidStatus asValidStatus(Integer code) {
        return ValidStatus.of(code).orElse(ValidStatus.INVALID);
    }
}
