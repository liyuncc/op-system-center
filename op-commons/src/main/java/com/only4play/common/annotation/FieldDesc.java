package com.only4play.common.annotation;

/**
 * FieldDesc
 *
 * @author liyuncong
 * @date 2023/11/06 10:31
 **/
public @interface FieldDesc {
    String name() default "";
}
