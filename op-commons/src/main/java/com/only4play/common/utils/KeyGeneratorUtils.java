package com.only4play.common.utils;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.spec.ECFieldFp;
import java.security.spec.ECParameterSpec;
import java.security.spec.ECPoint;
import java.security.spec.EllipticCurve;

/**
 * @author liyuncong
 * @version 1.0
 * @file KeyGeneratorUtils
 * @brief KeyGeneratorUtils
 * @details KeyGeneratorUtils
 * @date 2023-12-12
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-12-12               liyuncong          Created
 */
public class KeyGeneratorUtils {

    private static final String RSA_ALGORITHM = "RSA";
    private static final String EC_ALGORITHM = "EC";
    private static final String HMAC_SHA256_ALGORITHM = "HmacSha256";
    private static final int KEY_SIZE = 2048;

    private KeyGeneratorUtils() {
    }

    /**
     * 生成RSA密钥对
     */
    public static KeyPair generateRsaKey() {
        KeyPair keyPair;
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(RSA_ALGORITHM);
            keyPairGenerator.initialize(KEY_SIZE);
            keyPair = keyPairGenerator.generateKeyPair();
        } catch (NoSuchAlgorithmException ex) {
            throw new IllegalStateException(ex);
        }
        return keyPair;
    }

    /**
     * 生成EC密钥对
     */
    public static KeyPair generateEcKey() {
        EllipticCurve ellipticCurve = new EllipticCurve(
            new ECFieldFp(
                new BigInteger("115792089210356248762697446949407573530086143415290314195533631308867097853951")),
            new BigInteger("115792089210356248762697446949407573530086143415290314195533631308867097853948"),
            new BigInteger("41058363725152142129326129780047268409114441015993725554835256314039467401291"));
        ECPoint ecPoint = new ECPoint(
            new BigInteger("48439561293906451759052585252797914202762949526041747995844080717082404635286"),
            new BigInteger("36134250956749795798585127919587881956611106672985015071877198253568414405109"));
        ECParameterSpec ecParameterSpec = new ECParameterSpec(
            ellipticCurve,
            ecPoint,
            new BigInteger("115792089210356248762697446949407573529996955224135760342422259061068512044369"),
            1);

        KeyPair keyPair;
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(EC_ALGORITHM);
            keyPairGenerator.initialize(ecParameterSpec);
            keyPair = keyPairGenerator.generateKeyPair();
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
        return keyPair;
    }

    /**
     * 生成HmacSha256密钥对
     */
    static SecretKey generateSecretKey() {
        SecretKey hmacKey;
        try {
            hmacKey = KeyGenerator.getInstance(HMAC_SHA256_ALGORITHM).generateKey();
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
        return hmacKey;
    }
}
