package com.only4play.common.utils;

import java.util.Base64;

/**
 * @author liyuncong
 * @version 1.0
 * @file Base64Utils
 * @brief Base64Utils
 * @details Base64Utils
 * @date 2024-02-01
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2024-02-01               liyuncong          Created
 */
public class Base64Utils {

    /**
     * BASE64 编码
     *
     * @param input 原始字符串
     * @return 编码后的字符串
     */
    public static String encode(byte[] input) {
        return Base64.getEncoder().encodeToString(input);
    }

    /**
     * BASE64 编码
     *
     * @param input 原始字符串
     * @return 编码后的字符串
     */
    public static String encode(String input) {
        byte[] bytes = input.getBytes();
        return Base64.getEncoder().encodeToString(bytes);
    }

    /**
     * BASE64 解码
     *
     * @param encoded 编码后的字符串
     * @return 原始字符串
     */
    public static String decode(String encoded) {
        byte[] bytes = Base64.getDecoder().decode(encoded);
        return new String(bytes);
    }

    /**
     * Base64加密对url安全
     *
     * @param string 明文
     * @return 密文
     */
    public static String encodeBase64URLSafe(String string) {
        return Base64.getEncoder().encodeToString(string.getBytes())
            .replace("+", "-").replace("/", "_").replace("=", "");
    }

    /**
     * Base64解密对url安全
     *
     * @param string 密文
     * @return 明文
     */
    public static String decodeBase64URLSafe(String string) {
        String base64 = string.replace("-", "+").replace("_", "/");
        int mod = base64.length() % 4;
        if (mod > 0)
            base64 += "====".substring(mod);
        return new String(Base64.getDecoder().decode(base64));
    }
}
