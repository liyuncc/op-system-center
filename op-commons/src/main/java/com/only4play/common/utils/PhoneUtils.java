package com.only4play.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

/**
 * 手机号码工具类
 *
 * @author liyuncong
 * @date 2023/11/15 14:58
 **/
public class PhoneUtils {

    /**
     * 手机号格式校验正则
     */
    public static final String PHONE_REGEX = "^1(3|4|5|6|7|8|9)\\d{9}$";

    /**
     * 手机号脱敏筛选正则
     */
    public static final String PHONE_BLUR_REGEX = "(\\d{3})\\d{4}(\\d{4})";

    /**
     * 手机号脱敏替换正则
     */
    public static final String PHONE_BLUR_REPLACE_REGEX = "$1****$2";

    /**
     * 手机号格式校验
     *
     * @param phone 手机号码
     * @return 校验结果
     */
    public static boolean check(String phone) {
        if (StringUtils.isEmpty(phone)) {
            return false;
        }
        return phone.trim().matches(PHONE_REGEX);
    }

    /**
     * 手机号脱敏处理
     *
     * @param phone 手机号码
     * @return 脱敏结果
     */
    public static String blur(String phone) {
        if (!check(phone)) {
            throw new IllegalArgumentException("Incorrect phone number format.");
        }
        return phone.replaceAll(PHONE_BLUR_REGEX, PHONE_BLUR_REPLACE_REGEX);
    }
}
