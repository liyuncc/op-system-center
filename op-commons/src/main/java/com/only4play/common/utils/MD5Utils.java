package com.only4play.common.utils;

import com.only4play.common.constants.ErrorCodeEnum;
import com.only4play.common.exception.ApplicationException;
import org.springframework.security.crypto.codec.Hex;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Base64;

/**
 * @author liyuncong
 * @version 1.0
 * @file MD5Utils
 * @brief MD5Utils
 * @details MD5Utils
 * @date 2024-02-01
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2024-02-01               liyuncong          Created
 */
public class MD5Utils {

    public static String encrypt(String content) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            byte[] bytes = digest.digest(content.getBytes(StandardCharsets.UTF_8));
            return HexUtils.encodeHex(bytes);
        } catch (Exception exception) {
            throw new ApplicationException(ErrorCodeEnum.SERVER_ERROR);
        }
    }
}
