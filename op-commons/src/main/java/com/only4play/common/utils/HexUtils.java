package com.only4play.common.utils;

import jakarta.xml.bind.DatatypeConverter;

/**
 * @author liyuncong
 * @version 1.0
 * @file HexUtils
 * @brief HexUtils
 * @details HexUtils
 * @date 2024-02-02
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2024-02-02               liyuncong          Created
 */
public class HexUtils {

    /**
     * 对字节数组进行十六进制编码
     *
     * @param data 待编码的字节数组
     * @return 十六进制编码后的字符串
     */
    public static String encodeHex(byte[] data) {
        return DatatypeConverter.printHexBinary(data);
    }

    /**
     * 对十六进制编码的字符串进行解码
     *
     * @param encodedString 十六进制编码的字符串
     * @return 解码后的字节数组
     */
    public static byte[] decodeHex(String encodedString) {
        return DatatypeConverter.parseHexBinary(encodedString);
    }
}
