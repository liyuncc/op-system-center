package com.only4play.common.constants;

import java.util.Optional;

public enum ValidStatus implements BaseEnum<ValidStatus> {
    /**
     * 有效
     */
    VALID(1, "valid"),
    /**
     * 无效
     */
    INVALID(0, "invalid");

    private final Integer code;
    private final String message;

    ValidStatus(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public Integer getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    public static Optional<ValidStatus> of(Integer code) {
        return Optional.ofNullable(BaseEnum.parseByCode(ValidStatus.class, code));
    }
}
