package com.only4play.common.constants;

import java.util.Optional;

public enum ErrorCodeEnum implements BaseEnum<ErrorCodeEnum> {

    SUCCESS(0, "操作成功"),
    FAILURE(1, "操作失败"),
    INVALID_USERNAME_OR_PASSWORD(10000, "用户名或密码无效"),
    NOT_FOUND_ERROR(10001, "未查询到信息"),
    SAVE_ERROR(10002, "保存信息失败"),
    UpdateError(10003, "更新信息失败"),
    VALIDATE_ERROR(10004, "数据检验失败"),
    VALIDATE_CAPTCHA_ERROR(100041, "验证码错误"),
    VALIDATE_CAPTCHA_TIMEOUT(100042, "验证码过期"),
    StatusHasValid(10005, "状态已经被启用"),
    StatusHasInvalid(10006, "状态已经被禁用"),
    SystemError(10007, "系统异常"),
    BusinessError(10008, "业务异常"),
    PARAM_ILLEGAL(10009, "参数设置非法"),
    TransferStatusError(10010, "当前状态不正确，请勿重复提交"),
    NotGrant(10011, "没有操作该功能的权限，请联系管理员"),
    UNAUTHORIZED(10012, "未登录"),
    SERVER_ERROR(10013, "服务内部报错");

    private final Integer code;
    private final String message;

    ErrorCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }


    @Override
    public Integer getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    public static Optional<ErrorCodeEnum> of(Integer code) {
        return Optional.ofNullable(BaseEnum.parseByCode(ErrorCodeEnum.class, code));
    }

}
