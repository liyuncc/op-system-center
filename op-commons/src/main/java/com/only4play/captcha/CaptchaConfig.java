package com.only4play.captcha;

import jakarta.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

/**
 * @author liyuncong
 * @version 1.0
 * @file CaptchaConfig
 * @brief CaptchaConfig
 * @details CaptchaConfig
 * @date 2024-02-20
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2024-02-20               liyuncong          Created
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CaptchaConfig {
    // 是忽略大小写校验
    private Boolean ignoreCase = true;

    // 过期时间
    private Long expireTime = CaptchaConstant.DEFAULT_EXPIRE_TIME;
    // 随机字符范围
    private char[] randString = CaptchaConstant.DEFAULT_RAND_STRING;

    // 图片格式
    private String imgFormat = CaptchaConstant.IMG_FORMAT;
    // base64 图片前缀
    private String base64Prefix = CaptchaConstant.BASE64_PREFIX;

    // 图片属性
    private int imageWidth = CaptchaConstant.IMAGE_WIDTH;
    private int imageHeight = CaptchaConstant.IMAGE_HEIGHT;
    private int imageLineSize = CaptchaConstant.IMAGE_LINE_SIZE;
    private int imageCharNumber = CaptchaConstant.IMAGE_CHAR_NUMBER;
    private int imageFontSize = CaptchaConstant.IMAGE_FONT_SIZE;
}
