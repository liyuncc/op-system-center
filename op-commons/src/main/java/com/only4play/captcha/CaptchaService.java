package com.only4play.captcha;

/**
 * @author liyuncong
 * @version 1.0
 * @file CaptchaService
 * @brief CaptchaService
 * @details CaptchaService
 * @date 2024-02-20
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2024-02-20               liyuncong          Created
 */
public interface CaptchaService {

    CaptchaPayload generate(String randomKey);

    CaptchaPayload generate(CaptchaConfig config, String randomKey);


}
