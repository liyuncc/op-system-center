package com.only4play.captcha;

/**
 * @author liyuncong
 * @version 1.0
 * @file CaptchaConstant
 * @brief CaptchaConstant
 * @details CaptchaConstant
 * @date 2024-02-20
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2024-02-20               liyuncong          Created
 */
public interface CaptchaConstant {

    Long DEFAULT_EXPIRE_TIME = 60L * 60L;
    char[] DEFAULT_RAND_STRING = {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
        'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
        'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

    String IMG_FORMAT = "JPEG";
    String BASE64_PREFIX = "data:image/jpg;base64,";

    int IMAGE_WIDTH = 95;//图片宽度
    int IMAGE_HEIGHT = 40;//图片高度
    int IMAGE_LINE_SIZE = 10;//干扰线数量
    int IMAGE_CHAR_NUMBER = 4;//随机产生字符数量
    int IMAGE_FONT_SIZE = 25;//字体大小
}
