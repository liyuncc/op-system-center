package com.only4play.jwt;

import com.only4play.jwt.constant.JwtConstant;
import jakarta.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

/**
 * @author liyuncong
 * @date 2022-03-23
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class JwtConfig {
    private Long expireSeconds;
    private String issuer;
    private String secret;
    private Boolean validateWhenDecode;

    @PostConstruct
    private void init() {
        if (Objects.isNull(expireSeconds)) {
            expireSeconds = JwtConstant.DEFAULT_SECONDS;
        }
        if (Objects.isNull(issuer)) {
            issuer = JwtConstant.DEFAULT_ISSUER;
        }
        if (Objects.isNull(secret)) {
            secret = JwtConstant.DEFAULT_SECRET;
        }
        if (Objects.isNull(validateWhenDecode)) {
            validateWhenDecode = Boolean.TRUE;
        }
    }
}
