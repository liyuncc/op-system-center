package com.only4play.jwt;

import com.google.common.base.Strings;
import com.only4play.common.constants.ErrorCodeEnum;
import com.only4play.common.exception.ApplicationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author liyuncong
 * @version 1.0
 * @file JwtPayload
 * @brief JwtPayload
 * @details JwtPayload
 * @date 2022-03-23
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2022-03-23               liyuncong        Created
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class JwtPayload<T> {
    private String loginUserId;
    private String loginUserName;
    private T properties;

    /**
     * 验证是否有效.
     */
    public void validate(final JwtConfig config) {
        if (Strings.isNullOrEmpty(loginUserId)
            || Strings.isNullOrEmpty(loginUserName)) {
            throw new ApplicationException(ErrorCodeEnum.UNAUTHORIZED);
        }
    }
}
