package com.only4play.jwt;

/**
 * @author liyuncong
 * @version 1.0
 * @file JwtCodec.java
 * @brief JwtCodec
 * @details JwtCodec
 * @date 2022-03-23
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2022-03-23               liyuncong        Created
 */
public interface JwtCodec<T> {
    String encode(JwtPayload<T> payload, JwtConfig config);

    JwtPayload<T> decode(String input, JwtConfig config);

    boolean verify(String input, JwtConfig config);
}
