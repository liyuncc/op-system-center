package com.only4play.jwt.constant;

public class JwtConstant {

    public static final String CLAIM_LOGIN_USER_ID = "loginUserId";
    public static final String CLAIM_LOGIN_USER_NAME = "loginUserName";
    public static final String CLAIM_PROPERTIES = "properties";

    public static final Long DEFAULT_SECONDS = 60L * 60L;
    public static final String DEFAULT_ISSUER = "liyunc";
    public static final String DEFAULT_SECRET =
            "qXmfHZqWOR4W6cSt2HJWvOSKDb8O0ivabqbpFR65MQFxPqWtjJF4nWW8UwdYABAi"
                    + "yv8fzxTYxaxbYQQ8JlFve9bXDEbVIzLqTF1OqGtMdPMEJ4yBGStzAPcmyHCAyHtv"
                    + "0KGdSFMS8zifBcbrBVzJ0T6cyG9dQvUSYlCqvBdF680mmnY9k6DpR1zgbdjMroHk"
                    + "BRfxBbvzlJSck6rPMm0tx13WVAIez0lVTp2A4384c57oMUjoqNNp1ggDYFS43J6e"
                    + "D5WvAiRa9lydLXj5ZX7n7Qsrylbfrw5erswgGoTktGjhfa5P7ymkfD85ppiIYKb6"
                    + "TFZbt3NlQwFOqFX3Zt0AmouCOqjhjGOuSwzqkeY5rOUDNw33KgDTjBk1lb2tkmd9"
                    + "ocKsdcMRxvan9jHXAkIyFkaHpHOf4hgKJQsvUUCNlNMy5ujS6Szn8OSCJoqZcV7c"
                    + "ef5T9AW53qLn0xxClKtlhkTXDrJKal1iyUqkT7iuv8u0g7WNsnaD7ldIwSjrf6Hb"
                    + "Ik2uePmv2jEzj6iakoZisCcdt8ktL6x8oFtYhzBoJqb0jto5SwSTwmFowJBOXRhm"
                    + "kqrKc9fXzGU2SSOX5ZDx6uF0MoTTkf8JKDH9hChLM1R97FDBCs3WFNIqOIVBZIxR"
                    + "oJ3HItiCwWiDMTfzQl76H59cy4kctyebPzN7FiFI8tKkdj8eZ0dw5BfmeFw2xez5"
                    + "FFURUjElFB3u3ershE6rUFK0GU3mZtFRIakM68q48oSX0AfpV1pRljVzo1xP6SGY"
                    + "N3T9qOh3IMm7qpEWchab0PQN8WxXg4GrjcKu0n12CtEbTnPSkSjkKGCempGC0hVa"
                    + "0bh79vDI9a3aq1A9b3AnoMhOVvg7PcEFE9ZBeJqkvcgvvZhut9QNsy0YXPTRGcpY"
                    + "8XDVKWVfnVKAZ6ZiDlcr7Ss6hZaTRKD2dc60X7SndLPai929RUDIJ24LkA96hNLm"
                    + "lFsHEGW0QQK25xYLPeUS5sxgxMfAzS37fAFq0RtseeHDLOflS0tZipnyhGmHXES7";
}
