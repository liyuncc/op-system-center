package com.only4play.batch;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * BatchExecuteService
 *
 * @author liyuncong
 * @date 2023/11/27 14:22
 **/
public abstract class BatchExecuteService<T> {

    @Autowired
    protected BatchExecuteConfig config;

    public <S extends T> void invokeAll(
        Collection<Collection<S>> collections,
        Consumer<Collection<S>> consumer,
        ExecutorService executorService
    ) throws Exception {
        List<Callable<Void>> callables = collections.stream()
            .map(item -> (Callable<Void>) () -> {
                consumer.accept(item);
                return null;
            })
            .collect(Collectors.toList());
        executorService.invokeAll(callables);
    }
}
