package com.only4play.batch;

import jakarta.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Configuration;

import java.util.Objects;

/**
 * 批量执行配置
 *
 * @author liyuncong
 * @date 2023/11/27 13:55
 **/
@Getter
@Setter
@Configuration
public class BatchExecuteConfig {

    private Integer processorSize;
    // 如为jpa，需要在yml文件添加同样配置：spring.jpa.properties.hibernate.jdbc.batch_size=xxx
    private Integer batchSize;

    @PostConstruct
    public void init() {
        if (Objects.isNull(processorSize) || processorSize == 0) {
            this.processorSize = Runtime.getRuntime().availableProcessors() + 2;
        }
        if (Objects.isNull(batchSize) || batchSize == 0) {
            this.batchSize = 5000;
        }
    }
}
