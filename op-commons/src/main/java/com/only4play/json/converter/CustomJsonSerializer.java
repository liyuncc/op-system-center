package com.only4play.json.converter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * @author liyuncong
 * @version 1.0
 * @file CustomJsonSerializer
 * @brief CustomJsonSerializer
 * @details CustomJsonSerializer
 * @date 2022/5/21
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                        NAME               DESCRIPTION
 * 2022/5/21                     liyuncong          Created
 */
@Slf4j
public class CustomJsonSerializer {

    public static final CustomDateJsonSerializer dateJsonSerializer =
        new CustomDateJsonSerializer();

    private static class CustomDateJsonSerializer extends JsonSerializer<LocalDateTime> {
        private final DateTimeFormatter format = DateTimeFormatter.ISO_OFFSET_DATE_TIME;

        @Override
        public void serialize(LocalDateTime value,
                              JsonGenerator generator,
                              SerializerProvider provider
        ) throws IOException {
            generator.writeString(format.format(value.atZone(ZoneId.systemDefault())));
        }
    }
}
