package com.only4play.common;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author liyuncong
 * @version 1.0
 * @file PasswordGeneratorTest
 * @brief PasswordGenerator单元测试
 * @details PasswordGenerator单元测试
 * @date 2023-11-18
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-11-18               liyuncong          Created
 */
@Slf4j
public class PasswordGeneratorTest {

    private static final String loginUsername = "yuncong.li@outlook.com";
    private static final String loginPassword = "Z7X8ir=>nu~A1L]@";
    private static final String salt = "#UZ0#f2;7S|&pBvz";

    PasswordGenerator passwordGenerator = new PasswordGenerator() {
    };
    PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Test
    public void doTest() {
        String generate = passwordGenerator.generate(loginUsername, loginPassword, salt, passwordEncoder);
        log.info("generate: {}", generate);
        Assert.assertNotNull(generate);
        boolean verify = passwordGenerator.verify(loginUsername, loginPassword, salt, generate, passwordEncoder);
        log.info("verify: {}", verify);
        Assert.assertTrue(verify);
    }
}
