package com.only4play.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;

/**
 * AesUtils单元测试
 *
 * @author liyuncong
 * @date 2023/11/15 14:44
 **/
@Slf4j
public class AesUtilsTest {

    private final String content = "liyuncong";

    @Test
    public void testEncryptAndDecryptWithDefault() throws Exception {
        String encrypt = AesUtils.encrypt(content);
        log.info("encrypt: {}", encrypt);
        String decrypt = AesUtils.decrypt(encrypt);
        log.info("decrypt: {}", decrypt);
        Assert.assertEquals(decrypt, content);

        String encode = Base64Utils.encode(encrypt);
        log.info("encode = {}", encode);
        String decode = Base64Utils.decodeBase64URLSafe(encode);
        log.info("decode = {}", decode);


    }
}
