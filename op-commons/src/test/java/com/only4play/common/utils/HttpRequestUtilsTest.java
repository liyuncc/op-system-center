package com.only4play.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

/**
 * @author liyuncong
 * @version 1.0
 * @file HttpRequestUtils
 * @brief HttpRequestUtils
 * @details HttpRequestUtils
 * @date 2024-03-14
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2024-03-14               liyuncong          Created
 */

@Slf4j
public class HttpRequestUtilsTest {

    @Test
    public void doExecute() {
        HttpRequestUtils httpRequestUtils = new HttpRequestUtils();
        String url = "http://baidu.com";
        ResponseEntity<String> responseEntity = httpRequestUtils.execute(url, HttpMethod.GET, null, null, null, String.class);
        log.info("doExecute: execute={}", responseEntity.getStatusCode());
        ResponseEntity<String> getResponse = httpRequestUtils.doGet(url, null, String.class);
        log.info("doExecute: doGet={}", getResponse.getStatusCode());
        ResponseEntity<String> postResponse = httpRequestUtils.doPost(url, null, String.class);
        log.info("doExecute: doPost={}", postResponse.getStatusCode());
    }
}
