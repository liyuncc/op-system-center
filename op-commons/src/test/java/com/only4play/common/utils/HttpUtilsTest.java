package com.only4play.common.utils;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * HttpUtils单元测试
 *
 * @author liyuncong
 * @date 2023/11/21 10:43
 **/
@Slf4j
public class HttpUtilsTest {

    @SuppressFBWarnings("UC_USELESS_VOID_METHOD")
    @Test
    public void postTest() {
        String url = "http://127.0.0.1:8080/api/v1/auth/access_token";
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("grantType", "password");
        paramMap.put("appId", "123");
        paramMap.put("appSecret", "appSecret");
        paramMap.put("code", "code");
        // skip unit test
//        String response = HttpUtils.post(url, null, paramMap, null);
//        log.info("response: {}", response);
    }
}
