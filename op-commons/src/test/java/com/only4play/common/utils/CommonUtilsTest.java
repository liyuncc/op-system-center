package com.only4play.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;

/**
 * CommonUtils单元测试
 *
 * @author liyuncong
 * @date 2023/11/15 15:32
 **/
@Slf4j
public class CommonUtilsTest {

    @Test
    public void testUuid() {
        String uuid = CommonUtils.uuid();
        log.info("uuid: {}", uuid);
        Assert.assertNotNull(uuid);
    }

    @Test
    public void testRandomString() {
        int len = 16;
        String string = CommonUtils.randomString(len);
        log.info("randomString: {}", string);
        Assert.assertNotNull(string);
    }
}
