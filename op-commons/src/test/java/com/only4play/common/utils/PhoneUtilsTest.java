package com.only4play.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;

/**
 * PhoneUtils单元测试
 *
 * @author liyuncong
 * @date 2023/11/15 15:07
 **/
@Slf4j
public class PhoneUtilsTest {

    private static final String phone = "18845562014";

    @Test
    public void testCheck() {
        boolean check = PhoneUtils.check(phone);
        Assert.assertTrue(check);
    }

    @Test
    public void testBlur() {
        String blur = PhoneUtils.blur(phone);
        log.info("blur: {}", blur);
        Assert.assertNotNull(blur);
    }
}
