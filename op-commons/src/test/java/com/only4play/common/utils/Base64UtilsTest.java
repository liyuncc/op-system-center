package com.only4play.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author liyuncong
 * @version 1.0
 * @file Base64UtilsTest
 * @brief Base64UtilsTest
 * @details Base64UtilsTest
 * @date 2024-03-06
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2024-03-06               liyuncong          Created
 */

@Slf4j
public class Base64UtilsTest {

    @Test
    public void doTest() {
        String origin = "liyuncong";
        String encode = Base64Utils.encodeBase64URLSafe(origin);
        log.info("encode = {}", encode);
        String decode = Base64Utils.decodeBase64URLSafe(encode);
        log.info("decode = {}", decode);
        Assert.assertEquals(origin, decode);
    }
}
