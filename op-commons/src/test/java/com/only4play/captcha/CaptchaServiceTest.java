package com.only4play.captcha;

import com.only4play.captcha.impl.CaptchaServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * @author liyuncong
 * @version 1.0
 * @file CaptchaServiceTest
 * @brief CaptchaServiceTest
 * @details CaptchaServiceTest
 * @date 2024-02-20
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2024-02-20               liyuncong          Created
 */
@Slf4j
public class CaptchaServiceTest {

    @Test
    public void doGenImageCode() {
        CaptchaService captchaService = new CaptchaServiceImpl();
        CaptchaPayload payload = captchaService.generate("1234");
        log.info("image code base64 : {}", payload.base64());
        log.info("image code randomKey : {}", payload.getRandomKey());
        log.info("image code code : {}", payload.getCode());
        log.info("image code expireTime : {}", payload.getExpireTime());
    }
}
