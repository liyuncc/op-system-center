package com.only4play.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.only4play.jwt.impl.JwtCodecImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;

/**
 * JwtCodec单元测试
 *
 * @author liyuncong
 * @date 2023/11/16 10:36
 **/
@Slf4j
public class JwtCodecTest {

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final JwtCodec<String> codec = new JwtCodecImpl<>(objectMapper);
    private final JwtConfig config = new JwtConfig(
            24 * 3600L,
            "liyuncong",
            "27MLSKZvJsIY27Ma6KgnZggIaL118mV4jt0Y9",
            false
    );

    @Test
    public void doTest() {
        JwtPayload<String> payload = new JwtPayload<>(
                "18845562014",
                "liyuncong",
                "for test"
        );
        String jwt = codec.encode(payload, config);
        log.info("encode jwt: {}", jwt);
        Assert.assertNotNull(jwt);
        boolean verify = codec.verify(jwt, config);
        log.info("verify jwt: {}", verify);
        Assert.assertTrue(verify);
        JwtPayload<String> decode = codec.decode(jwt, config);
        log.info("decode payload: {}", decode);
        Assert.assertEquals(decode.getLoginUserId(), payload.getLoginUserId());
        Assert.assertEquals(decode.getLoginUserName(), payload.getLoginUserName());
        Assert.assertEquals(decode.getProperties(), payload.getProperties());
    }
}
