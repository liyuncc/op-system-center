package com.only4play.verification.model;

import java.io.Serializable;

/**
 * @author liyuncong
 * @version 1.0
 * @file BeanModel
 * @brief BeanModel
 * @details BeanModel
 * @date 2023-12-27
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-12-27               liyuncong          Created
 */
public class BeanModel implements Serializable {

}
