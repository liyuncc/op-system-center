package com.only4play.verification.annotation;

/**
 * @author liyuncong
 * @version 1.0
 * @file ParamCheck
 * @brief ParamCheck
 * @details ParamCheck
 * @date 2023-12-27
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-12-27               liyuncong          Created
 */
public @interface ParamCheck {

}
