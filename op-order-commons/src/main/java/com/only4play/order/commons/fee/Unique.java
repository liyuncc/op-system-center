package com.only4play.order.commons.fee;

/**
 * 计算器唯一编码
 *
 * @author liyuncong
 * @date 2023/11/06 15:32
 **/
public interface Unique {
    /**
     * 获取唯一编码
     *
     * @return 唯一编码
     */
    Integer getUniqueCode();
}
