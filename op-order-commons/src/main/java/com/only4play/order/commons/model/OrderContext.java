package com.only4play.order.commons.model;

import com.only4play.order.commons.constants.BizEnum;
import com.only4play.order.commons.filters.selector.FilterSelector;

/**
 * 订单上下文
 *
 * @author liyuncong
 * @date 2023/11/06 16:50
 **/
public interface OrderContext {

    /**
     * 获取项目编码
     *
     * @return
     */
    BizEnum getBizCode();

    /**
     * 获取过滤器选择器
     *
     * @return
     */
    FilterSelector getFilterSelector();

    /**
     * 是否继续链
     *
     * @return
     */
    boolean continueChain();
}
