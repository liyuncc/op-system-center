package com.only4play.order.commons.fee;

import com.only4play.common.constants.BaseEnum;

import java.util.Optional;

/**
 * 费用枚举
 *
 * @author liyuncong
 * @date 2023/11/06 15:52
 **/
public enum FeeEnum implements BaseEnum<FeeEnum> {
    AMOUNT_GREATER_ERROR(2001, "抵扣金额过大,超过需要抵扣的金额"),
    FEE_ITEM_EMPTY(2002, "计费项为空");


    private final Integer code;
    private final String message;

    FeeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public static Optional<FeeEnum> of(Integer code) {
        return Optional.ofNullable(BaseEnum.parseByCode(FeeEnum.class, code));
    }

    @Override
    public Integer getCode() {
        return this.code;
    }


    @Override
    public String getMessage() {
        return this.message;
    }
}
