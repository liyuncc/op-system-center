package com.only4play.order.commons.fee;

import java.math.BigDecimal;

/**
 * 费用项目抽象父类
 *
 * @author liyuncong
 * @date 2023/11/06 15:54
 **/
public class AbstractFeeItem<O> implements FeeItem<O> {

    private final O orderInfo;
    private final FeeItemType itemType;
    private final BigDecimal itemMoney;

    public AbstractFeeItem(O orderInfo, FeeItemType itemType, BigDecimal itemMoney) {
        this.orderInfo = orderInfo;
        this.itemType = itemType;
        this.itemMoney = itemMoney;
    }

    @Override
    public O getOrderInfo() {
        return orderInfo;
    }

    @Override
    public FeeItemType getFeeItemType() {
        return itemType;
    }

    @Override
    public BigDecimal getFeeItemOriginMoney() {
        return itemMoney;
    }
}
