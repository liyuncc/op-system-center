package com.only4play.order.commons.model;

import com.only4play.order.commons.constants.BizEnum;
import com.only4play.order.commons.filters.selector.FilterSelector;

/**
 * 订单上下文抽象父类
 *
 * @author liyuncong
 * @date 2023/11/06 16:53
 **/
public abstract class AbstractOrderContext implements OrderContext {

    private final BizEnum bizEnum;
    private final FilterSelector selector;

    public AbstractOrderContext(BizEnum bizEnum, FilterSelector selector) {
        this.bizEnum = bizEnum;
        this.selector = selector;
    }

    public BizEnum getBizEnum() {
        return bizEnum;
    }

    public FilterSelector getSelector() {
        return selector;
    }
}
