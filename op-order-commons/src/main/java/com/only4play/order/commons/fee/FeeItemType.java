package com.only4play.order.commons.fee;

import com.only4play.common.constants.BaseEnum;

import java.util.Optional;

/**
 * 费用项目类型
 *
 * @author liyuncong
 * @date 2023/11/06 15:42
 **/
public enum FeeItemType implements BaseEnum<FeeItemType> {
    SERVICE_FEE(1, "服务费"),
    ELECTRIC_FEE(2, "电费"),
    OVER_WEIGHT_FEE(3, "超重费"),
    OVER_TIME_FEE(4, "超时费");

    private final Integer code;
    private final String message;

    FeeItemType(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public static Optional<FeeItemType> of(Integer code) {
        return Optional.ofNullable(BaseEnum.parseByCode(FeeItemType.class, code));
    }

    @Override
    public Integer getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
