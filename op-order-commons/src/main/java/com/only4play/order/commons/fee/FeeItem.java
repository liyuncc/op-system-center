package com.only4play.order.commons.fee;

import java.math.BigDecimal;

/**
 * 费用项目
 *
 * @author liyuncong
 * @date 2023/11/06 15:37
 **/
public interface FeeItem<O> {
    /**
     * 原始金额
     *
     * @return
     */
    BigDecimal getFeeItemOriginMoney();

    /**
     * 费用类型
     * @return
     */
    FeeItemType getFeeItemType();

    O getOrderInfo();

}
