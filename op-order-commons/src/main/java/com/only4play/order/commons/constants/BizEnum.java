package com.only4play.order.commons.constants;

/**
 * 业务编码枚举
 *
 * @author liyuncong
 * @date 2023/11/06 15:26
 **/
public enum BizEnum {

    BIZ_XXX(1, "业务编码1");

    BizEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    private final Integer code;
    private final String message;

    public Integer getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }
}
