package com.only4play.order.commons.fee;

import com.only4play.order.commons.pay.PayItem;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

/**
 * 费用计算接口
 *
 * @author liyuncong
 * @date 2023/11/06 15:35
 **/
public interface FeeCalculate<O> {
    /**
     * 根据费用项计算每个费用项明细
     *
     * @param collection
     * @return
     */
    Map<FeeItemType, Collection<PayItem>> payItemCollection(Collection<FeeItem<O>> collection);

    /**
     * waitPay
     *
     * @param collection
     * @return
     */
    Map<FeeItemType, BigDecimal> calculateWaitPay(Collection<FeeItem<O>> collection);

    /**
     * 获取计算器的唯一编码
     *
     * @return
     */
    Unique getUnique();
}
