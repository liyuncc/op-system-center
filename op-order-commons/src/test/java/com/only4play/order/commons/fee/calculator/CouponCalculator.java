package com.only4play.order.commons.fee.calculator;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.only4play.order.commons.fee.*;
import com.only4play.order.commons.pay.PayItem;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

/**
 * @author gim 2021/12/6 8:30 下午
 */
public class CouponCalculator extends AbstractCalculator<OrderInfo> {

    public CouponCalculator(FeeCalculate feeCalculate) {
        super(feeCalculate, CalculateType.COUPON);
    }

    @Override
    protected Map<FeeItemType, BigDecimal> currentPayItem(
            Map<FeeItemType, BigDecimal> left, OrderInfo o) {
        Map<FeeItemType, BigDecimal> map = Maps.newHashMap();
        map.put(FeeItemType.SERVICE_FEE, new BigDecimal("5"));
        System.out.println("劵抵扣了5元费用");
        return map;
    }

    @Override
    protected Map<FeeItemType, Collection<PayItem>> payItemCollection() {
        Map<FeeItemType, Collection<PayItem>> map = Maps.newHashMap();
        Collection<PayItem> payItems = Lists.newArrayList();
        CouponPayItem cp = new CouponPayItem(new BigDecimal(5));
        cp.setCouponCode("C1234528");
        payItems.add(cp);
        map.put(FeeItemType.SERVICE_FEE, payItems);
        return map;
    }
}
