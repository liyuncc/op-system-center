package com.only4play.codegen.processor.mapper;

public @interface GenMapper {

    String pkgName();

    String sourcePath() default "src/main/java";

    boolean overrideSource() default false;
}
