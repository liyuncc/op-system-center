package com.only4play.codegen.processor.vo;

import com.only4play.jpa.support.entity.AggregateEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class AbstractBaseJpaVO {
    @Schema(title = "数据版本")
    private int version;
    @Schema(title = "主键")
    private String id;
    @Schema(title = "创建时间")
    private Long createdAt;
    @Schema(title = "修改时间")
    private Long updatedAt;

    protected AbstractBaseJpaVO(AggregateEntity source) {
        this.setId(source.getId());
    }

    protected AbstractBaseJpaVO() {
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }
}
