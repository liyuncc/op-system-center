package sources;

import com.only4play.codegen.processor.api.GenCreateRequest;
import com.only4play.codegen.processor.api.GenFeign;
import com.only4play.codegen.processor.api.GenQueryRequest;
import com.only4play.codegen.processor.api.GenResponse;
import com.only4play.codegen.processor.api.GenUpdateRequest;
import com.only4play.codegen.processor.controller.GenController;
import com.only4play.codegen.processor.creator.GenCreator;
import com.only4play.codegen.processor.mapper.GenMapper;
import com.only4play.codegen.processor.query.GenQuery;
import com.only4play.codegen.processor.repository.GenRepository;
import com.only4play.codegen.processor.service.GenService;
import com.only4play.codegen.processor.creator.IgnoreCreator;
import com.only4play.codegen.processor.updater.IgnoreUpdater;
import com.only4play.codegen.processor.service.GenServiceImpl;
import com.only4play.codegen.processor.updater.GenUpdater;
import com.only4play.codegen.processor.vo.GenVo;
import com.only4play.common.constants.ValidStatus;
import com.only4play.jpa.support.converter.ValidStatusConverter;
import com.only4play.jpa.support.entity.AggregateEntity;

import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@EqualsAndHashCode(callSuper = true)
@GenVo(pkgName = "com.only4play.codegen.test.vo", sourcePath = Constants.GEN_API_SOURCE)
@GenCreator(pkgName = "com.only4play.codegen.test.creator", sourcePath = Constants.GEN_API_SOURCE)
@GenUpdater(pkgName = "com.only4play.codegen.test.updater", sourcePath = Constants.GEN_API_SOURCE)
@GenRepository(pkgName = "com.only4play.codegen.test.repository", sourcePath = Constants.GEN_API_SOURCE)
@GenService(pkgName = "com.only4play.codegen.test.service", sourcePath = Constants.GEN_API_SOURCE)
@GenServiceImpl(pkgName = "com.only4play.codegen.test.service.impl", sourcePath = Constants.GEN_API_SOURCE)
@GenQuery(pkgName = "com.only4play.codegen.test.query", sourcePath = Constants.GEN_API_SOURCE)
@GenMapper(pkgName = "com.only4play.codegen.test.mapper", sourcePath = Constants.GEN_API_SOURCE)
@GenController(pkgName = "com.only4play.codegen.test.controller", sourcePath = Constants.GEN_API_SOURCE)
@GenCreateRequest(pkgName = "com.only4play.codegen.test.api.request", sourcePath = Constants.GEN_API_SOURCE)
@GenUpdateRequest(pkgName = "com.only4play.codegen.test.api.request", sourcePath = Constants.GEN_API_SOURCE)
@GenQueryRequest(pkgName = "com.only4play.codegen.test.api.request", sourcePath = Constants.GEN_API_SOURCE)
@GenResponse(pkgName = "com.only4play.codegen.test.api.response", sourcePath = Constants.GEN_API_SOURCE)
@GenFeign(pkgName = "com.only4play.codegen.test.api.service", sourcePath = Constants.GEN_API_SOURCE, serverName = "srv")
@Entity
@Table(name = "STUDENT")
@Getter
@Setter
public class Student extends AggregateEntity {

    @Id
    private String id;

    @Convert(converter = ValidStatusConverter.class)
    @IgnoreUpdater
    @IgnoreCreator
    private ValidStatus validStatus;

    public void init() {
        setValidStatus(ValidStatus.VALID);
    }

    public void valid() {
        setValidStatus(ValidStatus.VALID);
    }

    public void invalid() {
        setValidStatus(ValidStatus.INVALID);
    }
}