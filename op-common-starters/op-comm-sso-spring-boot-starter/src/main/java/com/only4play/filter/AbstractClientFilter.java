package com.only4play.filter;

import com.only4play.listener.LogoutListener;
import com.only4play.session.SessionMappingStorage;
import jakarta.servlet.Filter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Objects;

/**
 * @author liyuncong
 * @version 1.0
 * @file AbstractClientFilter
 * @brief 客户端过滤器
 * @details 客户端过滤器
 * @date 2023-11-19
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-11-19               liyuncong          Created
 */
public abstract class AbstractClientFilter extends ParamContainer implements Filter {

    private SessionMappingStorage sessionMappingStorage;

    public abstract boolean isAccessAllowed(HttpServletRequest request, HttpServletResponse response) throws IOException;

    protected SessionMappingStorage getSessionMappingStorage() {
        if (Objects.isNull(sessionMappingStorage)) {
            sessionMappingStorage = LogoutListener.getSessionMappingStorage();
        }
        return sessionMappingStorage;
    }
}
