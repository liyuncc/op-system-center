package com.only4play.filter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author liyuncong
 * @version 1.0
 * @file ParamContainer
 * @brief 参数过滤器
 * @details 参数过滤器
 * @date 2023-11-19
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-11-19               liyuncong          Created
 */
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class ParamContainer {

    private String appId;
    private String appSecret;
    private String serverUrl;
}
