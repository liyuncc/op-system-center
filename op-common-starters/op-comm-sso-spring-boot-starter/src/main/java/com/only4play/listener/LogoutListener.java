package com.only4play.listener;

import com.only4play.session.SessionMappingStorage;
import com.only4play.session.impl.LocalSessionMappingStorage;

import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.HttpSessionEvent;
import jakarta.servlet.http.HttpSessionListener;

/**
 * @author liyuncong
 * @version 1.0
 * @file LogoutListener
 * @brief 注销监听器
 * @details 注销监听器
 * @date 2023-11-19
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-11-19               liyuncong          Created
 */
public final class LogoutListener implements HttpSessionListener {

    private static SessionMappingStorage sessionMappingStorage = new LocalSessionMappingStorage();


    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        HttpSession session = se.getSession();
        sessionMappingStorage.removeBySessionById(session.getId());
    }

    public static SessionMappingStorage getSessionMappingStorage() {
        return sessionMappingStorage;
    }

    public static void setSessionMappingStorage(SessionMappingStorage sessionMappingStorage) {
        LogoutListener.sessionMappingStorage = sessionMappingStorage;
    }
}
