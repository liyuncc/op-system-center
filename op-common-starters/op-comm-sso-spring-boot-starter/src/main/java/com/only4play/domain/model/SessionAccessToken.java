package com.only4play.domain.model;

import com.only4play.auth.AuthenticatedUser;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

/**
 * @author liyuncong
 * @version 1.0
 * @file SessionAccessToken
 * @brief session授权令牌
 * @details session授权令牌
 * @date 2023-11-19
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-11-19               liyuncong          Created
 */
@Getter
@Setter
public class SessionAccessToken extends RpcAccessToken {

    /**
     * AccessToken过期时间
     */
    private long expirationTime;

    public SessionAccessToken(
        String accessToken, int expiresIn, String refreshToken, AuthenticatedUser<Void> authenticatedUser,
        long expirationTime
    ) {
        super(accessToken, expiresIn, refreshToken, authenticatedUser);
        this.expirationTime = expirationTime;
    }

    public boolean isExpired() {
        return Instant.now().isAfter(Instant.ofEpochMilli(expirationTime));
    }
}
