package com.only4play.domain.model;

import com.only4play.auth.AuthenticatedUser;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * @author liyuncong
 * @version 1.0
 * @file RpcAccessToken
 * @brief rpc访问令牌
 * @details rpc访问令牌
 * @date 2023-11-18
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-11-18               liyuncong          Created
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RpcAccessToken {
    private String accessToken;
    private int expiresIn;
    private String refreshToken;
    private AuthenticatedUser<Void> authenticatedUser;
}
