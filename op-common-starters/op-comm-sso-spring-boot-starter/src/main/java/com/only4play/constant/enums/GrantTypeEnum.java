package com.only4play.constant.enums;

import lombok.Getter;

/**
 * @author liyuncong
 * @version 1.0
 * @file GrantTypeEnum
 * @brief 授权类型枚举
 * @details 授权类型枚举
 * @date 2023-11-18
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-11-18               liyuncong          Created
 */
@Getter
public enum GrantTypeEnum {

    AUTHORIZATION_CODE(1, "code"),
    PASSWORD(2, "password");

    private final Integer code;
    private final String message;

    GrantTypeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
