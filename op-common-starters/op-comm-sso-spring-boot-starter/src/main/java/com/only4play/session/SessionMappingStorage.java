package com.only4play.session;


import jakarta.servlet.http.HttpSession;

/**
 * @author liyuncong
 * @version 1.0
 * @file SessionMappingStorage
 * @brief session关系存储器
 * @details session关系存储器
 * @date 2023-11-19
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-11-19               liyuncong          Created
 */
public interface SessionMappingStorage {

    void addSessionById(String accessToken, HttpSession session);

    void removeBySessionById(String sessionId);

    HttpSession removeSessionByMappingId(String accessToken);
}
