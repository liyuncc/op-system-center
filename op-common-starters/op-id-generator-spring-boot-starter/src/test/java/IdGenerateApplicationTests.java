import com.only4play.idgenerator.service.SingIdGenerator;
import com.only4play.idgenerator.service.impl.SnowFlakeIdWorker;

/**
 * ID生成器单元测试
 *
 * @author liyuncong
 * @date 2023/11/10 10:48
 **/
public class IdGenerateApplicationTests {

    private static final SingIdGenerator idGenerator = new SnowFlakeIdWorker();

    public static void main(String[] args) {
        long workerId = 30L;
        long datacenterId = 15L;
        Long firstId = idGenerator.snowFlake();
        System.out.println(firstId);
        Long secondId = idGenerator.snowFlake(workerId, datacenterId);
        System.out.println(secondId);
    }
}
