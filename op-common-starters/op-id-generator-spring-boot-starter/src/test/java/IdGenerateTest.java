import com.only4play.idgenerator.provider.IdGeneratorOptions;
import com.only4play.idgenerator.provider.IdGeneratorProvider;
import com.only4play.idgenerator.strategy.IdGenerateStrategy;
import lombok.extern.slf4j.Slf4j;

/**
 * @author liyuncong
 * @version 1.0
 * @file IdGenerateTest
 * @brief IdGenerateTest
 * @details IdGenerateTest
 * @date 2024-01-12
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2024-01-12               liyuncong          Created
 */
@Slf4j
public class IdGenerateTest {

    public static void main(String[] args) {
//        snow();
        uuid();
    }

    public static void snow() {
        IdGeneratorProvider snow = new IdGeneratorProvider();
        long l = snow.instance().newLong();
        log.info("snow: " + l);
    }

    public static void uuid() {
        IdGeneratorOptions options = IdGeneratorOptions.builder().generateStrategy(IdGenerateStrategy.UUID).build();
        IdGeneratorProvider uuid = new IdGeneratorProvider(options);
        String string = uuid.instance().newString();
        log.info("uuid: " + string);
    }
}
