package com.only4play.idgenerator.provider;

/**
 * @author liyuncong
 * @version 1.0
 * @file IdGenerator
 * @brief IdGenerator
 * @details IdGenerator
 * @date 2024-01-12
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2024-01-12               liyuncong          Created
 */
public interface IdGenerator {

    long newLong() throws IdGeneratorException;

    String newString() throws IdGeneratorException;
}
