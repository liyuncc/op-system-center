package com.only4play.idgenerator.service;

import com.only4play.idgenerator.provider.IdGeneratorException;

/**
 * @author liyuncong
 * @version 1.0
 * @file SnowFlakeIdentifierGenerator
 * @brief SnowFlakeIdentifierGenerator
 * @details SnowFlakeIdentifierGenerator
 * @date 2024-01-12
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2024-01-12               liyuncong          Created
 */
public interface SnowFlakeIdentifierGenerator {

    long nextId() throws IdGeneratorException;
}
