//package com.only4play.idgenerator.service;
//
//import com.only4play.common.model.entity.IdentifiedObject;
//import com.only4play.idgenerator.strategy.JpaIdStrategy;
//import org.hibernate.HibernateException;
//import org.hibernate.engine.spi.SharedSessionContractImplementor;
//import org.hibernate.id.IdentifierGenerator;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import jakarta.persistence.TableGenerator;
//import java.io.Serializable;
//import java.util.Objects;
//
///**
// * 雪花算法ID生成器
// *
// * @author liyuncong
// * @date 2023/11/10 10:15
// **/
//@Component
//@TableGenerator(name = JpaIdStrategy.SNOW_FLAKE)
//public class SnowFlakeIdGenerator implements IdentifierGenerator {
//
//    @Autowired
//    private SingIdGenerator singIdGenerator;
//
//
//    @Override
//    public Serializable generate(
//        SharedSessionContractImplementor sharedSessionContractImplementor, Object o
//    ) throws HibernateException {
//        if ((o instanceof IdentifiedObject identifiedObject)
//            && Objects.nonNull(identifiedObject.getId())) {
//            return identifiedObject.getId();
//        }
//        return singIdGenerator.snowFlake().toString();
//    }
//
//    @Override
//    public boolean supportsJdbcBatchInserts() {
//        return true;
//    }
//}
