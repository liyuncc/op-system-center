package com.only4play.idgenerator.strategy;

/**
 * @author liyuncong
 * @version 1.0
 * @file IdGenerateStrategy
 * @brief IdGenerateStrategy
 * @details IdGenerateStrategy
 * @date 2024-01-12
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2024-01-12               liyuncong          Created
 */
public enum IdGenerateStrategy {
    UUID,
    SNOW_FLAKE,
    DATABASE,
    REDIS,
}
