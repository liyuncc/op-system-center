package com.only4play.idgenerator.provider;

import com.only4play.idgenerator.strategy.IdGenerateStrategy;
import lombok.*;

/**
 * @author liyuncong
 * @version 1.0
 * @file IdGeneratorOptions
 * @brief IdGeneratorOptions
 * @details IdGeneratorOptions
 * @date 2024-01-12
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2024-01-12               liyuncong          Created
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class IdGeneratorOptions {

    public IdGenerateStrategy generateStrategy = IdGenerateStrategy.SNOW_FLAKE;

    /**
     * 机器码
     */
    public long workId = 0;

    /**
     * 数据中心ID
     */
    public long datacenterId = 0;
}
