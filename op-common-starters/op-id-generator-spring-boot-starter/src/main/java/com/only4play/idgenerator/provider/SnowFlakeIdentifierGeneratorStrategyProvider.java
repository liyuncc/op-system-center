//package com.only4play.idgenerator.provider;
//
//import com.only4play.idgenerator.service.SnowFlakeIdGenerator;
//import com.only4play.idgenerator.strategy.JpaIdStrategy;
//import org.hibernate.jpa.spi.IdentifierGeneratorStrategyProvider;
//
//import java.util.Map;
//
///**
// * 雪花算法ID生成器策略Provider
// *
// * @author liyuncong
// * @date 2023/11/10 10:33
// **/
//public class SnowFlakeIdentifierGeneratorStrategyProvider implements IdentifierGeneratorStrategyProvider {
//    @Override
//    public Map<String, Class<?>> getStrategies() {
//        return Map.of(JpaIdStrategy.SNOW_FLAKE, SnowFlakeIdGenerator.class);
//    }
//}
