package com.only4play.idgenerator.provider;

import com.only4play.idgenerator.service.impl.SnowFlakeIdWorker;
import com.only4play.idgenerator.service.impl.UUIDIdWorker;

import java.util.Objects;

/**
 * @author liyuncong
 * @version 1.0
 * @file IdGeneratorProvider
 * @brief IdGeneratorProvider
 * @details IdGeneratorProvider
 * @date 2024-01-12
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2024-01-12               liyuncong          Created
 */
public class IdGeneratorProvider {

    private static IdGenerator INSTANCE = null;

    private final IdGeneratorOptions options;

    public IdGeneratorProvider() {
        this.options = new IdGeneratorOptions();
    }

    public IdGeneratorProvider(IdGeneratorOptions options) throws IdGeneratorException {
        this.options = options;
    }

    // TODO DATABASE REDIS support
    public IdGenerator instance() {
        if (Objects.isNull(INSTANCE)) {
            synchronized (IdGeneratorProvider.class) {
                if (Objects.isNull(INSTANCE)) {
                    switch (options.generateStrategy) {
                        case UUID -> INSTANCE = new UUIDIdWorker();
                        case DATABASE, REDIS -> throw new IdGeneratorException("Not support.");
                        default -> INSTANCE = new SnowFlakeIdWorker();
                    }
                }
            }
        }
        return INSTANCE;
    }

}
