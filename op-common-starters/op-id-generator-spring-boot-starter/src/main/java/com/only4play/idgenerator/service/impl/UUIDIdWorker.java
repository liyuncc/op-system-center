package com.only4play.idgenerator.service.impl;

import com.only4play.idgenerator.provider.IdGenerator;
import com.only4play.idgenerator.provider.IdGeneratorException;
import com.only4play.idgenerator.service.UUIDIdentifierGenerator;

import java.util.UUID;

/**
 * @author liyuncong
 * @version 1.0
 * @file UUIDIdWorker
 * @brief UUIDIdWorker
 * @details UUIDIdWorker
 * @date 2024-01-12
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2024-01-12               liyuncong          Created
 */
public class UUIDIdWorker implements UUIDIdentifierGenerator, IdGenerator {

    @Override
    public String nextId() throws IdGeneratorException {
        // TODO just simple impl
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    @Override
    public long newLong() throws IdGeneratorException {
        throw new IdGeneratorException("Not supported for generation long.");
    }

    @Override
    public String newString() throws IdGeneratorException {
        return nextId();
    }
}
