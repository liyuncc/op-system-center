package com.only4play.idgenerator.service;

import com.only4play.idgenerator.provider.IdGeneratorException;

/**
 * @author liyuncong
 * @version 1.0
 * @file UUIDIdentifierGenerator
 * @brief UUIDIdentifierGenerator
 * @details UUIDIdentifierGenerator
 * @date 2024-01-12
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2024-01-12               liyuncong          Created
 */
public interface UUIDIdentifierGenerator {

    String nextId() throws IdGeneratorException;
}
