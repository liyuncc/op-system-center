package com.only4play.idgenerator.service;

/**
 * 唯一ID生成器
 *
 * @author liyuncong
 * @date 2023/11/10 10:18
 **/
public interface SingIdGenerator {

    Long snowFlake();

    Long snowFlake(Long workerId, Long datacenterId);
}
