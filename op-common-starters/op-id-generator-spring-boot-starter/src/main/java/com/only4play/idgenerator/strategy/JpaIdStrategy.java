package com.only4play.idgenerator.strategy;

/**
 * JPA ID生成策略
 *
 * @author liyuncong
 * @date 2023/11/10 10:20
 **/
public class JpaIdStrategy {
    public static final String SNOW_FLAKE = "snowflake";
    public static final String SNOW_FLAKE_GENERATOR = "com.only4play.idgenerator.service.SnowFlakeIdGenerator";
}
