package com.only4play.idgenerator.config;

//import com.only4play.idgenerator.provider.SnowFlakeIdentifierGeneratorStrategyProvider;
import com.only4play.idgenerator.service.SingIdGenerator;
import com.only4play.idgenerator.service.impl.SnowFlakeIdWorker;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 雪花算法配置
 *
 * @author liyuncong
 * @date 2023/11/10 10:14
 **/
@Configuration
@EntityScan(value = "com.only4play.idgenerator.config")
public class SnowFlakeConfiguration {
    @Bean
    public SingIdGenerator providerIdGenerate() {
        return new SnowFlakeIdWorker();
    }

//    @Bean
//    public SnowFlakeIdentifierGeneratorStrategyProvider snowFlakeGeneratorStrategyProvider() {
//        return new SnowFlakeIdentifierGeneratorStrategyProvider();
//    }
}
