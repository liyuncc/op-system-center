package com.only4play.jpa.support.entity;

import com.only4play.common.model.entity.IdentifiedObject;
import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Version;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.AbstractAggregateRoot;

import java.time.LocalDateTime;

// 标注为@MappedSuperclass的类将不是一个完整的实体类，他将不会映射到数据库表，但是他的属性都将映射到其子类的数据库字段中。
@MappedSuperclass
@Getter
@Setter
public abstract class AggregateEntity extends AbstractAggregateRoot<AggregateEntity>
    implements IdentifiedObject {

    @Column(name = "CREATED_BY", nullable = false, updatable = false)
    private String createdBy;

    @Column(name = "CREATED_AT", nullable = false, updatable = false)
    private LocalDateTime createdAt;

    @Column(name = "UPDATED_BY", nullable = false, updatable = false)
    private String updatedBy;

    @Column(name = "UPDATED_AT", nullable = false)
    private LocalDateTime updatedAt;

    @Version
    @Column(name = "ROW_STATUS")
    private Integer rowStatus;

    @PrePersist
    public void prePersist() {
    }

    @PreUpdate
    public void preUpdate() {
    }
}
