package com.only4play.jpa.support.batch;

import com.only4play.batch.BatchExecuteService;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.function.Consumer;

/**
 * JpaExecuteService
 *
 * @author liyuncong
 * @date 2023/11/27 14:30
 **/
@Service
public class JpaBatchExecuteService<T> extends BatchExecuteService<T> {
//    public <S extends T> void batchExecute(
//        Iterable<S> s, Consumer<S> consumer, EntityManager
//    ) {
//        int batchSize = this.config.getBatchSize();
//        try {
//            entityManager.getTransaction().begin();
//            Iterator<S> iterator = s.iterator();
//            int index = 0;
//            while (iterator.hasNext()) {
//                consumer.accept(iterator.next());
//                index++;
//                if (index % batchSize == 0) {
//                    entityManager.flush();
//                    entityManager.clear();
//                }
//            }
//            if (index % batchSize != 0) {
//                entityManager.flush();
//                entityManager.clear();
//            }
//            entityManager.getTransaction().commit();
//        } catch (Exception e) {
//            entityManager.getTransaction().rollback();
//        }
//    }
}
