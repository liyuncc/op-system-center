package com.only4play.mybatis.support;

import java.util.function.Supplier;

/**
 * 创建接口
 *
 * @author liyuncong
 * @date 2023/11/07 09:41
 **/
public interface CreateHandler<T> {

    UpdateHandler<T> create(Supplier<T> supplier);
}
