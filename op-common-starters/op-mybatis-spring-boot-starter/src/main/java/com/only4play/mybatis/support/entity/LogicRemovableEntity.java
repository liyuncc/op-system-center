package com.only4play.mybatis.support.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.only4play.common.model.entity.IdentifiedObject;
import lombok.Getter;
import lombok.Setter;

/**
 * 逻辑删除实体
 *
 * @author liyuncong
 * @date 2023/11/10 11:03
 **/
@Getter
@Setter
public abstract class LogicRemovableEntity implements IdentifiedObject {
    @TableLogic(delval = "0", value = "1")
    @TableField(value = "ROW_STATUS", fill = FieldFill.INSERT)
    private Integer rowStatus;
}
