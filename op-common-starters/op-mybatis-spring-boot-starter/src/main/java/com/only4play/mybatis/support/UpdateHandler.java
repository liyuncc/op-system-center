package com.only4play.mybatis.support;

import java.util.function.Consumer;

/**
 * 更新处理器
 *
 * @author liyuncong
 * @date 2023/11/07 09:44
 **/
public interface UpdateHandler<T> {

    Executor<T> update(Consumer<T> consumer);
}
