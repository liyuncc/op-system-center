package com.only4play.mybatis.support.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * 可审计的实体
 *
 * @author liyuncong
 * @date 2023/11/10 10:28
 **/
@Getter
@Setter
public abstract class AuditableEntity extends LogicRemovableEntity {
    @TableField(value = "CREATED_BY", fill = FieldFill.INSERT_UPDATE)
    private String createdBy;
    @TableField(value = "UPDATED_BY", fill = FieldFill.INSERT_UPDATE)
    private String updatedBy;
    @TableField(value = "CREATED_AT", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime createdAt;
    @TableField(value = "UPDATED_AT", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updatedAt;

    public void doTest(){

    }
}
