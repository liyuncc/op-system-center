package com.only4play.mybatis.support;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 实体操作
 *
 * @author liyuncong
 * @date 2023/11/07 09:26
 **/
public abstract class EntityOperations {
    public static <T> EntityUpdater<T> doUpdate(BaseMapper<T> baseMapper) {
        return new EntityUpdater<>(baseMapper);
    }

    public static <T> EntityCreator<T> doCreate(BaseMapper<T> baseMapper) {
        return new EntityCreator<>(baseMapper);
    }
}
