package com.only4play.mybatis.support;

import java.io.Serializable;
import java.util.function.Supplier;

/**
 * 加载器
 *
 * @author liyuncong
 * @date 2023/11/07 09:48
 **/
public interface Loader<T> {

    UpdateHandler<T> load(Supplier<T> t);

    UpdateHandler<T> loadById(Serializable id);
}
