package com.only4play.mybatis.support.batch;

import com.only4play.batch.BatchExecuteService;
import org.springframework.stereotype.Service;

/**
 * MybatisBatchService
 *
 * @author liyuncong
 * @date 2023/11/27 14:48
 **/
@Service
public class MybatisBatchService<T> extends BatchExecuteService<T> {
}
