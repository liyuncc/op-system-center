package com.only4play.mybatis.support;

import java.util.Optional;
import java.util.function.Consumer;

/**
 * 执行器
 *
 * @author liyuncong
 * @date 2023/11/07 09:49
 **/
public interface Executor<T> {

    Optional<T> execute();

    Executor<T> successHook(Consumer<T> consumer);

    Executor<T> failureHook(Consumer<? super Throwable> consumer);
}
