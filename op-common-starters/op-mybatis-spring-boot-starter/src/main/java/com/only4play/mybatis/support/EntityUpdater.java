package com.only4play.mybatis.support;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.google.common.base.Preconditions;
import com.only4play.common.constants.ErrorCodeEnum;
import com.only4play.common.exception.ApplicationException;
import com.only4play.common.validator.UpdateGroup;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * 实体更新器
 *
 * @author liyuncong
 * @date 2023/11/07 09:46
 **/
@Slf4j
public class EntityUpdater<T> extends BaseEntityOperation implements Loader<T>,
    UpdateHandler<T>, Executor<T> {

    private final BaseMapper<T> baseMapper;
    private T entity;
    private Consumer<T> successHook = t -> log.info("update success");
    private Consumer<? super Throwable> failureHook = Throwable::printStackTrace;

    public EntityUpdater(BaseMapper<T> baseMapper) {
        this.baseMapper = baseMapper;
    }


    @Override
    public Executor<T> update(Consumer<T> consumer) {
        Preconditions.checkArgument(Objects.nonNull(entity), "entity is null");
        consumer.accept(this.entity);
        return this;
    }

    @Override
    public Optional<T> execute() {
        doValidate(this.entity, UpdateGroup.class);
        baseMapper.updateById(entity);
        return Optional.ofNullable(this.entity);
    }

    @Override
    public Executor<T> successHook(Consumer<T> consumer) {
        this.successHook = consumer;
        return this;
    }

    @Override
    public Executor<T> failureHook(Consumer<? super Throwable> consumer) {
        this.failureHook = consumer;
        return this;
    }


    @Override
    public UpdateHandler<T> load(Supplier<T> t) {
        this.entity = t.get();
        return this;
    }

    @Override
    public UpdateHandler<T> loadById(Serializable id) {
        Preconditions.checkArgument(Objects.nonNull(id), "id is null");
        T t = baseMapper.selectById(id);
        if (Objects.isNull(t)) {
            throw new ApplicationException(ErrorCodeEnum.NOT_FOUND_ERROR);
        } else {
            this.entity = t;
        }
        return this;
    }
}
